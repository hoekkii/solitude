# solitude
The solitude engine is mainly a personal project to research different structuring- and programming possibilities.
To make the problems grounded in the real world a goal has been set.

## Goals

### Metronome
The first goal is to create a metronome.

### Settings saver

### Sheet music reader

### Note taking

### (multiple) audio tracks with subtitles

### Video



## Software
Click [here](./code/README.md) for engine documentation.

|                        |                                    |
|:-----------------------|:-----------------------------------|
| Internal Working Title | Solitude Engine                    |
| Enternal Working Title | Real Happy Sunburn Division Engine |


# Build
| Requirements | Why |
|:-------------|:----|
| [https://freetype.org/download.html](FreeType) | Generating fonts |
| [https://vulkan.lunarg.com/](Vulkan) | Rendering |


Inside `/code` are shell scripts for each build platform. For Unix systems -FreeBSD, Ubuntu, etc.- use `/code/unix.sh`. For Windows systems, use `/code/win.bat`.


# Folder structure
 - `/assets` files other than code such as audio-, fonts-, model- and texture files
 - `/builds` export directory
 - `/code`
 - `/docs` Documentation and resources
 - `/external` Tools and tests
 - `/solutions`  Platform specific files in order to compile


## Libraries and resources
https://wdobbie.com/post/gpu-text-rendering-with-vector-textures/
https://github.com/nothings/stb








#define PI 3.141592653589793f
#define TAU 6.283185307179586f
#define DEG2RAD 0.017453292519943295f
#define RAD2DEG 57.29577951308232f


#define saturate(x) max(0.0, min(1.0, x))
#define luminance(x) dot(x, vec3(0.22, 0.707, 0.071))


inline vec2 ParallaxOffset(float h, float height, vec3 viewDir)
{
	h = h * height - height / 2.0;
	vec3 v = normalize(viewDir);
	v.z += 0.42;
	return h * (v.xy / v.z);
}


uniform vec3 s_ambient_ground;
uniform vec3 s_ambient_equator;
uniform vec3 s_ambient_sky;


vec3 AmbientLight(vec3 normal)
{
	float upDot = normal.y; // dot(normal, up)
	float adjustedDot = upDot * 2.5;
	vec3 skyGroundColor = lerp(
		s_ambient_ground,
		s_ambient_sky,
		saturate((adjustedDot + 1.0) * 0.5));

	float equatorBright = saturate(dot(s_ambient_equator, s_ambient_equator));
	vec3 equatorBlurredColor = lerp(
		s_ambient_equator,
		saturate(s_ambient_equator + solitude_ambient_ground + s_ambient_sky),
		equatorBright * 0.33);

	float smoothDot = pow(abs(upDot), 1);
	vec3 equatorColor = lerp(equatorBlurredColor, s_ambient_ground, smoothDot) * step(upDot, 0)
	+ lerp(equatorBlurredColor, s_ambient_sky, smoothDot) * step(0, upDot);
	return lerp(skyGroundColor, equatorColor, saturate(equatorBright + 0.5)) * 0.75;
}




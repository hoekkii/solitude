#version 330 core
layout(row_major) uniform;

layout(location = 0) in vec2 position;
layout(location = 1) in uint data;
layout(location = 2) in vec4 color;

layout (location = 0) out FragmentData
{
	vec4 color;
	vec2 uv0;
	flat ivec4 st;
	flat uint offset;
} o;

layout(set = 0, binding = 0) uniform Object {
	samplerBuffer glyph;
} object;

layout(push_constant) uniform Constants {
	mat4 mvp;
	vec4 color;
} i;

float ushortFromVec2(vec2 v) { return (v.y * 65280.0 + v.x * 255.0); }
ivec2 vec2FromPixel(uint offset)
{
	vec4 pixel = texelFetch(uGlyphData, int(offset));
	return ivec2(ushortFromVec2(pixel.xy), ushortFromVec2(pixel.zw));
}

void main()
{
	oColor = color;
	glyphDataOffset = data >> 2u;
	oNormCoord = vec2((data & 2u) >> 1, data & 1u);
	oGridRect = ivec4(vec2FromPixel(glyphDataOffset), vec2FromPixel(glyphDataOffset + 1u));
	gl_Position = uTransform*vec4(position, 0.0, 1.0);
}

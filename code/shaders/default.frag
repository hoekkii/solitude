#version 450
layout(row_major) uniform;

// https://blog.selfshadow.com/publications/s2013-shading-course/karis/s2013_pbs_epic_notes_v2.pdf
// https://github.com/McNopper/OpenGL/blob/master/Example32/shader/brdf.frag.glsl
// http://www.merl.com/brdf/
// https://github.com/Unity-Technologies/ScriptableRenderPipeline/blob/master/com.unity.render-pipelines.core/ShaderLibrary/Sampling/Hammersley.hlsl

layout (location = 0) out vec4 color;
layout (location = 0) in FragmentData
{
	smooth vec4 pos;
	vec3 tangent;
	vec3 bitangent;
	vec3 normal;
	vec2 uv0;
	vec2 uv1;
	vec4 color;
	vec3 wpos;
	vec3 eye;
	vec3 viewdir;
} i;



/*layout(set = 0, binding = 0) uniform Object {
	mat4 model;
	mat4 modelinv;
	mat4 mvp;
	vec4 color;
	vec4 tex_st;
	vec4 light_st;
	vec4 eye;
} object;

void main()
{
	float a = length(i.pos)+length(i.tangent+i.bitangent+i.normal)+length(i.uv0+i.uv1);
	a = a + length(object.model * i.pos);
	a = a + length(object.modelinv * i.pos);
	a = a + length(object.mvp * i.pos);
	a = a + length(object.color + object.tex_st + object.light_st + object.eye);
	a = a / length(i.wpos+i.eye+i.viewdir);
	a = a * 0.0001;

	vec4 c = vec4(1.0, 0.0, 0.0, 1.0);
	c += i.color * object.color;
	c.a += 1.0 / (1.0 + a);
	//c.rgb *= c.a;
	color = c;
}*/


void main()
{
	float a = length(i.pos)+length(i.tangent+i.bitangent+i.normal)+length(i.uv0+i.uv1);
	a = a / length(i.wpos+i.eye+i.viewdir);
	a = a * 0.0001;

	vec4 c = vec4(0.1, 0.0, 0.0, 1.0);
	c += i.color;
	c.a += 1.0 / (1.0 + a);
	//c.rgb *= c.a;
	color = c;
}

/*
uniform vec3 u_eye;

uniform sampler2D u_parallax_tex;
uniform sampler2D u_main_tex;
uniform sampler2D u_bump_tex;
uniform sampler2D u_roughness_tex;
uniform sampler2D u_ambient_occlusion_tex;
uniform sampler2D u_skysphere_tex;
uniform sampler2D u_emission_tex;
uniform vec4 u_emission;
uniform float u_parallax;
uniform float u_bump_scale;
uniform float u_roughness;
uniform float u_occlusion;

uniform vec4 u_fog_params; // xyz: rgb, w:(density / sqrt(ln(2)))
uniform vec3 u_sun_direction;
uniform vec3 u_sun_color;
uniform mat4 u_lights_color;
uniform mat4 u_lights_position;


#if SOLITUDE_GL_VERSION >= 420
const vec2 u_hammersley64[64] = {
    vec2(0.00000000, 0.00000000),
    vec2(0.01562500, 0.50000000),
    vec2(0.03125000, 0.25000000),
    vec2(0.04687500, 0.75000000),
    vec2(0.06250000, 0.12500000),
    vec2(0.07812500, 0.62500000),
    vec2(0.09375000, 0.37500000),
    vec2(0.10937500, 0.87500000),
    vec2(0.12500000, 0.06250000),
    vec2(0.14062500, 0.56250000),
    vec2(0.15625000, 0.31250000),
    vec2(0.17187500, 0.81250000),
    vec2(0.18750000, 0.18750000),
    vec2(0.20312500, 0.68750000),
    vec2(0.21875000, 0.43750000),
    vec2(0.23437500, 0.93750000),
    vec2(0.25000000, 0.03125000),
    vec2(0.26562500, 0.53125000),
    vec2(0.28125000, 0.28125000),
    vec2(0.29687500, 0.78125000),
    vec2(0.31250000, 0.15625000),
    vec2(0.32812500, 0.65625000),
    vec2(0.34375000, 0.40625000),
    vec2(0.35937500, 0.90625000),
    vec2(0.37500000, 0.09375000),
    vec2(0.39062500, 0.59375000),
    vec2(0.40625000, 0.34375000),
    vec2(0.42187500, 0.84375000),
    vec2(0.43750000, 0.21875000),
    vec2(0.45312500, 0.71875000),
    vec2(0.46875000, 0.46875000),
    vec2(0.48437500, 0.96875000),
    vec2(0.50000000, 0.01562500),
    vec2(0.51562500, 0.51562500),
    vec2(0.53125000, 0.26562500),
    vec2(0.54687500, 0.76562500),
    vec2(0.56250000, 0.14062500),
    vec2(0.57812500, 0.64062500),
    vec2(0.59375000, 0.39062500),
    vec2(0.60937500, 0.89062500),
    vec2(0.62500000, 0.07812500),
    vec2(0.64062500, 0.57812500),
    vec2(0.65625000, 0.32812500),
    vec2(0.67187500, 0.82812500),
    vec2(0.68750000, 0.20312500),
    vec2(0.70312500, 0.70312500),
    vec2(0.71875000, 0.45312500),
    vec2(0.73437500, 0.95312500),
    vec2(0.75000000, 0.04687500),
    vec2(0.76562500, 0.54687500),
    vec2(0.78125000, 0.29687500),
    vec2(0.79687500, 0.79687500),
    vec2(0.81250000, 0.17187500),
    vec2(0.82812500, 0.67187500),
    vec2(0.84375000, 0.42187500),
    vec2(0.85937500, 0.92187500),
    vec2(0.87500000, 0.10937500),
    vec2(0.89062500, 0.60937500),
    vec2(0.90625000, 0.35937500),
    vec2(0.92187500, 0.85937500),
    vec2(0.93750000, 0.23437500),
    vec2(0.95312500, 0.73437500),
    vec2(0.96875000, 0.48437500),
    vec2(0.98437500, 0.98437500)
};
#else
uniform vec2 u_hammersley64[64];
#endif


void main()
{
	vec3 col = vec3(0.0, 0.0, 0.0);
	vec3 eye = normalize(i.eye);
	vec3 normal = normalize(i.normal);
	vec3 tangent = normalize(i.tangent);
	vec3 bitangent = normalize(i.bitangent);
	mat3 tangent_to_world = mat3(tangent, bitangent, normal);
	
	// IDK if this parallax works https://catlikecoding.com/unity/tutorials/rendering/part-20/
	vec3 viewdir = normalize((tangent_to_world * i.viewdir).xyz);
	float parallax = texture(u_parallax_tex, i.uv0).x;
	parallax = parallax * u_parallax - u_parallax / 2.0;
	vec2 texcoord = i.uv0 + viewdir.xy * parallax;
	
	vec4 pnormal = texture(u_bump_tex, texcoord);
	pnormal.x *= pnormal.w;
	vec3 tnormal;
	tnormal.xy = (pnormal.xy * 2.0 - 1.0) * u_bump_scale;
	tnormal.z = sqrt(1.0 - max(0.0, min(1.0, dot(tnormal.xy, tnormal.xy))));
	vec3 wnormal;
	wnormal.x = dot(vec3(i.tangent.x, i.bitangent.x, i.normal.x), tnormal);
	wnormal.y = dot(vec3(i.tangent.y, i.bitangent.y, i.normal.y), tnormal);
	wnormal.z = dot(vec3(i.tangent.z, i.bitangent.z, i.normal.z), tnormal);
	
	normal = wnormal;
	bitangent = cross(wnormal, tangent);
	tangent = cross(wnormal, bitangent);
	bitangent = cross(wnormal, tangent);
	tangent_to_world = mat3(tangent, bitangent, normal);
	
	vec4 c = texture(u_main_tex, texcoord) * i.color;
	
	// Lights
	vec3 light = vec3(0.0, 0.0, 0.0);
	for (int it = 0; it < 4; ++it)
	{
		vec4 l = u_lights_position[it];
		vec3 delta = l.xyz - i.wpos;
		float attenuation = 1.0 / (1.0 + l.w * dot(delta, delta));
		vec3 diff = attenuation * u_lights_color[it].rgb * max(0.0, dot(wnormal, normalize(delta)));
		light += diff;
	}
	light += u_sun_color * max(0.0, dot(wnormal, -u_sun_direction));
	
	// BRDF
	float roughness = texture(u_roughness_tex, texcoord).w * u_roughness;
	float k = (roughness + 1.0) * (roughness + 1.0) / 8.0;
	vec3 ambient = vec3(0.0, 0.0, 0.0);
	vec3 refl = vec3(0.0, 0.0, 0.0);
	for (int s = 0; s < 64; ++s)
	{
		vec2 p = u_hammersley64[s];
		vec3 pl = (tangent_to_world * vec3(
			sqrt(1.0 - p.x) * cos(2.0 * PI * p.y),
			sqrt(1.0 - p.x) * sin(2.0 * PI * p.y),
			sqrt(p.x))).xyz;
		vec2 lcoord = panorama(pl);
		ambient += c.rgb * texture(u_skysphere_tex, lcoord).rgb; // TODO: Check if we can read from mip-map
		
		float alpha = roughness + roughness;
		float phi = 2.0 * PI * p.y;
		float cost = sqrt((1.0 - p.x) / (1.0 + (alpha * alpha - 1.0) * p.x));
		float sint = sqrt(1.0 - cost * cost);
		vec3 h = tangent_to_world * vec3(sint * cos(phi), sint * sin(phi), cost);
		vec3 l = reflect(-eye, h);
		float ndl = dot(normal, l);
		float ndv = dot(normal, eye);
		float ndh = dot(normal, h);
		if (ndl > 0.0 && ndv > 0.0)
		{
			float vdh = dot(eye, h);
			float r0 = 0.5; // http://en.wikipedia.org/wiki/Schlick%27s_approximation
			float f = r0 + (1.0 - r0) * pow(1.0 - vdh, 5.0);
			float g = (ndl / (ndl * (1.0 - k) + k)) * (ndv / (ndv * (1.0 - k) + k));
			float c = f * g * vdh / (ndv * ndh);
			// TODO: check if isnan: if isnan(colorFactor) return 0
			refl += texture(u_skysphere_tex, panorama(l)).rgb * c;
		}
	}
	
	ambient /= 64.0;
	refl /= 64.0;
	
	c.rgb *= light;
	c.rgb += ambient;
	c.rgb += refl;
	
	// 
	float occlusion = texture(u_ambient_occlusion_tex, texcoord).r;
	occlusion = (1.0 - u_occlusion) + occlusion * u_occlusion;
	c.rgb *= occlusion;
	
	// 
	vec4 emission = texture(u_emission_tex, texcoord) * u_emission;
	c.rgb += emission.rgb * emission.a;
	
	// Apply fog
	float fogFactor = u_fog_params.w * i.pos.z;
	fogFactor = exp2(-fogFactor * fogFactor);
	c.rgb = mix(u_fog_params.rgb, c.rgb, fogFactor);
	
	// Fix blend mode
	c.a = 1.0; // TODO: Remove this
	c.rgb *= c.a;
	color = c;
	//color.rgb = ambient;
}*/

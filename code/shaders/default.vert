#version 450
layout(row_major) uniform;

layout (location = 0) in vec4 position;
layout (location = 1) in vec4 tangent;
layout (location = 2) in vec3 normal;
layout (location = 3) in vec2 uv0;
layout (location = 4) in vec2 uv1;
layout (location = 5) in vec4 color;
//layout (location = 6) in mediump uint light_index;

layout (location = 0) out FragmentData
{
	smooth vec4 pos;
	vec3 tangent;
	vec3 bitangent;
	vec3 normal;
	vec2 uv0;
	vec2 uv1;
	vec4 color;
	vec3 wpos;
	vec3 eye;
	vec3 viewdir;
} o;

layout(set = 0, binding = 0) uniform Object {
	mat4 model;
	mat4 modelinv;
	mat4 mvp;
	vec4 color;
	vec4 tex_st;
	vec4 light_st;
	vec4 eye;
} object;

layout(push_constant) uniform constants {
	mat4 mvp;
	vec4 color;
	vec4 tex_st;
	vec4 light_st;
} i;

void main()
{
	vec4 vertex = vec4(position.xyz, 1.0);
	vec4 wpos = object.model * vertex;
	vec4 pos = object.mvp * vertex;
	
	mat3 model_to_wdir = mat3(object.model);
	vec3 wnormal = normalize(model_to_wdir * normal);
	vec3 wtangent = normalize(model_to_wdir * tangent.xyz);
	vec3 wbinormal = cross(wnormal, wtangent) * tangent.w;
	vec3 eye = object.eye.xyz - wpos.xyz;
	
	o.pos = pos;
	o.tangent = wtangent;
	o.bitangent = wbinormal;
	o.normal = wnormal;
	o.uv0 = uv0 * object.tex_st.xy + object.tex_st.zw;
	o.uv1 = uv1 * object.light_st.xy + object.light_st.zw;
	o.color = color * object.color;
	o.wpos = wpos.xyz;
	o.eye = eye.xyz;
	o.viewdir = (object.modelinv * vec4(object.eye.xyz, 1.0)).xyz - position.xyz;
	
	gl_Position = i.mvp * vertex;
	//gl_Position = pos;
}

struct font_generator
{
	libhandle handle;

	int (*FT_Init_FreeType)(void*);
	int (*FT_Done_FreeType)(void*);
};


#if 1 || SOLTIDE_GL
error init(struct engine* engine, font_generator* generator)
{
	error err;
	var graphics_api = &engine->graphics_api;
	var asset_api = &engine->asset_api;

	if (graphics_api->type == graphics::api::GL)
	{
		//var vert = ;
	}
	else
	{
		return error(0x00000001, "Font generator requires OpenGL. This is enabled, but not active.");
	}

	// https://freetype.org/download.html
	var lib_name = PLATFORM_VAR("freetype.dll", "libfreetype.so");
	if (err = load_lib(&generator->handle, lib_name)) return err;

	var handle = generator->handle;
	if (err = load_proc(handle, generator->FT_Init_FreeType, "FT_Init_FreeType")) return err;
	if (err = load_proc(handle, generator->FT_Done_FreeType, "FT_Done_FreeType")) return err;
	
	return error::ok;
}

error dispose(font_generator* generator)
{
	if (!generator) return error::ok;
	if (generator->handle)
	{
		dispose_lib(generator->handle);
		generator->handle = {};
	}
	return error::ok;
}
#else
error init(font_generator* generator, graphics::api* api)
{
	*generator = {};
	return error(0x00000001, "Font generator only supports OpenGL");
}
error dispose(font_generator* generator) { return error::ok; }
#endif

error load_font(font_generator* generator, const char* path)
{
	if (generator && generator->handle)
	{
		

	}


	return error::ok;
}


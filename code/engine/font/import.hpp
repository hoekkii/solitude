struct font
{
	struct atlas
	{
		uint id;
		buffer_f grid;
		short2 next;
		uint gl_buffer; // TODO: Make generic
		uint gl_texture; // TODO: Make generic
		buffer_d data;
	};
	struct glyph
	{
		union
		{
			struct
			{
				short width;
				short height;
				short x;
				short y;
				short u;
				short v;
			};
			struct
			{
				short2 size;
				short2 offset;
				short2 uv;
			};
		};
		short advance;
	};
};



#if 1
#define SOLITUDE_FONTGENERATOR 1
#include "generator/import.hpp"
#endif



// Import
#include "math/import.hpp"
#include "assets/import.hpp"
#include "audio/import.hpp"
#include "rendering/import.hpp"


// Types
#include "input/types.hpp"

/**
 * fixed time: fixed_frame * clock_resolution / fixed_framerate
 * time: internal_time / clock_resolution
 *     NOTE: use incremental deltatime for timers, I don't want
 *     timescaling in the engine; it should be handled by every
 *     timer induvidualy. So [time] code needn't be written.
 * 
 * 
 * 
 */
struct engine
{
	static constexpr int fixed_rate = 240;
	static constexpr timestamp fixed_step = time::s(1.0 / fixed_rate);
	static constexpr timestamp max_timestep = time::ms(300);

	timestamp monotonic_time;
	timestamp internal_time;
	float deltatime;
	int frame;
	int fixed_frame;
	
	memory::job memory_jobs[4];
	
	asset::api asset_api;
	audio::api audio_api;
	graphics::api graphics_api;
	struct video video;
};

// Impl
#include "input/impl.hpp"
#include "font/import.hpp"





// TODO: we should have a platform layer?
error init(struct engine* engine)
{
	print("[ASSET] Initialize");
	{
		// TODO: list the graphics apis in order of platform/user preference
		asset::api::initproc* asset_apis[]{
			&fs_init,
		};

		struct asset::api api {};
		foreach(var proc, asset_apis)
		{
			error err = proc(&api, engine);
			if (err) print(err);
			else break;
		}

		if (api.type) print("Opened asset API: %v.", api.name);
		else print("Failed to open an asset API, continuing without assets.");

		engine->asset_api = api;
	}

	print("[AUDIO] Initialize");
	{
		// TODO: list the audio apis in order of platform/user preference
		audio::api::initproc* audio_apis[] {
			&init_oss,
			&init_alsa,
			&init_pulse,
		};
		
		struct audio::api api {};
		foreach(var proc, audio_apis)
		{
			error err = proc(&api);
			if (err) print(err);
			else break;
		}
		
		if (api.type) print("Opened audio API: %v.", api.name);
		else print("Failed to open an audio API, continuing without audio.");
		
		engine->audio_api = api;
	}
	
	print("[GRAPHICS] Initialize");
	{
		// TODO: list the graphics apis in order of platform/user preference
		graphics::api::initproc* graphics_apis[] {
			&init_gl,
			&init_gles,
			&init_vulkan,
		};
		
		struct graphics::api api {};
		foreach(var proc, graphics_apis)
		{
			error err = proc(&api, &engine->video);
			if (err) print(err);
			else break;
		}
		
		if (api.type) print("Opened grapphics API: %v.", api.name);
		else print("Failed to open an graphics API, continuing without graphics.");
		
		engine->graphics_api = api;
		if (engine->video.window)
			show(&engine->video);
	}
	
	engine->monotonic_time = time::precise();
	return error::ok;
}

void dispose(struct engine* engine)
{
	dispose(&engine->video);
	if (engine->audio_api.type)
		engine->audio_api.dispose(&engine->audio_api);
	if (engine->graphics_api.type)
		engine->graphics_api.dispose(&engine->graphics_api);
}

void begin_frame(struct engine* engine)
{
	engine->graphics_api.begin_frame(&engine->graphics_api);
}

void present_frame(struct engine* engine)
{
	engine->graphics_api.present_frame(&engine->graphics_api);
}



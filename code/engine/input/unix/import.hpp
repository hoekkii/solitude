
struct gamepad
{
	struct axis
	{
		int x;
		int y;
		float nx;
		float ny;
		float length;
		float angle;
		int dp;
		int dc;
	};
	struct trigger
	{
		int value;
		float length;
		int pp;
		int pc;
	};
};



#define O_RDONLY   0x0000
#define O_NONBLOCK 0x0004
struct input
{
	static constexpr float check_rate = 1.0f;

	// TODO: Maybe make a nice scheduler
	static constexpr char vendor_xbox               = 0b1000000;
	static constexpr char model_xbox_unknown        = 0b1000000;
	static constexpr char model_xbox_360            = 0b1000001;
	static constexpr char model_xbox_one            = 0b1000010;
	static constexpr char vendor_playstation        = 0b1100000;
	static constexpr char model_playstation_unknown = 0b1100000;
	static constexpr char model_playstation_3       = 0b1100001;
	static constexpr char model_playstation_4       = 0b1100010;
	static constexpr char model_playstation_5       = 0b1100011;

	static constexpr int device_capacity = 16;
	int device_length;
	struct
	{
		int fd;
		int state;
		int driver;
		char vendor;
		char model;
		char axis_length;
		char button_length;
		string_slice path;
		string_slice name;
	} devices[device_capacity];

	struct
	{
		uint n_ref;
		void* userdata;
	} udev;

	void* monitor;
	u64 check_timestamp;


	// Buffers
	static constexpr int path_capacity = 256;
	char path_buffer[device_capacity * path_capacity];
	static constexpr int name_capacity = 128;
	char name_buffer[device_capacity * name_capacity];
};


void dispose(struct input* inp)
{
	//udev_monitor_unref(inp->monitor);

}

error init(struct input* inp)
{
	char buf[512];
	string path;
	path.data = (byte*)buf;
	path.capacity = sizeof(buf);
	path.length = 0;
	append(&path, "/dev/input/");
	terminate(path);
	
	var dir = opendir(buf);
	if (!dir)
		return error(1, "Failed to open /dev/input/ directory.");

	var path_length = path.length;
	for (;;) {
		var de = readdir(dir);
		if (!de) break;

		var entry = *de;
		if (!entry.name || entry.name[0] == '.')
			continue;

		// Device path
		path.length = path_length;
		append(&path, entry.name, entry.name_length);
		terminate(path);

		// Open and check device
		union {
			uint v[4];
			char c[32];
			byte d[32];
		} device_data;

		var fd = open(buf, O_RDONLY | O_NONBLOCK);
		var version_ref = device_data.v + 0;
		if (ioctl_out(fd, 'j', 0x01, &device_data) == 0) {
			char* axes_ref = device_data.c + 8;
			ioctl_out(fd, 'j', 0x11, axes_ref);
			char* btns_ref = device_data.c + 4;
			ioctl_out(fd, 'j', 0x12, btns_ref);

			var device_length = inp->device_length;
			var name_buf = inp->name_buffer + input::name_capacity * device_length;
			ioctl_out(fd, 'j', 0x13, input::name_capacity - 1, name_buf);
			var name_len = len(name_buf);

			var path_buf = inp->path_buffer + input::path_capacity * device_length;
			copy(path_buf, path.data, path.length + 1);

			char vendor;
			char model;
			{
				// TODO: PlayStation
				// TODO: Xbox models
				if (contains_ignore_case_fast(name_buf, name_len, "xbox", 4) ||
					contains_ignore_case_fast(name_buf, name_len, "x-box", 5))
				{
					vendor = input::vendor_xbox;
					if (contains(name_buf, name_len, "360", 3))
						model = input::model_xbox_360;
					else model = input::model_xbox_unknown;
				}
				else
				{
					vendor = 0;
					model = 0;
				}
			}

			var device = inp->devices[device_length];
			{
				device.fd            = fd;
				device.driver        = *version_ref;
				device.vendor        = vendor;
				device.model         = model;
				device.axis_length   = *axes_ref;
				device.button_length = *btns_ref;
				device.path.data     = (byte*)path_buf;
				device.path.length   = path.length;
				device.name.data     = (byte*)name_buf;
				device.name.length   = name_len;
			}
			inp->devices[device_length] = device;
			inp->device_length++;
			print("joystick (%v): %v [%v;%v;%v;%v]", device.path, device.name, device.driver, (int)device.button_length, (int)device.axis_length, (int)device.model);
		} else {
			print("unknown (%v): %v", 0, path);
		}
	}
	closedir(dir);


	




	return error::ok;
}

void gamepad_update(struct input* inp)
{
	var time = time::fast();
	var delta = time - inp->check_timestamp;
	if (delta > time::s(1.0))
	{





		inp->check_timestamp = time;
	}


	var device_length = inp->device_length;
	for (var i = 0; i < device_length; i++)
	{
		var device = inp->devices[i];
		for (var j = 0; j < 12; j++)
		{
			struct
			{
				uint time;
				short value;
				byte type;
				byte number;
			} joy;
			if (read(device.fd, &joy, sizeof(joy)) <= 0)
				break;
			print("    gamepad_update (%v): %v %v %v", i, joy.type, joy.number, joy.value);
		}
	}
};



void input_test()
{
	var device = "/dev/input/js0";
	var df = open(device, O_RDONLY | O_NONBLOCK);
	print("df: %v", df);
	uint foo;
	var result = ioctl_out(df, 'j', 0x01, &foo);
	print("gamepad: %v, %v", result, foo);
	print("sizeof(uptr) = %v", sizeof(uptr));
}


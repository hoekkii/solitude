/**
 * Docs: https://docs.gl/
 * 
 */
struct gl : public graphics::share
{
	int test;

	// Platform
	void (*SwapBuffers)(void);

	// Textures
	void (*ActiveTexture)(flags32);
	void (*BindTexture)(flags32, uint);
	void (*TexBuffer)(flags32, flags32, uint);
	void (*TexImage1D)(flags32, int, int, int, int, flags32, flags32, const void*);
	void (*TexImage2D)(flags32, int, int, int, int, int, flags32, flags32, const void*);
	void (*TexImage2DMultisample)(flags32, int, int, int, int, uchar);
	void (*TexImage3D)(flags32, int, int, int, int, int, int, flags32, flags32, const void*);
	void (*TexImage3DMultisample)(flags32, int, int, int, int, int, uchar);
	void (*TexParameterf)(flags32, flags32, float);
	void (*TexParameteri)(flags32, flags32, int);
	void (*DeleteTextures)(int, const uint*);
	void (*GenTextures)(int, const uint*);
	
	// Rendering
	void (*ClearColor)(float, float, float, float);
	void (*Clear)(flags32);
	void (*Flush)(void);
	void (*Finish)(void);
	
	// Shaders
	void (*AttachShader)(uint, uint);
	void (*CompileShader)(uint);
	uint (*CreateProgram)(void);
	uint (*CreateShader)(flags32);
	void (*DeleteProgram)(uint);
	void (*DeleteShader)(uint);
	void (*DetachShader)(uint, uint);
	void (*GetProgramiv)(uint, uint, int*);
	void (*GetProgramInfoLog)(uint, uint, uint*, char*);
	void (*GetShaderiv)(uint, flags32, int*);
	void (*GetShaderInfoLog)(uint, uint, uint*, char*);
	void (*GetUniformfv)(uint, int, float*);
	void (*GetUniformiv)(uint, int, int*);
	void (*GetUniformuiv)(uint, int, uint*);
	int  (*GetUniformLocation)(uint, const char*);
	void (*LinkProgram)(uint);
	void (*ShaderSource)(uint, uint, const char**, const int*);
	void (*Uniform1f)(int, float);
	void (*Uniform2f)(int, float, float);
	void (*Uniform3f)(int, float, float, float);
	void (*Uniform4f)(int, float, float, float, float);
	void (*Uniform1i)(int, int);
	void (*Uniform2i)(int, int, int);
	void (*Uniform3i)(int, int, int, int);
	void (*Uniform4i)(int, int, int, int, int);
	void (*Uniform1ui)(int, uint);
	void (*Uniform2ui)(int, uint, uint);
	void (*Uniform3ui)(int, uint, uint, uint);
	void (*Uniform4ui)(int, uint, uint, uint, uint);
	void (*Uniform1fv)(int, uint, const float*);
	void (*Uniform2fv)(int, uint, const float*);
	void (*Uniform3fv)(int, uint, const float*);
	void (*Uniform4fv)(int, uint, const float*);
	void (*Uniform1iv)(int, uint, const int*);
	void (*Uniform2iv)(int, uint, const int*);
	void (*Uniform3iv)(int, uint, const int*);
	void (*Uniform4iv)(int, uint, const int*);
	void (*Uniform1uiv)(int, uint, const uint*);
	void (*Uniform2uiv)(int, uint, const uint*);
	void (*Uniform3uiv)(int, uint, const uint*);
	void (*Uniform4uiv)(int, uint, const uint*);
	void (*UniformMatrix2fv)(int, uint, uchar, const float*);
	void (*UniformMatrix3fv)(int, uint, uchar, const float*);
	void (*UniformMatrix4fv)(int, uint, uchar, const float*);
	void (*UniformMatrix2x3fv)(int, uint, uchar, const float*);
	void (*UniformMatrix3x2fv)(int, uint, uchar, const float*);
	void (*UniformMatrix2x4fv)(int, uint, uchar, const float*);
	void (*UniformMatrix4x2fv)(int, uint, uchar, const float*);
	void (*UniformMatrix3x4fv)(int, uint, uchar, const float*);
	void (*UniformMatrix4x3fv)(int, uint, uchar, const float*);
	void (*UseProgram)(uint);
	void (*ValidateProgram)(uint);
	
	// Buffer Objects
	void (*BindBuffer)(flags32, uint);
	void (*BufferData)(flags32, uptr, const void*, flags32);
	void (*BufferSubData)(flags32, sptr, uptr, const void*);
	void (*DeleteBuffers)(int, const uint*);
	void (*EnableVertexAttribArray)(uint);
	void (*DisableVertexAttribArray)(uint);
	void (*DrawArrays)(flags32, int, uint);
	void (*DrawArraysInstanced)(flags32, int, uint, uint);
	void (*GenBuffers)(uint, uint*);
	void (*VertexAttribPointer)(uint, int, flags32, uchar, uint, const void*);
	void (*VertexAttribIPointer)(uint, int, flags32, uint, const void*);
	

	// 

	void (*Begin)(flags32);
	void (*End)(void);
	void (*Color3f)(float, float, float);
	void (*Vertex3f)(float, float, float);
	flags32 (*GetError)(void);
	

	/*int (*glGenBuffers)();
	int (*glDeleteBuffers)();
	int (*glBindBuffer)();
	int (*glBufferData)();
	int (*glBufferSubData)();
	int (*glEnable)();
	int (*glEnableVertexAttribArray)();
	int (*glVertexAttribPointer)();
	int (*glVertexAttribIPointer)();
	int (*glDrawArrays)();
	int (*glDisableVertexAttribArray)();
	int (*glDisable)();
	int (*glGetUniformLocation)();
	int (*glUniform1i)();
	int (*glUniformMatrix4fv)();
	int (*glDeleteProgram)();
	int (*glGenBuffers)();
	int (*glUseProgram)();
	int (*glUniformMatrix4fv)();
	int (*glShaderSource)();
	int (*glCreateShader)();
	int (*glCompileShader)();
	int (*glGetShaderiv)();
	int (*glAttachShader)();
	int (*glLinkProgram)();
	int (*glGetProgramiv)();
	int (*glDetachShader)();
	int (*glDeleteShader)();*/
	
	byte shared[1024];
};


#include "api.hpp"
#if SOLITUDE_WIN
#include "win.hpp"
#elif SOLITUDE_UNIX
#include "unix.hpp"
#endif







void gl_begin_frame(struct graphics::api* api)
{
	var gl = (struct gl*)api->shared;
	gl->test++;


	

}

void gl_present_frame(struct graphics::api* api)
{
	flags32 test;
	var gl = (struct gl*)api->shared;

	//gl->DepthMask(1);
	//gl->ColorMask(1, 1, 1, 1);
	//gl->DepthFunc(GL_LEQUAL);
	//gl->Enable(GL_DEPTH_TEST);
	//gl->Enable(GL_CULL_FACE);
	//gl->CullFace(GL_BACK);
	//gl->FrontFace(GL_CCW);
	//gl->Enable(GL_MULTISAMPLE);
	//gl->Enable(GL_SCISSOR_TEST);
	//gl->Disable(GL_BLEND);
	//gl->BlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
	

	// Do some widow stuff

	//gl->ClearDepth(1.0f);
	float b = (gl->test & 0xFF) / (float)(0xFF);
	gl->ClearColor(1.0f, 0.0f, b, 1.0f);
	gl->Clear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);




	if (test = gl->GetError())
		print(test);

	gl->Begin(GL_POLYGON);
	if (test = gl->GetError())
		print(test);
	gl->Color3f(1, 0, 0); gl->Vertex3f(-0.6f, -0.75f, 0.5f);
	gl->Color3f(0, 1, 0); gl->Vertex3f(0.6f, -0.75f, 0);
	gl->Color3f(0, 0, 1); gl->Vertex3f(0, 0.75f, 0);
	gl->End();
	while (gl->GetError());




	//gl->Disable(GL_DEPTH_TEST);
	//gl->Disable(GL_BLEND);



	/*;

	gl->Begin(GL_POLYGON);

	test = gl->GetError();
	if (test)
		print(test);

	gl->Color3f(1, 0, 0); gl->Vertex3f(-0.6f, -0.75f, 0.5f);

	test = gl->GetError();
	if (test)
		print(test);

	gl->Color3f(0, 1, 0); gl->Vertex3f(0.6f, -0.75f, 0);
	gl->Color3f(0, 0, 1); gl->Vertex3f(0, 0.75f, 0);

	test = gl->GetError();
	if (test)
		print(test);

	gl->End();

	test = gl->GetError();
	if (test)
		print(test);*/
		
	gl->SwapBuffers();
}

static error gl_dispose(graphics::api* api)
{
	var gl = (struct gl*)api->shared;
	dispose_gl(gl);
	deallocate_raw(gl, 0);
	return error::ok;
}

static error init_gl(graphics::api* api, struct video* video)
{
	print("init_gl");
	error err;
	api->type = graphics::api::GL;
	api->name = "GL";
	api->begin_frame = gl_begin_frame;
	api->present_frame = gl_present_frame;
	api->dispose = gl_dispose;
	api->shared = allocate_raw(&(api->shared_size = sizeof(struct gl)), memory::CLEAR);
	print("init_gl");

	var gl = (struct gl*)api->shared;
	if (err = init_gl(gl, video))
		return err;
	
	print("init_glBBB");
	var test = gl->GetError();
	print("init_glCCC");
	if (test)
	{
		print(test);
		return error(0x00000001, "GL could not finish initializing %v", test);
	}
	print("init_glAAA");
	
	return error::ok;
}

static uint gl_shader_load(struct gl* gl, const char* vert_code, const char* frag_code)
{
	char buffer[2048];
	int result, log_length;

	// Compile vertex shader
	result = log_length = {};
	uint vert = gl->CreateShader(GL_VERTEX_SHADER);
	gl->ShaderSource(vert, 1, &vert_code, null);
	gl->CompileShader(vert);
	gl->GetShaderiv(vert, GL_COMPILE_STATUS, &result);
	gl->GetShaderiv(vert, GL_INFO_LOG_LENGTH, &log_length);
	if (log_length > 1)
	{
		assert(log_length < sizeof(buffer));
		gl->GetShaderInfoLog(vert, log_length, null, buffer);
		buffer[log_length] = 0;
		print(buffer);
	}
	if (!result) return 0;

	// Compile fragment shader
	result = log_length = {};
	uint frag = gl->CreateShader(GL_FRAGMENT_SHADER);
	gl->ShaderSource(frag, 1, &frag_code, null);
	gl->CompileShader(frag);
	gl->GetShaderiv(frag, GL_COMPILE_STATUS, &result);
	gl->GetShaderiv(frag, GL_INFO_LOG_LENGTH, &log_length);
	if (log_length > 1)
	{
		assert(log_length < sizeof(buffer));
		gl->GetShaderInfoLog(frag, log_length, null, buffer);
		buffer[log_length] = 0;
		print(buffer);
	}
	if (!result) return 0;

	// Link
	result = log_length = {};
	uint program = gl->CreateProgram();
	gl->AttachShader(program, vert);
	gl->AttachShader(program, frag);
	gl->LinkProgram(program);
	gl->GetProgramiv(program, GL_LINK_STATUS, &result);
	gl->GetProgramiv(program, GL_INFO_LOG_LENGTH, &log_length);
	if (log_length > 1)
	{
		assert(log_length < sizeof(buffer));
		gl->GetProgramInfoLog(program, log_length, null, buffer);
		buffer[log_length] = 0;
		print(buffer);
	}
	if (!result) return 0;

	gl->DetachShader(program, vert);
	gl->DetachShader(program, frag);
	gl->DeleteShader(vert);
	gl->DeleteShader(frag);
	return program;
}


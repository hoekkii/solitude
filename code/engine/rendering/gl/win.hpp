
#define SOLITUDE_GL_ENABLE_NEW_VIDEO 1
struct gl_win
{
	libhandle opengl32;
};

foreign void glFlush(void);
foreign void glFinish(void);
foreign flags32 glGetError(void);

foreign void glColor3f(float, float, float);
foreign void glVertex3f(float, float, float);

foreign void glBegin(flags32);
foreign void glEnd(void);

foreign void glEnd(void);
foreign void glDepthMask(int);
foreign void glColorMask(int, int, int, int);
foreign void glDepthFunc(flags32);
foreign void glEnable(flags32);
foreign void glDisable(flags32);
foreign void glCullFace(flags32);
foreign void glFrontFace(flags32);
foreign void glBlendFunc(flags32, flags32);
foreign void glClearDepth(float);
foreign void glClearColor(float red, float green, float blue, float alpha);
foreign void glClear(flags32);
foreign void glViewport(int, int, int, int);
foreign void glScissor(int, int, int, int);

static void swap_buffers_gl(void)
{
	SwapBuffers(wglGetCurrentDC());
}


static void dispose_gl(struct gl* gl)
{
	gl_win* platform = reinterpret_cast<gl_win*>(gl->shared);
	assert(sizeof(gl_win) <= sizeof(gl->shared));
	if (platform->opengl32)
		dispose_lib(platform->opengl32);
}

static error init_gl(struct gl* gl, struct video* video)
{
	error err = error::ok;
	gl_win* platform = reinterpret_cast<gl_win*>(gl->shared);
	assert(sizeof(gl_win) <= sizeof(gl->shared));
	
	
	// 
	BOOL(*wglChoosePixelFormatARB)(HDC, const int*, const float*, uint, int*, uint*) = 0;
	HGLRC(*wglCreateContextAttribsARB)(HDC, HGLRC, const int*) = 0;
	#if SOLITUDE_GL_ENABLE_NEW_VIDEO
	{
		HINSTANCE instance = GetModuleHandle(0);
		WNDCLASSA window_class = {};
		window_class.lpfnWndProc = Win32MainWindowCallback;
		window_class.hInstance = instance;
		window_class.lpszClassName = "SolitudeGLCheck";
		if (!RegisterClassA(&window_class))
			return error(0x00000001, "GL Failed to call RegisterClassA");

		HWND window = CreateWindowExA(
			0,
			window_class.lpszClassName,
			"Solitude - GL Check",
			WS_OVERLAPPEDWINDOW,
			CW_USEDEFAULT,
			CW_USEDEFAULT,
			CW_USEDEFAULT,
			CW_USEDEFAULT,
			0,
			0,
			instance,
			0);
		
		var hdc = GetDC(window);
		PIXELFORMATDESCRIPTOR descriptor = {};
		descriptor.nSize = sizeof(PIXELFORMATDESCRIPTOR);
		descriptor.nVersion = 1;
		descriptor.dwFlags = PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW | PFD_DOUBLEBUFFER;
		descriptor.iPixelType = PFD_TYPE_RGBA;
		descriptor.cColorBits = 32;
		descriptor.cAlphaBits = 8;
		descriptor.cDepthBits = 24;
		descriptor.iLayerType = PFD_MAIN_PLANE;
		
		PIXELFORMATDESCRIPTOR format;
		var index = ChoosePixelFormat(hdc, &descriptor);
		DescribePixelFormat(hdc, index, sizeof(PIXELFORMATDESCRIPTOR), &format);
		SetPixelFormat(hdc, index, &format);
		
		var glrc = wglCreateContext(hdc);
		if (wglMakeCurrent(hdc, glrc))
		{
			char* (*wglGetExtensionsStringEXT)(void) = 0;
			reinterpret_cast<void*&>(wglChoosePixelFormatARB) = wglGetProcAddress("wglChoosePixelFormatARB");
			reinterpret_cast<void*&>(wglCreateContextAttribsARB) = wglGetProcAddress("wglCreateContextAttribsARB");
			reinterpret_cast<void*&>(wglGetExtensionsStringEXT) = wglGetProcAddress("wglGetExtensionsStringEXT");
			if (wglGetExtensionsStringEXT)
			{
				var extensions = wglGetExtensionsStringEXT();
				print(extensions);
			}
			wglMakeCurrent(0, 0);
		}
		wglDeleteContext(glrc);
		ReleaseDC(window, hdc);
		DestroyWindow(window);
	}
	#endif
	
	// 
	{
		init(video);
		var hdc = GetDC(video->window);

		int format_index;
		uint format_count = 0;
		if (wglChoosePixelFormatARB)
		{
			// https://www.khronos.org/opengl/wiki/Creating_an_OpenGL_Context_(WGL)
			// https://www.khronos.org/registry/OpenGL/api/GL/wglext.h
			static int pixel_attribs[] = {
				0x2010, 0x0001, // OpenGL support
				0x2003, 0x2027, // acceleration
				0x2013, 0x202B, // pixel type
				0x2001, 0x0001, // draw
				0x2011, 0x0001, // double buffer
				0x2041,      1, // samples > 0
				0x2042,      8, // samples
				0x2014,     32, // color
				0x201B,      8, // alpha
				0x2022,     24, // depth
				0x2023,      0, // stencil
				0x20A9, 0x0001, // TODO: Make this one optional if WGL_EXT_framebuffer_sRGB or WGL_ARB_framebuffer_sRGB is in the extensions list.
				0x0000,
			};

			wglChoosePixelFormatARB(hdc, pixel_attribs, null, 1, &format_index, &format_count);
		}

		if (format_count == 0)
		{
			PIXELFORMATDESCRIPTOR descriptor = {};
			descriptor.nSize = sizeof(PIXELFORMATDESCRIPTOR);
			descriptor.nVersion = 1;
			descriptor.dwFlags = PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW | PFD_DOUBLEBUFFER;
			descriptor.iPixelType = PFD_TYPE_RGBA;
			descriptor.cColorBits = 32;
			descriptor.cAlphaBits = 8;
			descriptor.cDepthBits = 24;
			descriptor.iLayerType = PFD_MAIN_PLANE;
			format_index = ChoosePixelFormat(hdc, &descriptor);
		}

		PIXELFORMATDESCRIPTOR format;
		DescribePixelFormat(hdc, format_index, sizeof(PIXELFORMATDESCRIPTOR), &format);
		SetPixelFormat(hdc, format_index, &format);

		// Update pixel format
		HGLRC glrc = 0;
		if (wglCreateContextAttribsARB)
		{
			// Check if we can use a more modern context
			// https://registry.khronos.org/OpenGL/extensions/ARB/WGL_ARB_create_context.txt
			static int context_attribs[] = {
				0x2091,      3, // gl major version
				0x2092,      3, // gl minor version
				#if SOLITUDE_DEBUG
				0x2094, 0x0003,
				#else
				0x2094, 0x0002,
				#endif
				#if 1
				0x9126, 0x0002,
				#else
				0x9126, 0x0001,
				#endif
				0x0000,
			};
			glrc = wglCreateContextAttribsARB(hdc, 0, context_attribs);
		}
		if (!glrc)
		{
			print("Failed to wglCreateContextAttribsARB, falling back on old rendering.");
			glrc = wglCreateContext(hdc);
		}

		if (!wglMakeCurrent(hdc, glrc))
		{
			wglDeleteContext(glrc);
			ReleaseDC(video->window, hdc);
			dispose(video);
			return error(0x0000001, "Failed to create context");
		}
	}
	
	// Load dll
	var lib_name = PLATFORM_VAR("opengl32.dll");
	if (err = load_lib(&platform->opengl32, lib_name)) return err;
	var handle = platform->opengl32;
	
	gl->SwapBuffers = swap_buffers_gl;

	// Load procedures (find: \w+ \(\*(.+?)\).+  replace: GLPROC($1);)
	#define GLPROC(name) if (!(reinterpret_cast<void*&>(gl-> name) = wglGetProcAddress("gl" #name)) && (err = load_proc(handle, gl-> name, "gl" #name))) return err
	#define GLPROC_OPTIONAL(name) (reinterpret_cast<void*&>(gl-> name) = wglGetProcAddress("gl" #name)) || (load_proc(handle, gl-> name, "gl" #name))
	
	// Textures
	GLPROC(ActiveTexture);
	GLPROC(BindTexture);
	GLPROC(TexBuffer);
	GLPROC(TexImage1D);
	GLPROC(TexImage2D);
	GLPROC(TexImage2DMultisample);
	GLPROC(TexImage3D);
	GLPROC(TexImage3DMultisample);
	GLPROC(TexParameterf);
	GLPROC(TexParameteri);
	GLPROC(DeleteTextures);
	GLPROC(GenTextures);
	
	// Rendering
	gl->ClearColor = glClearColor;
	gl->Clear = glClear;
	gl->Flush = glFlush;
	gl->Finish = glFinish;
	gl->GetError = glGetError;
	
	// Shaders
	GLPROC(AttachShader);
	GLPROC(CompileShader);
	GLPROC(CreateProgram);
	GLPROC(CreateShader);
	GLPROC(DeleteProgram);
	GLPROC(DeleteShader);
	GLPROC(DetachShader);
	GLPROC(GetProgramiv);
	GLPROC(GetProgramInfoLog);
	GLPROC(GetShaderiv);
	GLPROC(GetShaderInfoLog);
	GLPROC(GetUniformfv);
	GLPROC(GetUniformiv);
	GLPROC(GetUniformuiv);
	GLPROC(GetUniformLocation);
	GLPROC(LinkProgram);
	GLPROC(ShaderSource);
	GLPROC(Uniform1f);
	GLPROC(Uniform2f);
	GLPROC(Uniform3f);
	GLPROC(Uniform4f);
	GLPROC(Uniform1i);
	GLPROC(Uniform2i);
	GLPROC(Uniform3i);
	GLPROC(Uniform4i);
	GLPROC(Uniform1ui);
	GLPROC(Uniform2ui);
	GLPROC(Uniform3ui);
	GLPROC(Uniform4ui);
	GLPROC(Uniform1fv);
	GLPROC(Uniform2fv);
	GLPROC(Uniform3fv);
	GLPROC(Uniform4fv);
	GLPROC(Uniform1iv);
	GLPROC(Uniform2iv);
	GLPROC(Uniform3iv);
	GLPROC(Uniform4iv);
	GLPROC(Uniform1uiv);
	GLPROC(Uniform2uiv);
	GLPROC(Uniform3uiv);
	GLPROC(Uniform4uiv);
	GLPROC(UniformMatrix2fv);
	GLPROC(UniformMatrix3fv);
	GLPROC(UniformMatrix4fv);
	GLPROC(UniformMatrix2x3fv);
	GLPROC(UniformMatrix3x2fv);
	GLPROC(UniformMatrix2x4fv);
	GLPROC(UniformMatrix4x2fv);
	GLPROC(UniformMatrix3x4fv);
	GLPROC(UniformMatrix4x3fv);
	GLPROC(UseProgram);
	GLPROC(ValidateProgram);
	
	// Buffer Objects
	GLPROC(BindBuffer);
	GLPROC(BufferData);
	GLPROC(BufferSubData);
	GLPROC(DeleteBuffers);
	GLPROC(EnableVertexAttribArray);
	GLPROC(DisableVertexAttribArray);
	GLPROC(DrawArrays);
	GLPROC(DrawArraysInstanced);
	GLPROC(GenBuffers);
	GLPROC(VertexAttribPointer);
	GLPROC(VertexAttribIPointer);
	
	
	
	
	
	gl->Begin = glBegin;
	gl->End = glEnd;
	GLPROC(Color3f);
	GLPROC(Vertex3f);
	
	#undef GLPROC
	return err;
}



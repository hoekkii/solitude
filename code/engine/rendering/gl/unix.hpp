

/**
 * 
 * 
 * 
 */
struct gl_unix
{
	libhandle libglx;
	libhandle libgl;
};



static void swap_buffers_gl()
{

}

static void dispose_gl(struct gl* gl)
{
	gl_unix* platform = reinterpret_cast<gl_unix*>(gl->shared);
	assert(sizeof(gl_unix) <= sizeof(gl->shared));
	
	if (platform->libglx)
		dispose_lib(platform->libglx);
	if (platform->libgl)
		dispose_lib(platform->libgl);
}

static error init_gl(struct gl* gl, struct video* video)
{
	error err;
	gl_unix* platform = reinterpret_cast<gl_unix*>(gl->shared);
	assert(sizeof(gl_unix) <= sizeof(gl->shared));
	
	if (err = init(video))
		return err;

	// GLX
	{
		libhandle handle;
		if ((err = load_lib(&handle, "libGLX.so")) && load_lib(&handle, "libGLX.so.0"))
			return err;
		
		platform->libglx = handle;
		void* (*glXGetProcAddressARB)(const void*);
		LOAD_FNPTR(glXGetProcAddressARB);
		
		// Load procedures
		#define GLPROC(name) if (!(reinterpret_cast<void*&>(gl-> name) = glXGetProcAddressARB("gl" #name)) && (err = load_proc(handle, gl-> name, "gl" #name))) return err
		#define GLPROC_OPTIONAL(name) (reinterpret_cast<void*&>(gl-> name) = glXGetProcAddressARB("gl" #name)) || (load_proc(handle, gl-> name, "gl" #name))
			
		// Textures
		GLPROC(ActiveTexture);
		GLPROC(BindTexture);
		GLPROC(TexBuffer);
		GLPROC(TexImage1D);
		GLPROC(TexImage2D);
		GLPROC(TexImage2DMultisample);
		GLPROC(TexImage3D);
		GLPROC(TexImage3DMultisample);
		GLPROC(TexParameterf);
		GLPROC(TexParameteri);
		GLPROC(DeleteTextures);
		GLPROC(GenTextures);
		
		// Rendering
		GLPROC(ClearColor);
		GLPROC(Clear);
		GLPROC(Flush);
		GLPROC(Finish);
		
		// Shaders
		GLPROC(AttachShader);
		GLPROC(CompileShader);
		GLPROC(CreateProgram);
		GLPROC(CreateShader);
		GLPROC(DeleteProgram);
		GLPROC(DeleteShader);
		GLPROC(DetachShader);
		GLPROC(GetProgramiv);
		GLPROC(GetProgramInfoLog);
		GLPROC(GetShaderiv);
		GLPROC(GetShaderInfoLog);
		GLPROC(GetUniformfv);
		GLPROC(GetUniformiv);
		GLPROC(GetUniformuiv);
		GLPROC(GetUniformLocation);
		GLPROC(LinkProgram);
		GLPROC(ShaderSource);
		GLPROC(Uniform1f);
		GLPROC(Uniform2f);
		GLPROC(Uniform3f);
		GLPROC(Uniform4f);
		GLPROC(Uniform1i);
		GLPROC(Uniform2i);
		GLPROC(Uniform3i);
		GLPROC(Uniform4i);
		GLPROC(Uniform1ui);
		GLPROC(Uniform2ui);
		GLPROC(Uniform3ui);
		GLPROC(Uniform4ui);
		GLPROC(Uniform1fv);
		GLPROC(Uniform2fv);
		GLPROC(Uniform3fv);
		GLPROC(Uniform4fv);
		GLPROC(Uniform1iv);
		GLPROC(Uniform2iv);
		GLPROC(Uniform3iv);
		GLPROC(Uniform4iv);
		GLPROC(Uniform1uiv);
		GLPROC(Uniform2uiv);
		GLPROC(Uniform3uiv);
		GLPROC(Uniform4uiv);
		GLPROC(UniformMatrix2fv);
		GLPROC(UniformMatrix3fv);
		GLPROC(UniformMatrix4fv);
		GLPROC(UniformMatrix2x3fv);
		GLPROC(UniformMatrix3x2fv);
		GLPROC(UniformMatrix2x4fv);
		GLPROC(UniformMatrix4x2fv);
		GLPROC(UniformMatrix3x4fv);
		GLPROC(UniformMatrix4x3fv);
		GLPROC(UseProgram);
		GLPROC(ValidateProgram);
		
		// Buffer Objects
		GLPROC(BindBuffer);
		GLPROC(BufferData);
		GLPROC(BufferSubData);
		GLPROC(DeleteBuffers);
		GLPROC(EnableVertexAttribArray);
		GLPROC(DisableVertexAttribArray);
		GLPROC(DrawArrays);
		GLPROC(DrawArraysInstanced);
		GLPROC(GenBuffers);
		GLPROC(VertexAttribPointer);
		GLPROC(VertexAttribIPointer);


		//
		GLPROC(Begin);
		GLPROC(End);
		GLPROC(Color3f);
		GLPROC(Vertex3f);
		GLPROC(GetError);
		
		#undef GLPROC
		#undef GLPROC_OPTIONAL
	}
	
	//if ((err = load_lib(&handle, "libGL.so")) && load_lib(&handle, "libGL.so.1"))
	//	return err;
	
	// Create window
	{
		init(video);
		int attributes[] = 
		{
			0x8011, 0x0001, // Render type = rgba
			0x8010, 0x0004, // Drawable type = pbuffer
			0x0000,
		};
		int config[] =
		{
			0x8041, 1280,   // Window width = 1280
			0x8040, 720,    // Window height = 720
			0x801B, 0x0001, // Preserved contents = true
			0x801C, 0x0000, // Largest pbuffer = false
			0x0000,
		};
		
		
	}
	
	return error::ok;
}

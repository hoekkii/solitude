error vulkan_dispose(struct graphics::api* api)
{
	// API
	var vk = (struct vulkan*)api->shared;

	// 
	var instance = vk->instance;
	if (instance)
	{
		if (vk->debug_messenger)
		{
			VkResult (*vkDestroyDebugUtilsMessengerEXT)(void*, VkPtr, void*);
			reinterpret_cast<void*&>(vkDestroyDebugUtilsMessengerEXT) = vk->find_proc(instance, "vkDestroyDebugUtilsMessengerEXT");
			if (vkDestroyDebugUtilsMessengerEXT)
				vkDestroyDebugUtilsMessengerEXT(instance, vk->debug_messenger, null);
		}
	}
	
	return platform_dispose(vk);
}



void vulkan_begin_frame(struct graphics::api* api)
{
	// API
	var vk = (struct vulkan*)api->shared;

	// HOT PATH
	constexpr u64 timeout = 1000000000UL;
	VkResult vk_result;
	var device = vk->device;

	vk_result = vk->vkAcquireNextImageKHR(vk->device, vk->swapchain.handle, timeout, vk->request_semaphore, null, &vk->swapchain.index);
	assert(vk_result == VK_SUCCESS);

	vk_result = vk->vkWaitForFences(device, 1, vk->swapchain.fences + vk->swapchain.index, VK_TRUE, timeout);
	assert(vk_result == VK_SUCCESS);

	vk_result = vk->vkResetCommandPool(vk->device, vk->command_pool, 0);
	assert(vk_result == VK_SUCCESS);

	var index = vk->swapchain.index;
	var cmd = vk->command_buffer = vk->swapchain.command_buffers[index];

	{
		VkCommandBufferBeginInfo info {};
		info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
		info.pInheritanceInfo = nullptr;

		vk_result = vk->vkBeginCommandBuffer(cmd, &info);
		assert(vk_result == VK_SUCCESS);

		// TODO: timestamp query pool
		//print("begin_frame %v", index);
	}
	{
		VkBufferCopy copyRegion {};
		copyRegion.size = vk->uniform_buffer_staging.length;
		vk->vkCmdCopyBuffer(vk->command_buffer, vk->uniform_buffer_staging.handle, vk->swapchain.uniform_buffers[index].handle, 1, &copyRegion);
	}
	{
		VkClearValue clearColor {};
		clearColor.color = {{0.0f, 0.1f, 0.1f, 1.0f}};
		
		constexpr int clearValueCount = 1;
		VkClearValue clearValues[clearValueCount];
		clearValues[0] = clearColor;
	
		VkRenderPassBeginInfo renderPassInfo{};
		renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
		renderPassInfo.renderPass = vk->render_pass;
		renderPassInfo.framebuffer = vk->swapchain.frame_buffers[index];
		renderPassInfo.renderArea = vk->viewport_rect;
		renderPassInfo.clearValueCount = clearValueCount;
		renderPassInfo.pClearValues = clearValues;
		vk->vkCmdBeginRenderPass(vk->command_buffer, &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);
	}
	
	// Test drawing
	{

		vk->vkCmdBindPipeline(vk->command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, vk->graphics_pipeline);
		

		//vk->vkCmdSetScissor(vk->command_buffer, 0, 1, &vk->viewport_rect);
		vk->vkCmdBindDescriptorSets(vk->command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, vk->pipeline_layout, 0, 1, &vk->descritpor_sets[index], 0, null);
		//vk->vkCmdDraw(vk->command_buffer, 3, 1, 0, 0);
	}
}

void vulkan_present_frame(struct graphics::api* api)
{
	// API
	var vk = (struct vulkan*)api->shared;
	
	// HOT PATH
	VkResult vk_result;
	var device = vk->device;
	// TODO: timestamp query pool
	
	{
		vk->vkCmdEndRenderPass(vk->command_buffer);
	}
	
	{
		vk_result = vk->vkEndCommandBuffer(vk->command_buffer);
		assert(vk_result == VK_SUCCESS);
	}

	var index = vk->swapchain.index;
	//print("end_frame %v", index);
	// Submit
	{
		flags32 pipe_stage_flags = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;//VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;

		VkSubmitInfo submitInfo {};
		submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		submitInfo.waitSemaphoreCount = 1;
		submitInfo.pWaitSemaphores = &vk->request_semaphore;
		submitInfo.pWaitDstStageMask = &pipe_stage_flags;
		submitInfo.commandBufferCount = 1;
		submitInfo.pCommandBuffers = &vk->command_buffer;
		submitInfo.signalSemaphoreCount = 1;
		submitInfo.pSignalSemaphores = &vk->present_semaphore;

		//vk->vkResetFences(device, 1, vk->swapchain.fences + vk->swapchain.index);
		vk_result = vk->vkQueueSubmit(vk->graphics_queue, 1, &submitInfo, null);//vk->swapchain.fences + vk->swapchain.index);
		//print(VkResultToString(vk_result));
		assert(vk_result == VK_SUCCESS);
		
		
		VkPresentInfoKHR presentInfo {};
		presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
		presentInfo.waitSemaphoreCount = 1;
		presentInfo.pWaitSemaphores = &vk->present_semaphore;
		presentInfo.swapchainCount = 1;
		presentInfo.pSwapchains = &vk->swapchain.handle;
		presentInfo.pImageIndices = &vk->swapchain.index;
		presentInfo.pResults = null;

		vk_result = vk->vkQueuePresentKHR(vk->presentation_queue, &presentInfo);
		assert(vk_result == VK_SUCCESS);
	}
	vk->vkDeviceWaitIdle(vk->device);

	// TODO: get timestamp data
}



error vulkan_init(graphics::api* api, struct video* video, vulkan::settings settings)
{
	// API
	var vk = (struct vulkan*)api->shared;

	// 
	VkResult vk_result;
	struct error err;
	if (err = platform_init(vk))
		return err;
		
	reinterpret_cast<void*&>(vk->vkEnumerateInstanceExtensionProperties) = vk->find_proc(null, "vkEnumerateInstanceExtensionProperties");
	reinterpret_cast<void*&>(vk->vkEnumerateInstanceLayerProperties) = vk->find_proc(null, "vkEnumerateInstanceLayerProperties");
	reinterpret_cast<void*&>(vk->vkCreateInstance) = vk->find_proc(null, "vkCreateInstance");

	print("[VULKAN] Getting extension properties");
	int required_extension_count = 0;
	const char* required_extensions[8];
	int required_layer_count = 0;
	const char* required_layers[8];
	{
		// TODO: also pass to through to platform_init_instance
		// When a pointer has been set, it is an optionsl extension
		bool* required_extension_availabilities[8] = {};
		bool* required_layer_availabilities[8] = {};
		uint available_extension_count;
		vk->vkEnumerateInstanceExtensionProperties(null, &available_extension_count, null);
		
		var ep_buffer = push(&vk->pool, sizeof(VkExtensionProperties) * available_extension_count);
		ep_buffer.data[0] = 1;
		vk->vkEnumerateInstanceExtensionProperties(null, &available_extension_count, ep_buffer.data);
		vk->extension_properties.elements = (VkExtensionProperties*)ep_buffer.data;
		vk->extension_properties.length = (int)available_extension_count;
		
		uint available_layer_count;
		vk->vkEnumerateInstanceLayerProperties(&available_layer_count, null);
		var el_buffer = push(&vk->pool, sizeof(VkLayerProperties) * available_layer_count);
		vk->vkEnumerateInstanceLayerProperties(&available_layer_count, el_buffer.data);
		vk->layer_properties.elements = (VkLayerProperties*)el_buffer.data;
		vk->layer_properties.length = (int)available_layer_count;

		if (err = platform_init_instance(vk, required_extensions, &required_extension_count, required_layers, &required_layer_count))
			return err;
	
		#if SOLITUDE_DEBUG
		required_extension_availabilities[required_extension_count] = &vk->debug_reports;
		required_extensions[required_extension_count++] = "VK_EXT_debug_report";
		required_extension_availabilities[required_extension_count] = &vk->debug_utils;
		required_extensions[required_extension_count++] = "VK_EXT_debug_utils";
		required_layer_availabilities[required_layer_count] = &vk->debug_validation;
		required_layers[required_layer_count++] = "VK_LAYER_KHRONOS_validation";
		#endif
	

		// Validate
		for (var i = required_extension_count - 1; i >= 0; i--)
		{
			var available = false;
			var target = required_extensions[i];
			var target_length = len(target);
			foreach(var extension, vk->extension_properties)
			{
				var extension_name = extension.extensionName;
				if (equals(target, extension_name, target_length))
				{
					available = true;
					break;
				}
			}

			var available_ptr = required_extension_availabilities[i];
			print((u64)available_ptr);
			if (available_ptr)
			{
				if (!available)
				{
					print("WARNING Could not find the following vulkan extension: %v", target);
					required_extension_count--;
					required_extensions[i] = required_extensions[required_extension_count];
				}
				*available_ptr = available;

			}
			else if (!available)
				return error(0x00000001, "%s was a required Vulkan extension, but it could not be found." FILE_LINE_FMT, target);
		}
		for (var i = required_layer_count - 1; i >= 0; i--)
		{
			var available = false;
			var target = required_layers[i];
			var target_length = len(target);
			print(target);
			foreach(var layer, vk->layer_properties)
			{
				print("Layer proeprty %v: %v", layer.layerName, layer.description);
				var layer_name = layer.layerName;
				if (equals(target, layer_name, target_length))
				{
					available = true;
					break;
				}
			}

			var available_ptr = required_layer_availabilities[i];
			if (available_ptr)
			{
				if (!available)
				{
					print("WARNING Could not find the following vulkan layer: %v", target);
					required_layer_count--;
					required_layers[i] = required_layers[required_layer_count];
				}
				*available_ptr = available;
			}
			else if (!available)
				return error(0x00000001, "%s was a required Vulkan layer, but it could not be found." FILE_LINE_FMT, target);
		}
	}
	
	print("[VULKAN] Creating instance");
	void* instance;
	{
		// Setup application info
		VkApplicationInfo appInfo;
		appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
		appInfo.pNext = null;
		appInfo.pApplicationName = SOLITUDE_APP_NAME;
		appInfo.applicationVersion = vk_version(0, 1, 0, 0);
		appInfo.pEngineName = "Solitude";
		appInfo.engineVersion = vk_version(0, 0, 1, 0);
		appInfo.apiVersion = vk_version(0, 1, 2, 0);
		

		// Create instance
		VkInstanceCreateInfo instanceInfo;
		instanceInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
		instanceInfo.pNext = null;
		instanceInfo.flags = 0;
		instanceInfo.pApplicationInfo = &appInfo;
		instanceInfo.enabledLayerCount = required_layer_count;
		instanceInfo.ppEnabledLayerNames = required_layers;
		instanceInfo.enabledExtensionCount = required_extension_count;
		instanceInfo.ppEnabledExtensionNames = required_extensions;

		if (vk_result = vk->vkCreateInstance(&instanceInfo, null, &instance))
			return error(0x00000001, VkResultToString(vk_result));
	
		if (vk->debug_validation)
		{
			print("[VULKAN] Enabling debug callbacks");
			VkDebugUtilsMessengerCreateInfoEXT debugInfo{};
			debugInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
			debugInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
			debugInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
			debugInfo.pfnUserCallback = &platform_vulkan_debug_message_callback;
			debugInfo.pUserData = vk;
			
			
			
			VkResult (*vkCreateDebugUtilsMessengerEXT)(void*, void*, void*, VkPtr);
			reinterpret_cast<void*&>(vkCreateDebugUtilsMessengerEXT) = vk->find_proc(instance, "vkCreateDebugUtilsMessengerEXT");
			if (vk_result = vkCreateDebugUtilsMessengerEXT(instance, &debugInfo, null, &vk->debug_messenger))
				return error(0x00000001, VkResultToString(vk_result));
		}

		vk->instance = instance;
	}
	
	if (err = vk->init_procedures())
		return err;
	
	print("[VULKAN] Creating surface");
	VkPtr surface;
	if (err = platform_init_surface(vk, video, &surface))
		return err;
	
	
	print("[VULKAN] Finding all graphic devices and selecting the best one");
	void* physicalDevice = null;
	VkPhysicalDeviceProperties       physicalDeviceProperties;
	VkPhysicalDeviceFeatures         physicalDeviceFeatures;
	VkPhysicalDeviceMemoryProperties physicalDeviceMemory;
	{
		uint device_count;
		void* devices[8];
		vk->vkEnumeratePhysicalDevices(instance, &device_count, null);
		vk->vkEnumeratePhysicalDevices(instance, &device_count, devices);
		iterate(i, device_count)
		{
			VkPhysicalDeviceProperties props;
			vk->vkGetPhysicalDeviceProperties(devices[i], &props);
			print(">>>> (id: %v; version: %v) %v", props.deviceID, props.apiVersion, props.deviceName);
			if (physicalDevice == null || physicalDeviceProperties.apiVersion < props.apiVersion)
			{
				physicalDevice = devices[i];
				physicalDeviceProperties = props;
			}
			if (settings.preferred_gpu == props.deviceID)
			{
				physicalDevice = devices[i];
				physicalDeviceProperties = props;
				break;
			}
		}
		
		if (physicalDevice == null)
			return error(0x00000001, "No suitable device found." FILE_LINE_FMT);
		
		vk->vkGetPhysicalDeviceFeatures(physicalDevice, &physicalDeviceFeatures);
		vk->vkGetPhysicalDeviceMemoryProperties(physicalDevice, &physicalDeviceMemory);
		vk->processor = physicalDevice;
		
		print("[VULKAN] Selected graphics device: %v", physicalDeviceProperties.deviceName);
	}
	
	
	
	
	print("[VULKAN] Detecting queue family indices");
	uint graphicsQueueIndex;
	uint computeQueueIndex;
	uint transferQueueIndex;
	uint presentationQueueIndex;
	{
		uint family_count;
		VkQueueFamilyProperties families[16];
		vk->vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &family_count, null);
		vk->vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &family_count, families);

		graphicsQueueIndex = UINTMAX;
		computeQueueIndex = UINTMAX;
		transferQueueIndex = UINTMAX;
		presentationQueueIndex = UINTMAX;
		iterate(i, family_count)
		{
			var f = families[i];
			if (graphicsQueueIndex == UINTMAX && f.queueFlags & VK_QUEUE_GRAPHICS_BIT)
				graphicsQueueIndex = i;

			if (computeQueueIndex == UINTMAX && f.queueFlags & VK_QUEUE_COMPUTE_BIT && !(f.queueFlags & VK_QUEUE_GRAPHICS_BIT))
				computeQueueIndex = i;

			if (transferQueueIndex == UINTMAX && f.queueFlags & VK_QUEUE_TRANSFER_BIT && !(f.queueFlags & (VK_QUEUE_GRAPHICS_BIT | VK_QUEUE_COMPUTE_BIT)))
				transferQueueIndex = i;

			uint supports_present;
			vk->vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevice, i, surface, &supports_present);
			if (presentationQueueIndex == U32MAX && supports_present)
				presentationQueueIndex = i;
		}

		if (graphicsQueueIndex == UINTMAX)
			return error(0x00000001, "Could not find a graphics queue");

		if (presentationQueueIndex == UINTMAX)
			return error(0x00000001, "Could not find a present queue");
		
		// When no awesome compute- and transfer queue are found
		// use a less awesome one.
		iterate(i, family_count)
		{
			var f = families[i];
			if (computeQueueIndex == U32MAX && f.queueFlags & VK_QUEUE_COMPUTE_BIT)
				computeQueueIndex = i;

			if (transferQueueIndex == U32MAX && f.queueFlags & VK_QUEUE_TRANSFER_BIT)
				transferQueueIndex = i;
		}
	}

	print("[VULKAN] Creating logical device");
	void* device;
	void* graphicsQueue;
	void* presentationQueue;
	{
		// Device queue
		constexpr u32 queueCount = 1;
		float queuePriorities[queueCount];
		queuePriorities[0] = 0.0f;

		u32 queueInfoCount = 1;
		VkDeviceQueueCreateInfo queueInfos[4];
		queueInfos[0].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		queueInfos[0].pNext = null;
		queueInfos[0].flags = 0;
		queueInfos[0].queueFamilyIndex = graphicsQueueIndex;
		queueInfos[0].queueCount = queueCount;
		queueInfos[0].pQueuePriorities = queuePriorities;

		if (presentationQueueIndex != graphicsQueueIndex)
		{
			queueInfos[queueInfoCount].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
			queueInfos[queueInfoCount].pNext = null;
			queueInfos[queueInfoCount].flags = 0;
			queueInfos[queueInfoCount].queueFamilyIndex = presentationQueueIndex;
			queueInfos[queueInfoCount].queueCount = queueCount;
			queueInfos[queueInfoCount].pQueuePriorities = queuePriorities;
			queueInfoCount++;
		}
		
		if (settings.use_compute && computeQueueIndex != graphicsQueueIndex && computeQueueIndex != presentationQueueIndex)
		{
			queueInfos[queueInfoCount].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
			queueInfos[queueInfoCount].pNext = null;
			queueInfos[queueInfoCount].flags = 0;
			queueInfos[queueInfoCount].queueFamilyIndex = computeQueueIndex;
			queueInfos[queueInfoCount].queueCount = queueCount;
			queueInfos[queueInfoCount].pQueuePriorities = queuePriorities;
			queueInfoCount++;
		}

		if (settings.use_transfer && transferQueueIndex != graphicsQueueIndex && transferQueueIndex != presentationQueueIndex && transferQueueIndex != computeQueueIndex)
		{
			queueInfos[queueInfoCount].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
			queueInfos[queueInfoCount].pNext = null;
			queueInfos[queueInfoCount].flags = 0;
			queueInfos[queueInfoCount].queueFamilyIndex = transferQueueIndex;
			queueInfos[queueInfoCount].queueCount = queueCount;
			queueInfos[queueInfoCount].pQueuePriorities = queuePriorities;
			queueInfoCount++;
		}

		// Device extensions
		u32 extensionCount = 0;
		const char* extensions[8];
		extensions[extensionCount++] = "VK_KHR_swapchain";
		
		if (vk->debug_validation)
			extensions[extensionCount++] = "VK_EXT_debug_marker";
		
		// Create actual device
		VkPhysicalDeviceFeatures deviceFeatures = settings.gpu_features;
		VkDeviceCreateInfo deviceInfo;
		deviceInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
		deviceInfo.pNext = null;
		deviceInfo.flags = 0;
		deviceInfo.queueCreateInfoCount = queueInfoCount;
		deviceInfo.pQueueCreateInfos = queueInfos;
		deviceInfo.enabledLayerCount = 0;
		deviceInfo.ppEnabledLayerNames = null;
		deviceInfo.enabledExtensionCount = extensionCount;
		deviceInfo.ppEnabledExtensionNames = extensions;
		deviceInfo.pEnabledFeatures = &deviceFeatures;

		if (vk_result = vk->vkCreateDevice(physicalDevice, &deviceInfo, null, &device))
			return error(0x00000001, VkResultToString(vk_result));

		vk->vkGetDeviceQueue(device, graphicsQueueIndex, 0, &graphicsQueue);
		vk->vkGetDeviceQueue(device, presentationQueueIndex, 0, &presentationQueue);
		vk->device = device;
		vk->graphics_queue = graphicsQueue;
		vk->presentation_queue = presentationQueue;
	}

	



	print("[VULKAN] Creating swapchain");
	VkSurfaceCapabilitiesKHR surfaceCapabilities;
	VkFormat swapchainFormat;
	VkColorSpaceKHR swapchainColorSpace;
	uint imageBufferCount;
	VkPtr swapchain;
	uint2 extent;
	{
		vk->vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice, surface, &surfaceCapabilities);
		if (surfaceCapabilities.currentExtent.x == U32MAX)
		{
			// TODO: This
			//int w, h;
			//SDL_GetWindowSize(window, &w, &h);
			//SDL_Vulkan_GetDrawableSize(window, &w, &h);
			surfaceCapabilities.currentExtent.x = clamp(1920U, surfaceCapabilities.minImageExtent.x, surfaceCapabilities.maxImageExtent.x);
			surfaceCapabilities.currentExtent.y = clamp(1080U, surfaceCapabilities.minImageExtent.y, surfaceCapabilities.maxImageExtent.y);
			return error(0x00000001, "Invalid surface capabilities extent; fallback not implemented.");
		}
		extent = surfaceCapabilities.currentExtent;

		// Find preferred surface format, if format_count is 1 and it is undefined: no format is preferred
		uint format_count;
		VkSurfaceFormatKHR* formats = vk->formats;
		vk->vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &format_count, null);
		vk->vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &format_count, formats);
		
		swapchainFormat = formats[0].format;
		swapchainColorSpace = formats[0].colorSpace;
		for (var i = 1; i < format_count; i++)
		{
			var format = formats[i];
			if (format.format == VK_FORMAT_B8G8R8A8_UNORM && format.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
			{
				swapchainFormat = VK_FORMAT_B8G8R8A8_UNORM;
				swapchainColorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;
				break;
			}
		}
		if (!swapchainFormat)
			swapchainFormat = VK_FORMAT_B8G8R8A8_UNORM;
		
		// Find present mode
		uint mode_count;
		VkPresentModeKHR modes[16];
		vk->vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, &mode_count, null);
		vk->vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, &mode_count, modes);

		VkPresentModeKHR presentMode = VK_PRESENT_MODE_FIFO_KHR;
		for (var i = 0; i < mode_count; i++)
		{
			if (modes[i] == VK_PRESENT_MODE_MAILBOX_KHR) {
				presentMode = VK_PRESENT_MODE_MAILBOX_KHR;
				break;
			}
		}

		VkCompositeAlphaFlagBitsKHR surfaceComposite =
			(surfaceCapabilities.supportedCompositeAlpha & VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR)
			? VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR
			: (surfaceCapabilities.supportedCompositeAlpha & VK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR)
			? VK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR
			: (surfaceCapabilities.supportedCompositeAlpha & VK_COMPOSITE_ALPHA_POST_MULTIPLIED_BIT_KHR)
			? VK_COMPOSITE_ALPHA_POST_MULTIPLIED_BIT_KHR
			: VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR;

		imageBufferCount = surfaceCapabilities.minImageCount + 1;
		if (surfaceCapabilities.maxImageCount > 0 && imageBufferCount > surfaceCapabilities.maxImageCount)
			imageBufferCount = surfaceCapabilities.maxImageCount;

		VkSwapchainCreateInfoKHR createInfo {};
		createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
		createInfo.surface = surface;
		createInfo.minImageCount = imageBufferCount;
		createInfo.imageFormat = swapchainFormat;
		createInfo.imageColorSpace = swapchainColorSpace;
		createInfo.imageExtent = surfaceCapabilities.currentExtent;
		createInfo.imageArrayLayers = 1;
		createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_STORAGE_BIT;
		createInfo.preTransform = surfaceCapabilities.currentTransform;
		createInfo.compositeAlpha = surfaceComposite;
		createInfo.presentMode = presentMode;
		createInfo.clipped = true;

		if (graphicsQueue == presentationQueue)
		{
			createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
			createInfo.queueFamilyIndexCount = 0;
			createInfo.pQueueFamilyIndices = null;
		}
		else
		{
			u32 queueFamilyIndices[2] = { graphicsQueueIndex, presentationQueueIndex };
			createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
			createInfo.queueFamilyIndexCount = 2;
			createInfo.pQueueFamilyIndices = queueFamilyIndices;
		}

		if (vk_result = vk->vkCreateSwapchainKHR(device, &createInfo, 0, &swapchain))
			return error(0x00000001, VkResultToString(vk_result));

		vk->swapchain.handle = swapchain;
		vk->swapchain.extent = extent;
		vk->swapchain.format = swapchainFormat;
	}

	print("[VULKAN] Creating swapchain images");
	uint image_count = 0;
	{
		if (vk_result = vk->vkGetSwapchainImagesKHR(device, swapchain, &image_count, null))
			return error(0x00000001, VkResultToString(vk_result));
		
		assert(image_count <= len(vk->swapchain.images));
		if (vk_result = vk->vkGetSwapchainImagesKHR(device, swapchain, &image_count, vk->swapchain.images))
			return error(0x00000001, VkResultToString(vk_result));
		
		iterate(i, image_count)
		{
			print("create_image_view %v", i);
			VkPtr image = vk->swapchain.images[i];
			VkPtr* view = vk->swapchain.views + i;
			*view = create_image_view(vk, image, vk->swapchain.format, VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_VIEW_TYPE_2D);
		}
		vk->swapchain.length = image_count;
	}

	vk->vertex_buffer          = create_buffer(vk, 4 * MEGABYTE, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
	vk->vertex_buffer_staging  = create_buffer(vk, 4 * MEGABYTE, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
	vk->index_buffer           = create_buffer(vk, 4 * MEGABYTE, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
	vk->index_buffer_staging   = create_buffer(vk, 4 * MEGABYTE, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
	
	vk->uniform_buffer_staging = create_buffer(vk, 1 * MEGABYTE, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
	for (int i = 0; i < vulkan::SWAPCHAIN_CAPACITY; i++)
	{
		vk->swapchain.uniform_buffers[i] = create_buffer(vk, 1 * MEGABYTE, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
	}

	print("[VULKAN] Creating command pool");
	{
		VkCommandPoolCreateInfo cmdpoolInfo = {};
		cmdpoolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
		cmdpoolInfo.pNext = null;
		cmdpoolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
		cmdpoolInfo.queueFamilyIndex = graphicsQueueIndex;
		if (vk_result = vk->vkCreateCommandPool(device, &cmdpoolInfo, null, &vk->command_pool))
			return error(0x00000001, VkResultToString(vk_result));

		VkCommandBufferAllocateInfo cmdBufferInfo = {};
		cmdBufferInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
		cmdBufferInfo.commandPool = vk->command_pool;
		cmdBufferInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
		cmdBufferInfo.commandBufferCount = imageBufferCount;
		if (vk_result = vk->vkAllocateCommandBuffers(device, &cmdBufferInfo, vk->swapchain.command_buffers))
			return error(0x00000001, VkResultToString(vk_result));

	}



	print("[VULKAN] Creating synchronization objects");
	// init ImageSemaphores, RenderSemaphores, ImageFences and RenderFences
	{
		VkSemaphoreCreateInfo semaphoreCreateInfo {};
		semaphoreCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

		if (vk_result = vk->vkCreateSemaphore(device, &semaphoreCreateInfo, nullptr, &vk->request_semaphore))
			return error(0x00000001, VkResultToString(vk_result));

		if (vk_result = vk->vkCreateSemaphore(device, &semaphoreCreateInfo, nullptr, &vk->present_semaphore))
			return error(0x00000001, VkResultToString(vk_result));


		VkFenceCreateInfo fenceCreateInfo {};
		fenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
		fenceCreateInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

		iterate(i, image_count)
		{
			var fence = vk->swapchain.fences + i;
			if (vk_result = vk->vkCreateFence(device, &fenceCreateInfo, nullptr, fence))
				return error(0x00000001, VkResultToString(vk_result));
		}
	}
	
	print("[VULKAN] Creating timestamp query pool");
	{
		VkPtr query_pool = null;
		VkQueryPoolCreateInfo create_info{};
		create_info.sType = VK_STRUCTURE_TYPE_QUERY_POOL_CREATE_INFO;
		create_info.queryType = VK_QUERY_TYPE_TIMESTAMP;
		create_info.queryCount = 2;
		if (vk_result = vk->vkCreateQueryPool(device, &create_info, nullptr, &query_pool))
			return error(0x00000001, VkResultToString(vk_result));
		vk->timestamp_query_pool = query_pool;
	}
	
	// TEST TEST TEST TEST
	{
		#if 1 // 2304
		var sphere = create_sphere({0.0f, 0.0f, 0.0f}, 1.0f);
		push(vk, &vk->vertex_buffer_staging, sphere.vertices, sizeof(sphere.vertices));
		print("vertex size: %v", vk->vertex_buffer_staging.length);
		push(vk, &vk->index_buffer_staging, sphere.indices, sizeof(sphere.indices));
		print("index size: %v", vk->index_buffer_staging.length);
		#endif
		#if 0 // 36
		var cube = create_cube({0.0f, 0.0f, 0.0f}, {1.0f, 1.0f, 1.0f});
		push(vk, &vk->vertex_buffer_staging, cube.vertices, sizeof(cube.vertices));
		print("vertex size: %v", vk->vertex_buffer_staging.length);
		push(vk, &vk->index_buffer_staging, cube.indices, sizeof(cube.indices));
		print("index size: %v", vk->index_buffer_staging.length);
		#endif
		#if 0
		struct vertex vertices[3] {};
		vertices[0].position = float4{ 0.0f, -0.5f, 0.0f, 1.0f};
		vertices[1].position = float4{ 0.9f,  0.5f, 0.0f, 1.0f};
		vertices[2].position = float4{-0.5f,  0.5f, 0.0f, 1.0f};
		vertices[0].color = float4{ 1.0f,1.0f,1.0f,1.0f };
		vertices[1].color = float4{ 0.0f,1.0f,1.0f,1.0f };
		vertices[2].color = float4{ 1.0f,0.0f,1.0f,1.0f };
		push(vk, &vk->vertex_buffer_staging, vertices, sizeof(vertices));
		print("vertex size: %v", vk->vertex_buffer_staging.length);
		
		ushort indices[3] {};
		indices[0] = 0;
		indices[1] = 1;
		indices[2] = 2;
		push(vk, &vk->index_buffer_staging, indices, sizeof(indices));
		print("index size: %v", vk->index_buffer_staging.length);
		#endif

	}
	{
		vulkan::ubo ubo {};
		ubo.model = float4x4::identity;
		ubo.modelinv = float4x4::identity;
		ubo.mvp = float4x4::identity;
		ubo.color = float4::one;
		
		vulkan::ubo ubos[] { ubo };
		push(vk, &vk->uniform_buffer_staging, ubos, sizeof(ubos));
	}
	
	{
		VkCommandBufferBeginInfo info {};
		info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
		info.pInheritanceInfo = nullptr;

		vk->staging_buffer = create_buffer(vk, 64 * MEGABYTE, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_COHERENT_BIT | VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);
		vk->command_buffer = vk->swapchain.command_buffers[0];
		if (vk_result = vk->vkBeginCommandBuffer(vk->command_buffer, &info))
			return error(0x00000001, VkResultToString(vk_result));
		
		{
			VkBufferCopy copyRegion {};
			copyRegion.size = vk->vertex_buffer_staging.length;
			vk->vkCmdCopyBuffer(vk->command_buffer, vk->vertex_buffer_staging.handle, vk->vertex_buffer.handle, 1, &copyRegion);
		}
		{
			VkBufferCopy copyRegion {};
			copyRegion.size = vk->index_buffer_staging.length;
			vk->vkCmdCopyBuffer(vk->command_buffer, vk->index_buffer_staging.handle, vk->index_buffer.handle, 1, &copyRegion);
		}
		{
			VkBufferCopy copyRegion {};
			copyRegion.size = vk->uniform_buffer_staging.length;
			vk->vkCmdCopyBuffer(vk->command_buffer, vk->uniform_buffer_staging.handle, vk->swapchain.uniform_buffers[0].handle, 1, &copyRegion);
			vk->vkCmdCopyBuffer(vk->command_buffer, vk->uniform_buffer_staging.handle, vk->swapchain.uniform_buffers[1].handle, 1, &copyRegion);
			vk->vkCmdCopyBuffer(vk->command_buffer, vk->uniform_buffer_staging.handle, vk->swapchain.uniform_buffers[2].handle, 1, &copyRegion);
		}
		
		if (vk_result = vk->vkEndCommandBuffer(vk->command_buffer))
			return error(0x00000001, VkResultToString(vk_result));
			
		
		VkSubmitInfo submitInfo {};
		submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		submitInfo.commandBufferCount = 1;
		submitInfo.pCommandBuffers = &vk->command_buffer;
		if (vk_result = vk->vkQueueSubmit(vk->graphics_queue, 1, &submitInfo, null))
			return error(0x00000001, VkResultToString(vk_result));
		
		vk->vkQueueWaitIdle(vk->graphics_queue);
		print("pushing data");
	}
	
	
	
	print("[VULKAN] Creating render pipeline");
	{
		// Vertrex input
		VkPipelineVertexInputStateCreateInfo pipelineVertexInputStateInfo;
		constexpr int pipelineVertexInputBindingCount = 1;
		VkVertexInputBindingDescription pipelineVertexInputBindings[pipelineVertexInputBindingCount];
		constexpr int pipelineVertexInputAttributeCount = 6;
		VkVertexInputAttributeDescription pipelineVertexInputAttributes[pipelineVertexInputAttributeCount];
		{
			VkVertexInputBindingDescription binding {};
			binding.binding = 0;
			binding.stride = sizeof(vertex);
			binding.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
			pipelineVertexInputBindings[0] = binding;

			VkVertexInputAttributeDescription a_position {};
			a_position.binding = 0;
			a_position.location = 0;
			a_position.format = VK_FORMAT_R32G32B32A32_SFLOAT;
			a_position.offset = offsetof(vertex, position);
			pipelineVertexInputAttributes[0] = a_position;
			
			VkVertexInputAttributeDescription a_tangent {};
			a_tangent.binding = 0;
			a_tangent.location = 1;
			a_tangent.format = VK_FORMAT_R32G32B32A32_SFLOAT;
			a_tangent.offset = offsetof(vertex, tangent);
			pipelineVertexInputAttributes[1] = a_tangent;

			VkVertexInputAttributeDescription a_normal {};
			a_normal.binding = 0;
			a_normal.location = 2;
			a_normal.format = VK_FORMAT_R32G32B32_SFLOAT;
			a_normal.offset = offsetof(vertex, normal);
			pipelineVertexInputAttributes[2] = a_normal;

			VkVertexInputAttributeDescription a_uv0 {};
			a_uv0.binding = 0;
			a_uv0.location = 3;
			a_uv0.format = VK_FORMAT_R32G32_SFLOAT;
			a_uv0.offset = offsetof(vertex, uv0);
			pipelineVertexInputAttributes[3] = a_uv0;

			VkVertexInputAttributeDescription a_uv1 {};
			a_uv1.binding = 0;
			a_uv1.location = 4;
			a_uv1.format = VK_FORMAT_R32G32_SFLOAT;
			a_uv1.offset = offsetof(vertex, uv1);
			pipelineVertexInputAttributes[4] = a_uv1;

			VkVertexInputAttributeDescription a_color {};
			a_color.binding = 0;
			a_color.location = 5;
			a_color.format = VK_FORMAT_R32G32B32A32_SFLOAT; // if uint -> VK_FORMAT_R8G8B8A8_UNORM
			a_color.offset = offsetof(vertex, color);
			pipelineVertexInputAttributes[5] = a_color;
			
			/*VkVertexInputAttributeDescription a_light_index {};
			a_color.binding = 0;
			a_color.location = 6;
			a_color.format = VK_FORMAT_R16_UINT;
			a_color.offset = offsetof(vertex, light_index);
			pipelineVertexInputAttributes[6] = a_light_index;*/
			
			VkPipelineVertexInputStateCreateInfo info {};
			info.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
			info.vertexBindingDescriptionCount = pipelineVertexInputBindingCount;
			info.pVertexBindingDescriptions = pipelineVertexInputBindings;
			info.vertexAttributeDescriptionCount = pipelineVertexInputAttributeCount;
			info.pVertexAttributeDescriptions = pipelineVertexInputAttributes;
			pipelineVertexInputStateInfo = info;
		}
		
		// Input assembly
		VkPipelineInputAssemblyStateCreateInfo pipelineInputAssembly;
		{
			VkPipelineInputAssemblyStateCreateInfo assembly {};
			assembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
			assembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
			assembly.primitiveRestartEnable = VK_FALSE;
			pipelineInputAssembly = assembly;
		}
		
		// Viewports and scissors
		VkPipelineViewportStateCreateInfo fullscreen{};
		VkRect2D viewport_rect{};
		VkViewport viewport{};
		{
			var extent = vk->swapchain.extent;
			viewport.x = 0;
			viewport.y = 0;
			viewport.width = extent.x;
			viewport.height = extent.y;
			viewport.minDepth = 0;
			viewport.maxDepth = 1;
			vk->viewport = viewport;
			
			viewport_rect.offset = {0, 0};
			viewport_rect.extent = extent;
			vk->viewport_rect = viewport_rect;
			
			fullscreen.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
			fullscreen.viewportCount = 1;
			fullscreen.pViewports = &viewport;
			fullscreen.scissorCount = 1;
			fullscreen.pScissors = &viewport_rect;
		}
		
		/*VkSampleCountFlagBits max_samples = {};
		{
			VkPhysicalDeviceProperties properties;
			vk->vkGetPhysicalDeviceProperties(vk->processor, &properties);
			var colors = properties.limits.framebufferColorSampleCounts;
			var depths = properties.limits.framebufferDepthSampleCounts;
			var bits = min(colors, depths);
			if      (bits & VK_SAMPLE_COUNT_64_BIT) max_samples = VK_SAMPLE_COUNT_64_BIT;
			else if (bits & VK_SAMPLE_COUNT_32_BIT) max_samples = VK_SAMPLE_COUNT_32_BIT;
			else if (bits & VK_SAMPLE_COUNT_16_BIT) max_samples = VK_SAMPLE_COUNT_16_BIT;
			else if (bits & VK_SAMPLE_COUNT_8_BIT)  max_samples = VK_SAMPLE_COUNT_8_BIT;
			else if (bits & VK_SAMPLE_COUNT_4_BIT)  max_samples = VK_SAMPLE_COUNT_4_BIT;
			else if (bits & VK_SAMPLE_COUNT_2_BIT)  max_samples = VK_SAMPLE_COUNT_2_BIT;
			else if (bits & VK_SAMPLE_COUNT_1_BIT)  max_samples = VK_SAMPLE_COUNT_1_BIT;

			//var extent = vk->fullscreen_rect.extent;
			//vk->rt_zbuffer     = create_render_target(vk, extent, VK_FORMAT_D32_SFLOAT, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, VK_IMAGE_ASPECT_DEPTH_BIT);
			//vk->rt_zbuffer_atc = create_render_target(vk, extent, VK_FORMAT_D32_SFLOAT, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT, VK_IMAGE_ASPECT_DEPTH_BIT, max_samples);
			//vk->rt_color_atc   = create_render_target(vk, extent, vk->swapchain.format, VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT, VK_IMAGE_ASPECT_COLOR_BIT, max_samples);
		}*/
		// Descriptors
		VkPtr descriptor_uniform_buffer;
		{
			VkDescriptorPoolSize pool_size;
			pool_size.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;// _DYNAMIC;
			pool_size.descriptorCount = vk->swapchain.length;

			VkDescriptorPoolCreateInfo poolInfo{};
			poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
			poolInfo.poolSizeCount = 1;
			poolInfo.pPoolSizes = &pool_size;
			poolInfo.maxSets = vk->swapchain.length;

			VkPtr descriptor_pool;
			if (vk_result = vk->vkCreateDescriptorPool(vk->device, &poolInfo, null, &descriptor_pool))
				return error(0x00000001, VkResultToString(vk_result));

			vk->descriptor_pool = descriptor_pool;

			{
				VkDescriptorSetLayoutBinding layoutBinding{};
				layoutBinding.binding = 0;
				layoutBinding.descriptorCount = 1;
				layoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;// _DYNAMIC;
				layoutBinding.pImmutableSamplers = null;
				layoutBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
				//layoutBinding.stageFlags = VK_SHADER_STAGE_ALL_GRAPHICS;

				VkDescriptorSetLayoutBinding bindings[] = { layoutBinding };
				VkDescriptorSetLayoutCreateInfo layoutInfo = {};
				layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
				layoutInfo.bindingCount = len(bindings);
				layoutInfo.pBindings = bindings;

				if (vk_result = vk->vkCreateDescriptorSetLayout(vk->device, &layoutInfo, null, &descriptor_uniform_buffer))
					return error(0x00000001, VkResultToString(vk_result));
			}
			// TODO: All descriptors
		}

		// Push constants
		VkPushConstantRange push_constant {};
		{
			push_constant.offset = 0;
			push_constant.size = sizeof(vulkan::push_constant);
			push_constant.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
		}

		{
			VkPtr layouts[vulkan::SWAPCHAIN_CAPACITY];
			for (int i = 0; i < vulkan::SWAPCHAIN_CAPACITY; i++)
				layouts[i] = descriptor_uniform_buffer;

			VkDescriptorSetAllocateInfo allocInfo{};
			allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
			allocInfo.descriptorPool = vk->descriptor_pool;
			allocInfo.descriptorSetCount = vk->swapchain.length;
			allocInfo.pSetLayouts = layouts;

			VkPtr descriptor_sets[vulkan::SWAPCHAIN_CAPACITY];
			if (vk_result = vk->vkAllocateDescriptorSets(vk->device, &allocInfo, descriptor_sets))
				return error(0x00000001, VkResultToString(vk_result));

			// for every swapchain image???
			for (int i = 0; i < vk->swapchain.length; i++)
			{
				VkDescriptorBufferInfo bufferInfo{};
				bufferInfo.buffer = vk->swapchain.uniform_buffers[i].handle;
				bufferInfo.offset = 0;
				bufferInfo.range = sizeof(vulkan::ubo);

				// 
				VkWriteDescriptorSet descriptorWrite{};
				descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
				descriptorWrite.dstSet = descriptor_sets[i];
				descriptorWrite.dstBinding = 0;
				descriptorWrite.dstArrayElement = 0;
				descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;// _DYNAMIC;
				descriptorWrite.descriptorCount = 1;
				descriptorWrite.pBufferInfo = &bufferInfo;
				descriptorWrite.pImageInfo = nullptr;
				descriptorWrite.pTexelBufferView = nullptr;
				vk->vkUpdateDescriptorSets(vk->device, 1, &descriptorWrite, 0, null);
			}
			copy(vk->descritpor_sets, descriptor_sets, sizeof(descriptor_sets));
		}

		
		// Rasterizer
		VkPipelineRasterizationStateCreateInfo cull_off;
		VkPipelineRasterizationStateCreateInfo cull_back;
		VkPipelineRasterizationStateCreateInfo cull_front;
		{
			VkPipelineRasterizationStateCreateInfo rasterization{};
			rasterization.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
			rasterization.depthClampEnable = VK_FALSE;
			rasterization.rasterizerDiscardEnable = VK_FALSE;
			rasterization.polygonMode = VK_POLYGON_MODE_FILL;
			rasterization.lineWidth = 1.0f;
			rasterization.frontFace = VK_FRONT_FACE_CLOCKWISE;
			rasterization.depthBiasEnable = VK_FALSE;
			rasterization.depthBiasConstantFactor = 0.0f;
			rasterization.depthBiasClamp = 0.0f;
			rasterization.depthBiasSlopeFactor = 0.0f;
			
			cull_front = cull_back = cull_off = rasterization;
			cull_off.cullMode = VK_CULL_MODE_NONE;
			cull_back.cullMode = VK_CULL_MODE_BACK_BIT;
			cull_front.cullMode = VK_CULL_MODE_FRONT_BIT;
		}
		
		// Multisampling
		VkPipelineMultisampleStateCreateInfo multisample {};
		{
			multisample.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
			multisample.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
			multisample.minSampleShading = 1.0f;

			/*if (settings.multisample)
			{
				multisample.rasterizationSamples = max_samples;
				multisample.sampleShadingEnable = VK_TRUE;
				multisample.minSampleShading = 1.0f / max_samples;
				multisample.pSampleMask = null;
				multisample.alphaToCoverageEnable = VK_TRUE;
				multisample.alphaToOneEnable = VK_TRUE;
			}*/
		}
		
		// Color blending
		VkPipelineColorBlendAttachmentState attachment = {};
		VkPipelineColorBlendStateCreateInfo colorBlendState = {};
		{
			attachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
			attachment.blendEnable = VK_FALSE;//VK_TRUE;
			attachment.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
			attachment.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;//VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
			attachment.colorBlendOp = VK_BLEND_OP_ADD;
			attachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
			attachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;//VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
			attachment.alphaBlendOp = VK_BLEND_OP_ADD;
			
			colorBlendState.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
			colorBlendState.logicOpEnable = VK_FALSE;
			colorBlendState.logicOp = VK_LOGIC_OP_COPY;
			colorBlendState.attachmentCount = 1;
			colorBlendState.pAttachments = &attachment;
		}
		
		// Dynamic state
		VkDynamicState dynamicStates[] = {
			VK_DYNAMIC_STATE_VIEWPORT,
			VK_DYNAMIC_STATE_LINE_WIDTH
		};
		VkPipelineDynamicStateCreateInfo dynamicState {};
		{
			dynamicState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
			dynamicState.dynamicStateCount = 0;
			dynamicState.pDynamicStates = dynamicStates;
		}
		
		// Pipeline layout
		VkPtr pipelineLayout;
		{
			constexpr int set_layout_length = 1;
			VkPtr set_layouts[set_layout_length] = {
				descriptor_uniform_buffer,
			};

			constexpr int push_constant_length = 1;
			VkPushConstantRange push_constants[push_constant_length] = {
				push_constant,
			};

			VkPipelineLayoutCreateInfo layout_info {};
			layout_info.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
			layout_info.setLayoutCount = set_layout_length;
			layout_info.pSetLayouts = set_layouts;
			layout_info.pushConstantRangeCount = push_constant_length;
			layout_info.pPushConstantRanges = push_constants;
			if (vk_result = vk->vkCreatePipelineLayout(device, &layout_info, nullptr, &pipelineLayout))
				return error(0x00000001, VkResultToString(vk_result));
		}
		
		// Attachment description
		VkAttachmentDescription colorAttachment;
		{
			// Color attachment
			{
				VkAttachmentDescription description = {};
				description.format = vk->swapchain.format;
				description.samples = VK_SAMPLE_COUNT_1_BIT;
				description.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
				description.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
				description.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
				description.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
				description.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
				description.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
				colorAttachment = description;
			}
		}
		
		// Render pass, Subpasses and attachment references
		VkPtr renderPass;
		{
			constexpr int colorAttachmentCount = 1;
			VkAttachmentReference colorAttachments[colorAttachmentCount];
			colorAttachments[0].attachment = 0;
			colorAttachments[0].layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
			
			VkSubpassDescription subpass = {};
			subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
			subpass.colorAttachmentCount = colorAttachmentCount;
			subpass.pColorAttachments = colorAttachments;
			
			VkSubpassDependency dependency {};
			dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
			dependency.dstSubpass = 0;
			dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
			dependency.srcAccessMask = 0;
			dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
			dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT| VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
			
			VkRenderPassCreateInfo renderPassInfo = {};
			renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
			renderPassInfo.attachmentCount = 1;
			renderPassInfo.pAttachments = &colorAttachment;
			renderPassInfo.subpassCount = 1;
			renderPassInfo.pSubpasses = &subpass;
			renderPassInfo.dependencyCount = 1;
			renderPassInfo.pDependencies = &dependency;
			
			if (vk_result = vk->vkCreateRenderPass(device, &renderPassInfo, nullptr, &renderPass))
				return error(0x00000001, VkResultToString(vk_result));
				
			vk->render_pass = renderPass;
		}
		
		// Framebuffers
		{
			var fs = vk->viewport;
			for (uptr i = 0; i < vk->swapchain.length; i++)
			{
				VkFramebufferCreateInfo framebufferInfo = {};
				framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
				framebufferInfo.renderPass = renderPass;
				framebufferInfo.attachmentCount = 1;
				framebufferInfo.pAttachments = &vk->swapchain.views[i];
				framebufferInfo.width = fs.width;
				framebufferInfo.height = fs.height;
				framebufferInfo.layers = 1;

				VkPtr framebuffer;
				if (vk_result = vk->vkCreateFramebuffer(vk->device, &framebufferInfo, nullptr, &framebuffer))
					return error(0x00000001, VkResultToString(vk_result));
				vk->swapchain.frame_buffers[i] = framebuffer;
			}
		}
		
		
		//
		VkPtr pipeline_handle;
		{
			// !!!!! DEBUG: TODO: Change this to asset handling
			VkPipelineShaderStageCreateInfo shaderStages[2];
			if (err = create_stage(vk, __DEBUG_BASE_PATH "assets/shaders/default.vert.spv", VK_SHADER_STAGE_VERTEX_BIT, &shaderStages[0]))
				return err;
			if (err = create_stage(vk, __DEBUG_BASE_PATH "assets/shaders/default.frag.spv", VK_SHADER_STAGE_FRAGMENT_BIT, &shaderStages[1]))
				return err;
		
			VkGraphicsPipelineCreateInfo pipelineInfo{};
			pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
			pipelineInfo.pNext = null;
			pipelineInfo.flags = 0;
			pipelineInfo.stageCount = 2;
			pipelineInfo.pStages = shaderStages;
			pipelineInfo.pVertexInputState = &pipelineVertexInputStateInfo;
			pipelineInfo.pInputAssemblyState = &pipelineInputAssembly;
			pipelineInfo.pTessellationState = null;
			pipelineInfo.pViewportState = &fullscreen;
			pipelineInfo.pRasterizationState = &cull_off;
			pipelineInfo.pRasterizationState = &cull_back;
			pipelineInfo.pMultisampleState = &multisample;
			pipelineInfo.pDepthStencilState = nullptr;
			pipelineInfo.pColorBlendState = &colorBlendState;
			pipelineInfo.pDynamicState = &dynamicState;
			pipelineInfo.layout = pipelineLayout;
			pipelineInfo.renderPass = renderPass;
			pipelineInfo.subpass = 0;
			pipelineInfo.basePipelineHandle = null;
			pipelineInfo.basePipelineIndex = -1;
			
			if (vk_result = vk->vkCreateGraphicsPipelines(device, null, 1, &pipelineInfo, nullptr, &pipeline_handle))
				return error(0x00000001, VkResultToString(vk_result));
			vk->graphics_pipeline = pipeline_handle;
			vk->pipeline_layout = pipelineLayout;
		}
	}
	
	print("Init: SUCCESS");
	return error::ok;
}








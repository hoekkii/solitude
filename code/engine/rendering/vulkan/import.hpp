#include "api.hpp"

struct vulkan : public graphics::share
{
	static constexpr u64 MEMORY_CHUNK_CAPACITY = 256 * MEGABYTE;
	static constexpr int MAX_TEXTURE_COUNT = 4;
	static constexpr int SWAPCHAIN_CAPACITY = 8;
	
	struct settings
	{
		bool use_compute;
		bool use_transfer;
		bool multisample;
		uint preferred_gpu;
		VkPhysicalDeviceFeatures gpu_features;
	};
	struct memory_pool
	{
		VkPtr        memory;
		u64          capacity;
		u64          length;
		memory_pool* next;
	};
	struct memory_chunk
	{
		VkPtr memory;
		u64 offset;
	};
	struct buffer
	{
		VkPtr handle;
		memory_chunk chunk;
		u64 capacity;
		u64 length;
		VkMemoryRequirements requirements;
	};
	struct buffer_chunk
	{
		VkPtr handle;
		u64 offset;
		memory_chunk chunk;
	};
	struct image
	{
		VkImageCreateInfo info;
		VkPtr image;
		memory_chunk chunk;
	};
	struct render_target : public image
	{
		VkFormat format;
		VkPtr view;

		// TODO: Improve datastructure when multiple views
		VkPtr combined_image_sampler;
		VkPtr input_attachment;
		VkPtr storage_image;
	};
	struct alignas(256) ubo
	{
		alignas(64) float4x4 model;
		alignas(64) float4x4 modelinv;
		alignas(64) float4x4 mvp;
		alignas(16) float4   color;
		alignas(16) float4   tex_st;
		alignas(16) float4   light_st;
		alignas(16) float4   eye;
	};

	struct alignas(128) ubo_shared
	{
		alignas(16) float4   eye;
	};
	struct alignas(128) ubo_vertex
	{

	};
	struct alignas(128) ubo_fragment
	{

	};
	struct alignas(128) push_constant
	{
		alignas(64) float4x4 mvp;
		alignas(16) float4   color;
		alignas(16) float4   tex_st;
		alignas(16) float4   light_st;
	};


	
	// Data
	libhandle libvulkan;
	void* instance;
	void* device;
	void* processor;
	
	buffer vertex_buffer;
	buffer index_buffer;
	buffer vertex_buffer_staging;
	buffer index_buffer_staging;
	buffer uniform_buffer_staging;

	struct
	{
		VkPtr    handle;
		uint2    extent;
		uint     index;
		uint     length;
		VkFormat format;
		VkPtr    images[SWAPCHAIN_CAPACITY];
		VkPtr    views[SWAPCHAIN_CAPACITY];
		VkPtr    fences[SWAPCHAIN_CAPACITY];
		VkPtr    frame_buffers[SWAPCHAIN_CAPACITY];
		void*    command_buffers[SWAPCHAIN_CAPACITY];
		buffer   uniform_buffers[SWAPCHAIN_CAPACITY];
	} swapchain;

	void* command_buffer;
	void* graphics_queue;
	void* presentation_queue;
	VkPtr render_pass;
	VkPtr graphics_pipeline;
	VkPtr command_pool;
	VkPtr request_semaphore;
	VkPtr present_semaphore;
	VkPtr timestamp_query_pool;
	buffer staging_buffer;
	VkViewport viewport;
	VkRect2D viewport_rect;
	
	
	// State
	memory::pool pool;
	memory_pool memory_pools[32];
	struct
	{
		
	} state;
	

	// TODO: Organize the following
	VkPtr descriptor_pool;
	VkPtr descritpor_sets[SWAPCHAIN_CAPACITY];
	VkPtr pipeline_layout;

	// Debug
	slice<VkExtensionProperties> extension_properties;
	slice<VkLayerProperties> layer_properties;
	VkPtr debug_messenger;
	bool debug_reports;
	bool debug_utils;
	bool debug_validation;
	
	// TODO: Restructure
	VkSurfaceFormatKHR formats[16];
	
	
	
	// Static procedures
	void* (*find_proc)(void*, const char*);
	VkResult (*vkEnumerateInstanceExtensionProperties)(const char*, uint*, void*);
	VkResult (*vkEnumerateInstanceLayerProperties)(uint*, void*);
	VkResult (*vkCreateInstance)(const void*, const void*, void**);

	// Local procedures
	void     (*vkDestroyInstance)(void*, const void*);
	VkResult (*vkEnumeratePhysicalDevices)(void*, uint*, void**);
	VkResult (*vkEnumerateDeviceExtensionProperties)(void*, const char*, uint*, void*);
	VkResult (*vkEnumerateDeviceLayerProperties)(void*, uint*, void*);
	void     (*vkGetPhysicalDeviceFeatures)(void*, void*);
	void     (*vkGetPhysicalDeviceFormatProperties)(void*, VkFormat, void*);
	VkResult (*vkGetPhysicalDeviceImageFormatProperties)(void*, VkFormat, VkImageType, VkImageTiling, flags32, flags32, void*);
	void     (*vkGetPhysicalDeviceProperties)(void*, void*);
	void     (*vkGetPhysicalDeviceQueueFamilyProperties)(void*, uint*, void*);
	void     (*vkGetPhysicalDeviceMemoryProperties)(void*, void*);
	VkResult (*vkGetPhysicalDeviceSurfaceSupportKHR)(void*, uint, VkPtr, uint*);
	VkResult (*vkGetPhysicalDeviceSurfaceCapabilitiesKHR)(void*, VkPtr, void*);
	VkResult (*vkGetPhysicalDeviceSurfaceFormatsKHR)(void*, VkPtr, uint*, void*);
	VkResult (*vkGetPhysicalDeviceSurfacePresentModesKHR)(void*, VkPtr, uint*, void*);
	VkResult (*vkGetImageMemoryRequirements)(void*, VkPtr, void*);
	VkResult (*vkGetBufferMemoryRequirements)(void*, VkPtr, void*);
	VkResult (*vkGetSwapchainImagesKHR)(void*, VkPtr, uint*, VkPtr*);
	void     (*vkGetDeviceQueue)(void*, uint, uint, void**);
	VkResult (*vkCreateSwapchainKHR)(void*, const void*, const void*, void*);
	VkResult (*vkCreateDevice)(void*, const void*, const void*, void**);
	VkResult (*vkCreateCommandPool)(void*, const void*, const void*, void*);
	VkResult (*vkCreateBuffer)(void*, const void*, const void*, void*);
	VkResult (*vkCreateSemaphore)(void*, const void*, const void*, void*);
	VkResult (*vkCreateFence)(void*, const void*, const void*, void*);
	VkResult (*vkCreateQueryPool)(void*, const void*, const void*, void*);
	VkResult (*vkCreateImage)(void*, const void*, const void*, void*);
	VkResult (*vkCreateImageView)(void*, const void*, const void*, void*);
	VkResult (*vkCreateDescriptorPool)(void*, const void*, const void*, VkPtr*);
	VkResult (*vkCreateDescriptorSetLayout)(void*, const void*, const void*, VkPtr*);
	VkResult (*vkCreatePipelineLayout)(void*, const void*, const void*, VkPtr*);
	VkResult (*vkCreateGraphicsPipelines)(void*, VkPtr, uint, const void*, const void*, VkPtr*);
	VkResult (*vkCreateSampler)(void*, const void*, const void*, VkPtr*);
	VkResult (*vkCreateShaderModule)(void*, const void*, const void*, VkPtr*);
	VkResult (*vkCreateRenderPass)(void*, const void*, const void*, VkPtr*);
	VkResult (*vkCreateFramebuffer)(void*, const void*, const void*, VkPtr*);
	void     (*vkUpdateDescriptorSets)(void*, uint, const void*, uint, const void*);
	VkResult (*vkAcquireNextImageKHR)(void*, VkPtr, u64, VkPtr, VkPtr, uint*);
	VkResult (*vkBindBufferMemory)(void*, VkPtr, VkPtr, u64);
	VkResult (*vkBindImageMemory)(void*, VkPtr, VkPtr, u64);
	VkResult (*vkQueueSubmit)(void*, uint, const void*, VkPtr);
	VkResult (*vkQueueWaitIdle)(void*);
	VkResult (*vkQueuePresentKHR)(void*, const void*);
	VkResult (*vkDeviceWaitIdle)(void*);
	VkResult (*vkAllocateCommandBuffers)(void*, const void*, void**);
	VkResult (*vkAllocateMemory)(void*, const void*, const void*, VkPtr*);
	VkResult (*vkAllocateDescriptorSets)(void*, const void*, VkPtr*);
	void     (*vkFreeMemory)(void*, VkPtr, const void*);
	VkResult (*vkMapMemory)(void*, VkPtr, u64, u64, flags32, void**);
	void     (*vkUnmapMemory)(void*, VkPtr);
	VkResult (*vkBeginCommandBuffer)(void*, const void*);
	VkResult (*vkEndCommandBuffer)(void*);
	VkResult (*vkWaitForFences)(void*, uint, const VkPtr*, uint, u64); 
	VkResult (*vkResetCommandPool)(void*, VkPtr, flags32);
	VkResult (*vkResetFences)(void*, uint, const VkPtr*);
	void     (*vkDestroyDevice)(void*, const void*);

	void     (*vkCmdBindPipeline)(const void*, flags32, VkPtr);
	void     (*vkCmdSetViewport)(const void*, uint, uint, const void*);
	void     (*vkCmdSetScissor)(const void*, uint, uint, const void*);
	void     (*vkCmdSetLineWidth)(const void*, float);
	void     (*vkCmdSetDepthBias)(const void*, float, float, float);
	void     (*vkCmdSetBlendConstants)(const void*, const float[4]);
	void     (*vkCmdSetDepthBounds)(const void*, float, float);
	void     (*vkCmdSetStencilCompareMask)(const void*, flags32, uint);
	void     (*vkCmdSetStencilWriteMask)(const void*, flags32, uint);
	void     (*vkCmdSetStencilReference)(const void*, flags32, uint);
	void     (*vkCmdBindDescriptorSets)(const void*, flags32, VkPtr, uint, uint, const VkPtr*, uint, const uint*);
	void     (*vkCmdBindIndexBuffer)(const void*, VkPtr, u64, flags32);
	void     (*vkCmdBindVertexBuffers)(const void*, uint, uint, const VkPtr*, const u64*);
	void     (*vkCmdDraw)(const void*, uint, uint, uint, uint);
	void     (*vkCmdDrawIndexed)(const void*, uint, uint, uint, int, uint);
	void     (*vkCmdDrawIndirect)(const void*, VkPtr, u64, uint, uint);
	void     (*vkCmdDrawIndexedIndirect)(const void*, VkPtr, u64, uint, uint);
	void     (*vkCmdDispatch)(const void*, uint, uint, uint);
	void     (*vkCmdDispatchIndirect)(const void*, VkPtr, u64);
	void     (*vkCmdCopyBuffer)(const void*, VkPtr, VkPtr, uint, const VkBufferCopy*);
	void     (*vkCmdCopyImage)(const void*, VkPtr, flags32, VkPtr, flags32, uint, const VkImageCopy*);
	void     (*vkCmdBlitImage)(const void*, VkPtr, flags32, VkPtr, flags32, uint, const VkImageBlit*, flags32);
	void     (*vkCmdCopyBufferToImage)(const void*, VkPtr, VkPtr, flags32, uint, const VkBufferImageCopy*);
	void     (*vkCmdCopyImageToBuffer)(const void*, VkPtr, flags32, VkPtr, uint, const VkBufferImageCopy*);
	void     (*vkCmdUpdateBuffer)(const void*, VkPtr, u64, u64, const void*);
	void     (*vkCmdFillBuffer)(const void*, VkPtr, u64, u64, uint);
	void     (*vkCmdClearColorImage)(const void*, VkPtr, flags32, const VkClearColorValue*, uint, const VkImageSubresourceRange*);
	void     (*vkCmdClearDepthStencilImage)(const void*, VkPtr, flags32, const VkClearDepthStencilValue*, uint, const VkImageSubresourceRange*);
	void     (*vkCmdClearAttachments)(const void*, uint, const VkClearAttachment*, uint, const VkClearRect*);
	void     (*vkCmdResolveImage)(const void*, VkPtr, flags32, VkPtr, flags32, uint, const VkImageResolve*);
	void     (*vkCmdSetEvent)(const void*, VkPtr, flags32);
	void     (*vkCmdResetEvent)(const void*, VkPtr, flags32);
	void     (*vkCmdWaitEvents)(const void*, uint, const VkPtr*, flags32, flags32, uint, const VkMemoryBarrier*, uint, const VkBufferMemoryBarrier*, uint, const VkImageMemoryBarrier*);
	void     (*vkCmdPipelineBarrier)(const void*, flags32, flags32, flags32, uint, const VkMemoryBarrier*, uint, const VkBufferMemoryBarrier*, uint, const VkImageMemoryBarrier*);
	void     (*vkCmdBeginQuery)(const void*, VkPtr, uint, flags32);
	void     (*vkCmdEndQuery)(const void*, VkPtr, uint);
	void     (*vkCmdResetQueryPool)(const void*, VkPtr, uint, uint);
	void     (*vkCmdWriteTimestamp)(const void*, flags32, VkPtr, uint);
	void     (*vkCmdCopyQueryPoolResults)(const void*, VkPtr, uint, uint, VkPtr, u64, u64, flags32);
	void     (*vkCmdPushConstants)(const void*, VkPtr, flags32, uint, uint, const void*);
	void     (*vkCmdBeginRenderPass)(const void*, const void*, flags32);
	void     (*vkCmdNextSubpass)(const void*, flags32);
	void     (*vkCmdEndRenderPass)(const void*);
	void     (*vkCmdExecuteCommands)(const void*, uint, const void*);


	error init_procedures()
	{
		#ifdef VK_INIT_FUNC
		#error VK_INIT_FUNC macro has already been defined, rename the local macro.
		#endif
		#define VK_INIT_FUNC(name) {\
			reinterpret_cast<void*&>(name) = find_proc(instance, #name);\
			if (!name) return error(0x00000001, "Could not find " #name "." FILE_LINE_FMT);\
		}
		
		VK_INIT_FUNC(vkDestroyInstance);
		VK_INIT_FUNC(vkEnumeratePhysicalDevices);
		VK_INIT_FUNC(vkEnumerateDeviceExtensionProperties);
		VK_INIT_FUNC(vkEnumerateDeviceLayerProperties);
		VK_INIT_FUNC(vkGetPhysicalDeviceFeatures);
		VK_INIT_FUNC(vkGetPhysicalDeviceFormatProperties);
		VK_INIT_FUNC(vkGetPhysicalDeviceImageFormatProperties);
		VK_INIT_FUNC(vkGetPhysicalDeviceProperties);
		VK_INIT_FUNC(vkGetPhysicalDeviceQueueFamilyProperties);
		VK_INIT_FUNC(vkGetPhysicalDeviceMemoryProperties);
		VK_INIT_FUNC(vkGetPhysicalDeviceSurfaceSupportKHR);
		VK_INIT_FUNC(vkGetPhysicalDeviceSurfaceCapabilitiesKHR);
		VK_INIT_FUNC(vkGetPhysicalDeviceSurfaceFormatsKHR);
		VK_INIT_FUNC(vkGetPhysicalDeviceSurfacePresentModesKHR);
		VK_INIT_FUNC(vkGetImageMemoryRequirements);
		VK_INIT_FUNC(vkGetBufferMemoryRequirements);
		VK_INIT_FUNC(vkGetSwapchainImagesKHR);
		VK_INIT_FUNC(vkGetDeviceQueue);
		VK_INIT_FUNC(vkCreateSwapchainKHR);
		VK_INIT_FUNC(vkCreateDevice);
		VK_INIT_FUNC(vkCreateCommandPool);
		VK_INIT_FUNC(vkCreateBuffer);
		VK_INIT_FUNC(vkCreateSemaphore);
		VK_INIT_FUNC(vkCreateFence);
		VK_INIT_FUNC(vkCreateQueryPool);
		VK_INIT_FUNC(vkCreateImage);
		VK_INIT_FUNC(vkCreateImageView);
		VK_INIT_FUNC(vkCreateDescriptorPool);
		VK_INIT_FUNC(vkCreateDescriptorSetLayout);
		VK_INIT_FUNC(vkCreateSampler);
		VK_INIT_FUNC(vkCreateShaderModule);
		VK_INIT_FUNC(vkCreateRenderPass);
		VK_INIT_FUNC(vkCreateFramebuffer);
		VK_INIT_FUNC(vkCreatePipelineLayout);
		VK_INIT_FUNC(vkCreateGraphicsPipelines);
		VK_INIT_FUNC(vkUpdateDescriptorSets);
		VK_INIT_FUNC(vkAcquireNextImageKHR);
		VK_INIT_FUNC(vkBindBufferMemory);
		VK_INIT_FUNC(vkBindImageMemory);
		VK_INIT_FUNC(vkQueueSubmit);
		VK_INIT_FUNC(vkQueueWaitIdle);
		VK_INIT_FUNC(vkQueuePresentKHR);
		VK_INIT_FUNC(vkDeviceWaitIdle);
		VK_INIT_FUNC(vkAllocateCommandBuffers);
		VK_INIT_FUNC(vkAllocateMemory);
		VK_INIT_FUNC(vkAllocateDescriptorSets);
		VK_INIT_FUNC(vkFreeMemory);
		VK_INIT_FUNC(vkMapMemory);
		VK_INIT_FUNC(vkUnmapMemory);
		VK_INIT_FUNC(vkBeginCommandBuffer);
		VK_INIT_FUNC(vkEndCommandBuffer);
		VK_INIT_FUNC(vkWaitForFences);
		VK_INIT_FUNC(vkResetCommandPool);
		VK_INIT_FUNC(vkResetFences);
		VK_INIT_FUNC(vkDestroyDevice);
		
		VK_INIT_FUNC(vkCmdBindPipeline);
		VK_INIT_FUNC(vkCmdSetViewport);
		VK_INIT_FUNC(vkCmdSetScissor);
		VK_INIT_FUNC(vkCmdSetLineWidth);
		VK_INIT_FUNC(vkCmdSetDepthBias);
		VK_INIT_FUNC(vkCmdSetBlendConstants);
		VK_INIT_FUNC(vkCmdSetDepthBounds);
		VK_INIT_FUNC(vkCmdSetStencilCompareMask);
		VK_INIT_FUNC(vkCmdSetStencilWriteMask);
		VK_INIT_FUNC(vkCmdSetStencilReference);
		VK_INIT_FUNC(vkCmdBindDescriptorSets);
		VK_INIT_FUNC(vkCmdBindIndexBuffer);
		VK_INIT_FUNC(vkCmdBindVertexBuffers);
		VK_INIT_FUNC(vkCmdDraw);
		VK_INIT_FUNC(vkCmdDrawIndexed);
		VK_INIT_FUNC(vkCmdCopyBuffer);
		VK_INIT_FUNC(vkCmdCopyImage);
		VK_INIT_FUNC(vkCmdBlitImage);
		VK_INIT_FUNC(vkCmdCopyBufferToImage);
		VK_INIT_FUNC(vkCmdCopyImageToBuffer);
		VK_INIT_FUNC(vkCmdUpdateBuffer);
		VK_INIT_FUNC(vkCmdFillBuffer);
		VK_INIT_FUNC(vkCmdClearColorImage);
		VK_INIT_FUNC(vkCmdClearDepthStencilImage);
		VK_INIT_FUNC(vkCmdClearAttachments);
		VK_INIT_FUNC(vkCmdResolveImage);
		VK_INIT_FUNC(vkCmdSetEvent);
		VK_INIT_FUNC(vkCmdResetEvent);
		VK_INIT_FUNC(vkCmdWaitEvents);
		VK_INIT_FUNC(vkCmdPipelineBarrier);
		VK_INIT_FUNC(vkCmdBeginQuery);
		VK_INIT_FUNC(vkCmdEndQuery);
		VK_INIT_FUNC(vkCmdResetQueryPool);
		VK_INIT_FUNC(vkCmdWriteTimestamp);
		VK_INIT_FUNC(vkCmdCopyQueryPoolResults);
		VK_INIT_FUNC(vkCmdPushConstants);
		VK_INIT_FUNC(vkCmdBeginRenderPass);
		VK_INIT_FUNC(vkCmdNextSubpass);
		VK_INIT_FUNC(vkCmdEndRenderPass);
		VK_INIT_FUNC(vkCmdExecuteCommands);
		
		return error::ok;
	}
};

#include "utils.hpp"

#if SOLITUDE_WIN
	#define __DEBUG_BASE_PATH "../../"
	#include "win/import.hpp"
#elif SOLITUDE_LINUX || SOLITUDE_FREEBSD
	#define __DEBUG_BASE_PATH "../"
	#include "unix/import.hpp"
#else
	#error Vulkan is not supported on the target platform. Change the SOLITUDE_VULKAN directive to 0 to disable Vulkan.
#endif


/** vulkan.hpp
 * Depends on some platform specific implementation:
 *  - platform_init: Assigns vulkan::libvulkan and vulkan::proc_of
 *  - platform_dispose: It should cleanup created data in platform_init(vulkan*) such as
 *  - platform_init_instance: 
 *  - platform_init_surface: 
 *  - platform_vulkan_debug_message_callback: 
 */
#include "vulkan.hpp"

error init_vulkan(struct graphics::api* api, struct video* video)
{
	struct error err;

	// Settings
	struct vulkan::settings settings {};
	settings.use_compute = false;
	settings.use_transfer = false;
	settings.multisample = false;
	settings.preferred_gpu = -1;
	settings.gpu_features.shaderStorageImageExtendedFormats = true;
	settings.gpu_features.samplerAnisotropy = true;

	// Initialize api
	api->type          = graphics::api::VULKAN;
	api->name          = "Vulkan";
	api->shared        = allocate_raw(&(api->shared_size = sizeof(struct vulkan)), memory::CLEAR);
	api->begin_frame   = vulkan_begin_frame;
	api->present_frame = vulkan_present_frame;
	api->dispose       = vulkan_dispose;

	var vulkan = (struct vulkan*)api->shared;

	
	// Initialize vulkan
	if (err = vulkan_init(api, video, settings))
	{
		print("An error occured while initializing vulkan: %v", err);
		api->type = graphics::api::UNKNOWN;
		deallocate_raw(api->shared, api->shared_size);
		return err;
	}
	return error::ok;
}




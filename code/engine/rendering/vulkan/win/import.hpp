error platform_init(vulkan* vk)
{
	var handle = LoadLibrary("vulkan-1.dll");
	if (!handle)
		return error(0x00000001, "Could not open vulkan-1.dll using LoadLibrary.");
	
	var find_proc = GetProcAddress(handle, "vkGetInstanceProcAddr");
	if (!find_proc)
	{
		find_proc = GetProcAddress(handle, "_vkGetInstanceProcAddr");
		if (!find_proc)
		{
			FreeLibrary(handle);
			return error(0x00000001, "Could not find symbol vkGetInstanceProcAddr nor _vkGetInstanceProcAddr using dlsym.");
		}
	}
	
	vk->libvulkan = handle;
	reinterpret_cast<void*&>(vk->find_proc) = find_proc;
	return error::ok;
}

error platform_init_instance(vulkan* vk, const char* extensions[], int* extension_count, const char* layers[], int* layer_count)
{
	var count = *extension_count;
	extensions[count++] = "VK_KHR_surface";
	extensions[count++] = "VK_KHR_win32_surface";

	var extension_properties = vk->extension_properties;
	foreach(var ep, extension_properties)
	{
		print("Extension Property (%v): %v", ep.specVersion, ep.extensionName);
	}

	*extension_count = count;
	return error::ok;
}

error platform_init_surface(vulkan* vk, struct video* video, VkPtr* result)
{
	VkResult vk_result;
	struct {
		VkStructureType sType;
		const void* pNext;
		flags32 flags;
		HINSTANCE dpy;
		HWND window;
	} info = {};
	info.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
	info.dpy = video->display;
	info.window = video->window;

	VkResult (*vkCreateWin32SurfaceKHR)(void*,const void*, const void*, void*);
	reinterpret_cast<void*&>(vkCreateWin32SurfaceKHR) = vk->find_proc(vk->instance, "vkCreateWin32SurfaceKHR");
	if (!vkCreateWin32SurfaceKHR)
		return error(0x00000001, "Could not find vkCreateWin32SurfaceKHR.");

	if (vk_result = vkCreateWin32SurfaceKHR(vk->instance, &info, null, result))
		return error(0x00000001, VkResultToString(vk_result));
	
	return error::ok;
}

error platform_dispose(vulkan* vk)
{
	if (vk->libvulkan)
	{
		FreeLibrary(vk->libvulkan);
		vk->libvulkan = null;
	}
	return error::ok;
}

uint platform_vulkan_debug_message_callback(flags32 severity, flags32 type, const VkDebugUtilsMessengerCallbackDataEXT* data, void* user_data)
{
	print(data->pMessage);
	return 0;
}

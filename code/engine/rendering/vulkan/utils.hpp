inline vulkan::memory_chunk request_memory(vulkan* vk, VkMemoryRequirements requirements, flags32 flags)
{
	uint type_index = UINTMAX;
	VkPhysicalDeviceMemoryProperties properties;
	vk->vkGetPhysicalDeviceMemoryProperties(vk->processor, &properties);
	iterate(i, properties.memoryTypeCount)
	{
		if ((requirements.memoryTypeBits & (1 << i)) && (properties.memoryTypes[i].propertyFlags & flags) == flags)
		{
			type_index = i;
			break;
		}
	}
	
	var pool = vk->memory_pools + type_index;
	if (pool->capacity == 0)
	{
		*pool = {};
		iterate(i, 2)
		{
			u64 capacity = vulkan::MEMORY_CHUNK_CAPACITY / (1 << i);
			VkMemoryAllocateInfo memoryAllocInfo = {};
			memoryAllocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
			memoryAllocInfo.pNext = null;
			memoryAllocInfo.allocationSize = capacity;
			memoryAllocInfo.memoryTypeIndex = type_index;
			
			VkPtr memory;
			VkResult result = vk->vkAllocateMemory(vk->device, &memoryAllocInfo, null, &memory);
			if (result == VK_SUCCESS)
			{
				pool->memory = memory;
				pool->capacity = capacity;
				break;
			}
		}

		assert(pool->capacity);
	}

	u64 aligned_offset = ALIGN_POW2(pool->length, requirements.alignment);
	u64 aligned_size   = ALIGN_POW2(requirements.size, requirements.alignment);
	assert(aligned_size <= pool->capacity - aligned_offset);
	pool->length = aligned_offset + aligned_size;

	vulkan::memory_chunk chunk;
	chunk.memory = pool->memory;
	chunk.offset = aligned_offset;
	return chunk;
}



inline vulkan::buffer create_buffer(vulkan* vk, u64 size, flags32 usage, flags32 flags)
{
	VkResult result;
	VkPtr buffer = null;
	{
		VkBufferCreateInfo bufCreateInfo = {};
		bufCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
		bufCreateInfo.size = size;
		bufCreateInfo.usage = usage;
		bufCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
		result = vk->vkCreateBuffer(vk->device, &bufCreateInfo, null, &buffer);
		assert(result == VK_SUCCESS);
	}

	VkMemoryRequirements requirements;
	vk->vkGetBufferMemoryRequirements(vk->device, buffer, &requirements);
	var chunk = request_memory(vk, requirements, flags);
	vk->vkBindBufferMemory(vk->device, buffer, chunk.memory, chunk.offset);

	vulkan::buffer b;
	b.handle = buffer;
	b.chunk = chunk;
	b.capacity = size;
	b.length = 0;
	b.requirements = requirements;
	return b;
}



inline u64 push(vulkan* vk, vulkan::buffer* buffer, void* data, u64 length)
{
	var offset = buffer->length;
	var chunk = buffer->chunk;
	void* memory;
	VkResult result = vk->vkMapMemory(vk->device, chunk.memory, chunk.offset + offset, length, 0, &memory);
	assert(result == VK_SUCCESS);
	copy(memory, data, length);
	vk->vkUnmapMemory(vk->device, chunk.memory);
	buffer->length += length;
	return offset;
}
/*inline void update(vulkan* vk, vulkan::buffer* buffer, void* data, u64 length)
{
    vulkan_buffer_chunk staging = getStagingBufferChunk(size);

    void* memory;
    VkResult result = vkMapMemory(renderPath->device, staging.chunk.deviceMemory, staging.chunk.offset + staging.offset, size, 0, &memory);
    Assert(result == VK_SUCCESS);
    Copy(size, data, memory);
    vkUnmapMemory(renderPath->device, staging.chunk.deviceMemory);

    VkBufferCopy copyRegion = {};
    copyRegion.size = size;
    copyRegion.srcOffset = staging.offset;
    vkCmdCopyBuffer(renderPath->commandBuffer, staging.buffer, buffer, 1, &copyRegion);
}*/



inline VkPtr create_image_view(vulkan* vk, VkPtr image, VkFormat format, flags32 aspect_flags, VkImageViewType type)
{
	VkImageViewCreateInfo viewInfo = {};
	viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	viewInfo.image = image;
	viewInfo.viewType = type;
	viewInfo.format = format;
	viewInfo.subresourceRange.aspectMask = aspect_flags;
	viewInfo.subresourceRange.baseMipLevel = 0;
	viewInfo.subresourceRange.levelCount = 1;
	viewInfo.subresourceRange.baseArrayLayer = 0;
	viewInfo.subresourceRange.layerCount = 1;

	VkPtr view {};
	VkResult result = vk->vkCreateImageView(vk->device, &viewInfo, null, &view);
	assert(result == VK_SUCCESS);
	return view;
}



inline vulkan::image create_image(vulkan* vk, uint3 extent, VkFormat format, VkImageTiling tiling, flags32 usage, flags32 flags, VkSampleCountFlagBits samples = VK_SAMPLE_COUNT_1_BIT)
{
	VkPtr image;
	VkImageCreateInfo imageInfo{};
	imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	imageInfo.imageType = extent.y <= 1 
		? VK_IMAGE_TYPE_1D
		: (extent.z <= 1 ? VK_IMAGE_TYPE_2D : VK_IMAGE_TYPE_3D);
	imageInfo.extent = extent;
	if (extent.z == 0)
		imageInfo.extent.z = 1;
	imageInfo.mipLevels = 1;
	imageInfo.arrayLayers = 1;
	imageInfo.format = format;
	imageInfo.tiling = tiling;
	imageInfo.initialLayout = VK_IMAGE_LAYOUT_PREINITIALIZED;
	imageInfo.usage = usage;
	imageInfo.samples = samples;
	imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	
	VkImageFormatProperties properties;
	vk->vkGetPhysicalDeviceImageFormatProperties(vk->processor, format, imageInfo.imageType, tiling, imageInfo.usage, imageInfo.flags, &properties);
	VkResult result = vk->vkCreateImage(vk->device, &imageInfo, null, &image);
	assert(result == VK_SUCCESS);

	VkMemoryRequirements memory_requirements;
	vk->vkGetImageMemoryRequirements(vk->device, image, &memory_requirements);
	var chunk = request_memory(vk, memory_requirements, flags);
	vk->vkBindImageMemory(vk->device, image, chunk.memory, chunk.offset);

	vulkan::image i;
	i.info = imageInfo;
	i.image = image;
	i.chunk = chunk;
	return i;
}



/*inline VkPtr descriptor_of(vulkan* vk, VkDescriptorType type)
{
	switch (type)
	{
		case VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER: return vk->descriptor_combined_image_sampler;
		case VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT: return vk->descriptor_input_attachment;
		case VK_DESCRIPTOR_TYPE_STORAGE_IMAGE: return vk->descriptor_storage_image;
	}
	return null;
}



inline VkPtr create_descriptor(vulkan* vk, VkPtr view, VkPtr previous, VkDescriptorType layout)
{
	VkPtr set = previous;
	if (!set)
	{
		var descriptor = descriptor_of(vk, layout);
		VkDescriptorSetAllocateInfo info {};
		info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
		info.descriptorPool = vk->descriptor_pool;
		info.descriptorSetCount = 1;
		info.pSetLayouts = &descriptor;
		VkResult result = vk->vkAllocateDescriptorSets(vk->device, &info, &set);
		assert(result == VK_SUCCESS);
	}
	
	{
		VkDescriptorImageInfo info {};
		info.imageView = view;
		info.imageLayout = layout == VK_DESCRIPTOR_TYPE_STORAGE_IMAGE
			? VK_IMAGE_LAYOUT_GENERAL
			: VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		info.sampler = layout == VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER
			? vk->sampler
			: null;

		constexpr int write_count = 1;
		VkWriteDescriptorSet writes[write_count] {};
		writes[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		writes[0].dstSet = set;
		writes[0].dstBinding = 0;
		writes[0].dstArrayElement = 0;
		writes[0].descriptorType = layout;
		writes[0].descriptorCount = 1;
		writes[0].pImageInfo = &info;

		vk->vkUpdateDescriptorSets(vk->device, write_count, writes, 0, null);
	}

	return set;
}


inline vulkan::render_target create_render_target(vulkan* vk, uint2 extent, VkFormat format, flags32 usage, flags32 aspect = VK_IMAGE_ASPECT_COLOR_BIT, VkSampleCountFlagBits samples = VK_SAMPLE_COUNT_1_BIT)
{
	var image = create_image(vk, {extent.x, extent.y, 0}, format, VK_IMAGE_TILING_OPTIMAL, usage, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, samples); 
	var view = create_image_view(vk, image.image, format, aspect, VK_IMAGE_VIEW_TYPE_2D);

	vulkan::render_target result {image, format, view, 0,0,0};
	if (usage & VK_IMAGE_USAGE_SAMPLED_BIT)
		result.combined_image_sampler = create_descriptor(vk, view, null, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER);
	if (usage & VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT)
		result.input_attachment = create_descriptor(vk, view, null, VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT);
	if (usage & VK_IMAGE_USAGE_STORAGE_BIT)
		result.storage_image = create_descriptor(vk, view, null, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE);

	return result;
}

*/




// TODO: Remove this function!!!!
error create_stage(vulkan* vk, const char* shader_path, VkShaderStageFlagBits stage, VkPipelineShaderStageCreateInfo* shader)
{
	VkResult vk_result;

	void* file = fopen(shader_path, "rb");
	if (!file)
		return error(0x00000001, "Could not find file");
	
	s32 size;
	fseek(file, 0, SEEK_END);
	size = ftell(file);
	fseek(file, 0, SEEK_SET);
	
	auto data = malloc(size);
	fread(data, 1, size, file);
	fclose(file);
	
	VkShaderModuleCreateInfo info {};
	info.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	info.codeSize = size;
	info.pCode = (uint*)data;

	VkPipelineShaderStageCreateInfo result {};
	result.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	result.stage = stage;
	result.pName = "main";
	
	if (vk_result = vk->vkCreateShaderModule(vk->device, &info, null, &result.module))
		return error(0x00000001, VkResultToString(vk_result));
		
	*shader = result;
	return error::ok;
}





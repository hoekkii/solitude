error platform_init(vulkan* vk)
{
	var handle = dlopen("libvulkan.so", RTLD_NOW|RTLD_LOCAL);
	if (!handle)
	{
		handle = dlopen("libvulkan.so.1", RTLD_NOW|RTLD_LOCAL);
		if (!handle)	
			return error(0x00000001, "Could not open libvulkan.so nor libvulkan.so.1 using dlopen with arguments RTLD_NOW|RTLD_LOCAL.");
	}
	
	var find_proc = dlsym(handle, "vkGetInstanceProcAddr");
	if (!find_proc)
	{
		find_proc = dlsym(handle, "_vkGetInstanceProcAddr");
		if (!find_proc)
		{
			dlclose(handle);
			return error(0x00000001, "Could not find symbol vkGetInstanceProcAddr nor _vkGetInstanceProcAddr using dlsym.");
		}
	}
	
	vk->libvulkan = handle;
	reinterpret_cast<void*&>(vk->find_proc) = find_proc;
	return error::ok;
}

error platform_init_instance(vulkan* vk, const char* extensions[], int* extension_count, const char* layers[], int* layer_count)
{
	var count = *extension_count;
	extensions[count++] = "VK_KHR_surface";
	extensions[count++] = "VK_KHR_xlib_surface";

	var extension_properties = vk->extension_properties;
	foreach(var ep, extension_properties)
	{
		print("Extension Property (%v): %v", ep.specVersion, ep.extensionName);
	}

	*extension_count = count;
	return error::ok;
}

error platform_init_surface(vulkan* vk, struct video* video, VkPtr* result)
{
	VkResult vk_result;
	struct {
		VkStructureType sType;
		const void* pNext;
		flags32 flags;
		void* dpy;
		uptr window;
	} info = {};
	info.sType = VK_STRUCTURE_TYPE_XLIB_SURFACE_CREATE_INFO_KHR;
	info.dpy = video->display;
	info.window = video->window;

	VkResult (*vkCreateXlibSurfaceKHR)(void*,const void*, const void*, void*);
	reinterpret_cast<void*&>(vkCreateXlibSurfaceKHR) = vk->find_proc(vk->instance, "vkCreateXlibSurfaceKHR");
	if (!vkCreateXlibSurfaceKHR)
		return error(0x00000001, "Could not find vkCreateXlibSurfaceKHR.");

	if (vk_result = vkCreateXlibSurfaceKHR(vk->instance, &info, null, result))
		return error(0x00000001, VkResultToString(vk_result));
	
	return error::ok;
}

error platform_dispose(vulkan* vk)
{
	if (vk->libvulkan)
	{
		dlclose(vk->libvulkan);
		vk->libvulkan = null;
	}
	return error::ok;
}

uint platform_vulkan_debug_message_callback(flags32 severity, flags32 type, const VkDebugUtilsMessengerCallbackDataEXT* data, void* user_data)
{
	print(data->pMessage);
	return 0;
}

#include "api.hpp"
#include "wasm.hpp"

struct gles : public graphics::share
{
	int test;
};


void gles_begin_frame(struct graphics::api* api)
{
	var gles = (struct gles*)api->shared;
	gles->test++;
	
	float b = (gles->test & 0xFF) / (float)(0xFF);
	glClearColor(0, 0, b, 1);
	glClear(GL_COLOR_BUFFER_BIT);
}

void gles_present_frame(struct graphics::api* api)
{

}

static error gles_dispose(graphics::api* api)
{
	
	return error::ok;
}

static error init_gles(graphics::api* api, struct video* video)
{
	api->type          = graphics::api::GLES;
	api->name          = "GLES";
	api->begin_frame   = gles_begin_frame;
	api->present_frame = gles_present_frame;
	api->dispose       = gles_dispose;
	api->shared        = allocate_raw(&(api->shared_size = sizeof(struct gles)), memory::CLEAR);
	
	EmscriptenWebGLContextAttributes attributes {};
	attributes.alpha = 0;
	attributes.depth = 1;
	attributes.stencil = 1;
	attributes.antialias = 1;
	attributes.premultipliedAlpha = 0;
	attributes.preserveDrawingBuffer = 0;
	attributes.powerPreference = EmscriptenWebGLContextAttributes::POWER_DEFAULT;
	attributes.failIfMajorPerformanceCaveat = 0;
	attributes.majorVersion = 1;
	attributes.minorVersion = 0;
	attributes.enableExtensionsByDefault = 0;
	
	int context = emscripten_webgl_create_context("#canvas", &attributes);
	if (!context)
		return error(0, "[GRAPHICS:GLES] Failed to initialize");
	
	emscripten_webgl_make_context_current(context);
	return error::ok;
}

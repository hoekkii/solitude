struct EmscriptenWebGLContextAttributes
{
	static constexpr int POWER_DEFAULT = 0;
	static constexpr int POWER_LOW = 1;
	static constexpr int POWER_HIGH = 2;
	
	static constexpr int PROXY_DISALOW = 0;
	static constexpr int PROXY_FALLBACK = 1;
	static constexpr int PROXY_ALWAYS = 2;
	
	int alpha;
	int depth;
	int stencil;
	int antialias;
	int premultipliedAlpha;
	int preserveDrawingBuffer;
	int powerPreference;
	int failIfMajorPerformanceCaveat;

	int majorVersion;
	int minorVersion;

	int enableExtensionsByDefault;
	int explicitSwapControl;
	int proxyContextToMainThread;
	int renderViaOffscreenBackBuffer;
};

foreign int emscripten_webgl_create_context(const char *target, const EmscriptenWebGLContextAttributes *attributes);
foreign int emscripten_webgl_make_context_current(int context);

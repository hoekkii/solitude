
struct vertex
{
	float4 position;
	float4 tangent;
	float3 normal;
	float2 uv0; // albedo texture
	float2 uv1; // lightmap texture
	float4 color; // TODO: maybe pack as uint?
	//ushort light_index;
};

#include "primitives.hpp"

struct graphics
{
	struct share {};
	struct api
	{
		static constexpr int UNKNOWN = 0;
		static constexpr int GL = 1;
		static constexpr int GLES = 2;
		static constexpr int VULKAN = 3;
		typedef error initproc(struct api*, struct video*);

		int type;
		void* handle;
		string_slice name;
		void (*begin_frame)(struct api*);
		void (*present_frame)(struct api*);
		error (*dispose)(struct api*);
		uptr shared_size;
		void* shared;
	};
};


#if SOLITUDE_GL
#include "gl/import.hpp"
#else
error init_gl(graphics::api*, struct video*) { return error(0x00000001, "GL is marked disabled with the SOLITUDE_GL directive."); }
#endif


// Make a difference between WASM and the other platform GLES api
#if SOLITUDE_GLES
#include "gles/import.hpp"
#else
error init_gles(graphics::api*, struct video*) { return error(0x00000001, "GLES is marked disabled with the SOLITUDE_GLES directive."); }
#endif


#if SOLITUDE_VULKAN
#include "vulkan/import.hpp"
#else
error init_vulkan(graphics::api*, struct video*) { return error(0x00000001, "Vulkan is marked disabled with the SOLITUDE_VULKAN macro."); }
#endif


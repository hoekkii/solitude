



/**
 * The asset has to have its endresult data.
 * It has to have a link to its (meta) file in case the editor makes changes.
 * It is unknown how long it takes to load the asset (from disk, or web request).
 * It has to be known how to reload the unloaded asset
 * It is useful to know what the lifetime of the asset will be; shader data only has to exist for a couple of instructions.
 * Maybe like multiple assetfiles with all their own table of contents
 * 
 * Different serialization per platform
 */
struct asset
{
	struct font
	{

	};
	struct mesh
	{

	};
	struct bitmap
	{

	};
	struct sound
	{

	};
	
	// Type
	static constexpr int UNKNOWN = 0; // 
	static constexpr int FAILED  = 1; // 
	static constexpr int LOADING = 2; // 
	static constexpr int RAW     = 3; // 
	static constexpr int BITMAP  = 4; // 
	static constexpr int FONT    = 5; // 

	// Flags
	static constexpr int ALIVE   = 1 << 0; // 
	static constexpr int REAL    = 1 << 1; // 
									  //
	// Job
	static constexpr int MAX_TEMP_JOBS = 8;
	static constexpr flags32 FREE = 0;
	static constexpr flags32 USED = 1 << 17;

	union
	{
		int type;
		volatile int volatile_type;
	};

	flags32 flags;

	union
	{
		struct error error;
		struct buffer_f raw;
	};
	
	struct file
	{
		string_slice path;
	} EDITOR_PARAM(file_);
	
	struct handle
	{
		union
		{
			struct
			{
				short id;
				short version;
			};
			uint hash;
		};
	};

	struct table
	{
		// handles
	};
	
	struct meta
	{
		int type;
		string_slice name;
	} meta;
	
	
	struct job
	{
		struct memory::job* job;
		struct asset* asset;
	};

	struct sjob : public memory::unmanaged
	{
		flags32 state;
	};
	
	// TODO: Where to put safe-data
	struct api
	{
		static constexpr int UNKNOWN = 0;
		static constexpr int FS      = 1;
		static constexpr int WEB     = 2;
		typedef ::error initproc(struct api*, struct engine*);
		//typedef ::error (asset::api::*initproc)(struct engine*);

		int type;
		void* handle;
		string_slice name;
		sjob temp_jobs[MAX_TEMP_JOBS];

		asset::handle(*find)(asset::api*, string_slice);
		job (*load)(asset::api*, asset::handle);
		void (*temp)(asset::api*, asset::sjob*, string_slice, flags32);
		void (*unload)(asset::api*, asset::handle);
		void (*dispose)(asset::api*);

		byte shared[1024];
	};
};

/**
 * Should be called on the main thread
 */
static asset::sjob* temp(asset::api* api, string_slice path, flags32 flags)
{
	var jobs = api->temp_jobs;
	for (var i = 0; i < asset::MAX_TEMP_JOBS; i++)
	{
		var job = &jobs[i];
		if (job->state == asset::FREE)
		{
			job->state = asset::USED;
			job->flags = memory::DEFAULT_FLAGS;
			job->length = 0;
			api->temp(api, job, path, flags);
			return job;
		}
	}

	// TODO: Manage jobs
	assert(!"All File System job positions taken! Increase the job size, or do less concurrent jobs.");
	return null;
}
static flags32 wait(asset::sjob* job, int timeout)
{
	// TODO: Implement this
	assert(job->state & asset::ALIVE);
	return 0;
}
static void done(asset::sjob* job)
{
	job->state = asset::FREE;
}

// TODO: fix handle, this is not correct!!!
inline asset::handle find(asset::api* api, string_slice handle) { return api->find(api, handle); }
inline asset::job load(asset::api* api, asset::handle handle) { return api->load(api, handle); }
inline void unload(asset::api* api, asset::handle handle) { api->unload(api, handle); }
inline void dispose(asset::api* api)
{
	if (api->dispose)
	{
		api->dispose(api);
		api->dispose = null;
	}
}


#if SOLITUDE_WINFS
#include "fs.hpp"
#else
error fs_init(asset::api*, engine*) { return error(0x00000001, "File System is not available on this platform."); }
#endif

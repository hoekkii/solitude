
struct fs
{
	static constexpr u64 MAX_NAME_LENGTH = 1024;
	static constexpr u64 POOL_CAPACITY = 1024;
	static constexpr flags32 ASSET = 1 << 0;

	struct file
	{
		flags32 flags;
		asset::handle handle;
		string_slice name;
		string_slice path;
	};

	struct filepool
	{
		int length;
		file files[POOL_CAPACITY];
		filepool* previous;
	} *current;
	
	string_slice asset_path;
	string path;
	// serialize
	// deserialize

	memory::pool pool;
	asset::handle counter;
};


static void fs_temp(asset::api* api, asset::sjob* job, string_slice pathname, flags32 flags)
{
	assert(job->state & asset::USED);

	auto fs = reinterpret_cast<struct fs*>(api->shared);
	var path = fs->path;
	append(&path, pathname);
	terminate(path);
	
	var f = fopen((char*)path.data, "rb");
	if (!f)
	{
		reserve(job, sizeof(error), job->flags);
		job->state |= asset::FAILED;
		var err = (error*)job->data;
		*err = error(0x00000001, "Failed to open \"%v\". File not found.", path);
		return;
	}

	fseek(f, 0, SEEK_END);
	uptr length = ftell(f);
	fseek(f, 0, SEEK_SET);

	uptr capacity = length + 1;
	reserve(job, capacity, job->flags);
	fread(job->data, 1, length, f);
	fclose(f);

	terminate(*job);
	job->length = length;
	job->state |= asset::ALIVE;
	print("45PADSF %v / %v", length, job->capacity);
}



// fs_reload: load new asset before destroying the old one, to increase art iteration speed
static asset::handle fs_find(asset::api* api, string_slice str)
{
	auto fs = reinterpret_cast<struct fs*>(api->shared);
	var current = fs->current;
	while (current)
	{
		for (int i = 0; i < fs::POOL_CAPACITY; i++)
		{
			var file = current->files[i];
			if (equals(str, file.path))
				return file.handle;
		}
		current = current->previous;
	}
	return {};
}

// TODO: make this usable
static asset::job fs_load(asset::api* api, asset::handle handle)
{
	return {};
	/*auto fs = reinterpret_cast<struct fs*>(api->shared);
	var current = fs->current;
	while (current)
	{
		for (int i = 0; i < fs::POOL_CAPACITY; i++)
		{
			var file = current->files[i];
			if (file.handle.id == handle.id)
			{
				// TODO: Remove data from thing
				var path = fs->path;
				append(&path, file.path);
				terminate(path);


				// Create memory job
				memory::job* memjob{};
				var memjobs = fs->jobs;
				for (var i = 0; i < fs::MAX_JOBS; i++)
				{
					if (memjobs[i].flags != memory::JOB_FREE)
						continue;

					memjob = memjobs + i;
					memjob->flags = memory::JOB_WORKING;
					break;
				}


				struct asset* asset = (struct asset*)push(&fs->pool, sizeof(struct asset)).data;

				asset::job result{};
				result.job = memjob;
				result.asset = asset;

				// TODO: Run on thread

				raw.length = size;

				memjob->flags = memory::JOB_DONE;
				asset->type = asset::RAW;
				asset->raw = raw;
				return result;
			}
		}
		current = current->previous;
	}

	return {};*/
}
static void fs_unload(asset::api* api, asset::handle handle)
{
	return;
}
static void fs_dispose(asset::api* api)
{
	return;
}

static void fs_add(struct fs* fs, struct fs::file file)
{
	var current = fs->current;
	if (current->length >= fs::POOL_CAPACITY)
	{
		fs->current = (fs::filepool*)push(&fs->pool, sizeof(fs::filepool)).data;
		fs->current->length = 0;
		fs->current->previous = current;
		current = fs->current;
	}

	current->files[current->length] = file;
	current->length++;
}

static void fs_add(struct fs* fs, string_slice name, string_slice directory, string_slice root)
{
	// Construct fs file
	var full_length = directory.length - root.length + 1 + name.length + 1;
	string filepath = push(&fs->pool, full_length);
	append(&filepath, substring(directory, root.length));
	append(&filepath, '/');
	append(&filepath, name);
	terminate(filepath);

	// 
	fs::file fs_file{};
	fs_file.flags = fs::ASSET;
	fs_file.handle.id = ++fs->counter.id;
	fs_file.path = filepath;
	fs_file.name = substring(filepath, filepath.length - name.length);

	// 
	fs_add(fs, fs_file);
}

u64 fs_parentpath(buffer_f b)
{
	for (u64 i = b.length - 2; i > 0; --i)
	{
		char c = b[i];
		if (c == '/' || c == '\\')
			return i + 1;
	}

	return 0;
}



#if SOLITUDE_WIN
void files_enumerator(struct fs* fs, string root, string directory)
{
	var search_pattern = directory;
	append(&search_pattern, "/*");
	terminate(search_pattern);

	WIN32_FIND_DATAA file;
	var search_handle = FindFirstFileA((LPCSTR)search_pattern.data, &file);
	if (search_handle == INVALID_HANDLE_VALUE)
		return;

	do
	{
		// Filter
		string_slice name = file.cFileName;
		if (name[0] == '.')
			continue;

		fs_add(fs, name, directory, root);

		// Search subdirectory
		search_pattern = directory;
		append(&search_pattern, '/');
		append(&search_pattern, name);
		terminate(search_pattern);
		files_enumerator(fs, root, search_pattern);
	} while (FindNextFileA(search_handle, &file));

	if (GetLastError() != 18)
		CloseHandle(search_handle);
}
#elif SOLITUDE_UNIX
void files_enumerator(struct fs* fs, string root, string directory)
{
	terminate(directory);
	if (var dir = opendir(directory.data))
	{
		while (var entry = readdir(dir))
		{
			string_slice name = entry->name;
			if (!name.length || name[0] == '.' || name[0] == '/')
				continue;
			
			print("inode:%H  namelen:%v  type:%v  name:%v", entry->inode, entry->name_length, entry->type, entry->name);
			fs_add(fs, name, directory, root);
			
			// Search subdirectory
			var search_pattern = directory;
			append(&search_pattern, '/');
			append(&search_pattern, name);
			terminate(search_pattern);
			files_enumerator(fs, root, search_pattern);
		};
		closedir(dir);
	}
}
#else
#error files_enumerator unsupported on the platform.
#endif

error fs_init(asset::api* api, struct engine* engine)
{
	static_assert(sizeof(fs) < sizeof(asset::api::shared), "Increase the shared size in order to support assets on fs.");

	error err;
	api->type = asset::api::FS;
	api->name = "File System";
	api->find = fs_find;
	api->load = fs_load;
	api->temp = fs_temp;
	api->unload = fs_unload;
	api->dispose = fs_dispose;

	var fs = (struct fs*)api->shared;
	fs->pool = memory::default_pool();
	
	// Path https://stackoverflow.com/questions/1528298/get-path-of-executable
	memory::temp memory;
	#if SOLITUDE_WIN
	{
		memory = temp(&fs->pool, 1024);
		for (;;)
		{
			memory.length = GetModuleFileNameA(0, (LPSTR)memory.data, memory.capacity);
			if (memory.length < memory.capacity)
				break;

			var err = GetLastError();
			if (memory.length == memory.capacity && (err == ERROR_INSUFFICIENT_BUFFER || err == ERROR_SUCCESS))
			{
				memory = temp(&fs->pool, memory.capacity * 2);
			}
			else
			{
				memory.length = -1;
				break;
			}
		}
	}
	#elif SOLITUDE_BSD
	{
		// usr/include/sys/sysctl.h
		constexpr int name_length = 4;
		int name[name_length] {
		 	 1, // CTL_KERN
			14, // KERN_PROC
			12, // KERN_PROC_PATH_NAME
			-1,
		};
		uptr ctl_length;
		if (sysctl(name, name_length, null, &ctl_length, null, 0) == -1)
			memory.length = -1;
		else
		{
			memory = temp(&fs->pool, ctl_length + 1);
			memory.length = sysctl(name, name_length, memory.data, &ctl_length, null, 0) == -1
				? -1 : ctl_length;
		}
	}
	#elif SOLITUDE_LINUX
	{
		memory = temp(&fs->pool, 1024);
		for (;;)
		{
			memory.length = readlink("/proc/self/exe", memory.data, memory.capacity);
			if (memory.length < memory.capacity)
				break;

			memory = temp(&fs->pool, memory.capacity * 2);
		}
	}
	#else
	#error fs_init unsupported on the platform.
	#endif

	// Fallback on the executable arguments
	if (memory.length < 0)
	{
		var arguments = platform_arguments();
		if (arguments.length < 1)
			return error(0x00000001, "Failed to get the current executable location.");

		memory.data = (byte*)arguments[0];
		memory.length = len(arguments[0]);
		memory.capacity = 0;
	}


	// Reserve the correct size
	var asset_directory = string_slice("assets/");
	var buffer_capacity = ceil_to_pagesize(memory.length + asset_directory.length + fs::MAX_NAME_LENGTH);
	var buffer = push(&fs->pool, buffer_capacity);
	buffer.length = fs_parentpath(memory);
	copy(buffer.data, memory.data, buffer.length);

	// Traferse back from the executable until the asset directory is found
	static constexpr int asset_length = 6;
	static constexpr int asset_capacity = asset_length + 1;
	#if SOLITUDE_WIN
	for (;;)
	{
		copy(buffer.data + buffer.length, "assets", asset_capacity);
		var attributes = GetFileAttributesA((LPCSTR)buffer.data);
		if (attributes != INVALID_FILE_ATTRIBUTES && attributes == FILE_ATTRIBUTE_DIRECTORY)
		{
			buffer.length += asset_length;
			break;
		}

		buffer.length = fs_parentpath(buffer);
		if (!buffer.length)
			return error(0x00000001, "Could not find the assets folder.");
	}
	#elif SOLITUDE_BSD || SOLITUDE_LINUX
	for (;;)
	{
		copy(buffer.data + buffer.length, "assets", asset_capacity);
		var dir = opendir(buffer.data);
		if (dir)
		{
			closedir(dir);
			buffer.length += asset_length;
			break;
		}
		
		buffer.length = fs_parentpath(buffer);
		if (!buffer.length)
			return error(0x00000001, "Could not find the assets folder.");
	}
	#else
	#error fs_init unsupported on the platform
	#endif

	// Initialize all assets
	fs->path = buffer;
	fs->asset_path = buffer;
	fs->current = (fs::filepool*)push(&fs->pool, sizeof(fs::filepool)).data;
	fs->current->length = 0;
	fs->current->previous = null;
	files_enumerator(fs, buffer, buffer);

	// Convert files to assets
	var current = fs->current;
	while (current)
	{
		int length = current->length;
		for (int i = 0; i < length; i++)
		{
			var file = current->files[i];
			//if (ends(file.name, ".meta");
			print("Parsing: %v", current->files[i].path);

		}
		current = current->previous;
	}

	return error::ok;
}


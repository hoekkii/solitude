union rotor3
{
	struct
	{
		float x; // bivector y^z
		float y; // bivector z^x
		float z; // bivector x^y
		float w; // dot product scalar
	};
	struct
	{
		float3 xyz;
		float scalar;
	};
	float4 v;
	float e[4];
};
inline rotor3 angle_plane(float angle, bivector3 p)
{
	float a = angle / 2;
	float s = -sin(a);
	return { s * p.x, s * p.y, s * p.z, cos(a) };
}
inline rotor3 angle_x(float angle) { float a = angle/2; return { -sin(a), 0, 0, cos(a)}; }
inline rotor3 angle_y(float angle) { float a = angle/2; return { 0, -sin(a), 0, cos(a)}; }
inline rotor3 angle_z(float angle) { float a = angle/2; return { 0, 0, -sin(a), cos(a)}; }
inline rotor3 operator * (rotor3 l, rotor3 r)
{
	return {
		l.w * r.x + l.x * r.w + l.y * r.z - l.z * r.y,
		l.w * r.y + l.y * r.w + l.z * r.x - l.x * r.z,
		l.w * r.z + l.z * r.w + l.x * r.y - l.y * r.x,
		l.w * r.w - l.x * r.x - l.y * r.y - l.z * r.z,
	};
}


inline rotor3 inverse(rotor3 r) { return { -r.x, -r.y, -r.z, r.w }; }
inline vector3 rotate(rotor3 r, vector3 v)
{
	 // r*v*r'
	vector3 q {
		 r.w * v.x + r.y * v.z - r.z * v.y,
		 r.w * v.y + r.z * v.x - r.x * v.z,
		 r.w * v.z + r.x * v.y - r.y * v.x,
	};
	float t = - r.x * v.x - r.y * v.y - r.z * v.z;
	return {
		t * -r.x + r.w * q.x - q.y * r.z + q.z * r.y,
		t * -r.y + r.w * q.y - q.z * r.x + q.x * r.z,
		t * -r.z + r.w * q.z - q.x * r.y + q.y * r.x,
	};
}
//inline rotor3 rotate(rotor3 a, rotor3 b) { return a * b * inverse(a); }


inline bivector3 wedge(vector3 p, vector3 q)
{
	return {
		p.y * q.z - p.z * q.y, // y^z
		p.z * q.x - p.x * q.z, // z^x
		p.x * q.y - p.y * q.x, // x^y
	};
}
inline rotor3 geometric(vector3 p, vector3 q)
{
	// TODO: Should we normalize?
	rotor3 result;
	result.xyz = wedge(q, p);
	result.scalar = dot(q, p);
	return result;
}




// TODO: Check if needed to transpose properly
inline float3x3 to_matrix3x3(rotor3 q)
{
	var sqw = q.w*q.w;
	var sqx = q.x*q.x;
	var sqy = q.y*q.y;
	var sqz = q.z*q.z;
	var invs = 1.0f / (sqx + sqy + sqz + sqw);
	var m00 = ( sqx - sqy - sqz + sqw)*invs;
	var m11 = (-sqx + sqy - sqz + sqw)*invs;
	var m22 = (-sqx - sqy + sqz + sqw)*invs;
	
	var tmp1 = q.x*q.y;
	var tmp2 = q.z*q.w;
	var m10 = 2.0f * (tmp1 + tmp2)*invs;
	var m01 = 2.0f * (tmp1 - tmp2)*invs;
	
	tmp1 = q.x*q.z;
	tmp2 = q.y*q.w;
	var m20 = 2.0f * (tmp1 - tmp2)*invs;
	var m02 = 2.0f * (tmp1 + tmp2)*invs;
	tmp1 = q.y*q.z;
	tmp2 = q.x*q.w;
	var m21 = 2.0f * (tmp1 + tmp2)*invs;
	var m12 = 2.0f * (tmp1 - tmp2)*invs;
	
	return {
		m00, m01, m02,
		m10, m11, m12,
		m20, m21, m22,
	};
}
inline float4x4 RTS(rotor3 q, point p)
{
	var sqw = q.w*q.w;
	var sqx = q.x*q.x;
	var sqy = q.y*q.y;
	var sqz = q.z*q.z;
	var invs = 1.0f / (sqx + sqy + sqz + sqw);
	var m00 = ( sqx - sqy - sqz + sqw)*invs;
	var m11 = (-sqx + sqy - sqz + sqw)*invs;
	var m22 = (-sqx - sqy + sqz + sqw)*invs;
	
	var tmp1 = q.x*q.y;
	var tmp2 = q.z*q.w;
	var m10 = 2.0f * (tmp1 + tmp2)*invs;
	var m01 = 2.0f * (tmp1 - tmp2)*invs;
	
	tmp1 = q.x*q.z;
	tmp2 = q.y*q.w;
	var m20 = 2.0f * (tmp1 - tmp2)*invs;
	var m02 = 2.0f * (tmp1 + tmp2)*invs;
	tmp1 = q.y*q.z;
	tmp2 = q.x*q.w;
	var m21 = 2.0f * (tmp1 + tmp2)*invs;
	var m12 = 2.0f * (tmp1 - tmp2)*invs;
	
	return {
		m00, m01, m02, p.x,
		m10, m11, m12, p.y,
		m20, m21, m22, p.z,
		0, 0, 0, 1.0f,
	};
}
inline float4x4 RTS(rotor3 q, point p, vector3 s)
{
	var sqw = q.w*q.w;
	var sqx = q.x*q.x;
	var sqy = q.y*q.y;
	var sqz = q.z*q.z;
	var invs = 1.0f / (sqx + sqy + sqz + sqw);
	var m00 = ( sqx - sqy - sqz + sqw)*invs;
	var m11 = (-sqx + sqy - sqz + sqw)*invs;
	var m22 = (-sqx - sqy + sqz + sqw)*invs;
	
	var tmp1 = q.x*q.y;
	var tmp2 = q.z*q.w;
	var m10 = 2.0f * (tmp1 + tmp2)*invs;
	var m01 = 2.0f * (tmp1 - tmp2)*invs;
	
	tmp1 = q.x*q.z;
	tmp2 = q.y*q.w;
	var m20 = 2.0f * (tmp1 - tmp2)*invs;
	var m02 = 2.0f * (tmp1 + tmp2)*invs;
	tmp1 = q.y*q.z;
	tmp2 = q.x*q.w;
	var m21 = 2.0f * (tmp1 + tmp2)*invs;
	var m12 = 2.0f * (tmp1 - tmp2)*invs;
	
	return {
		m00 * s.x, m01 * s.x, m02 * s.x, p.x,
		m10 * s.y, m11 * s.y, m12 * s.y, p.y,
		m20 * s.z, m21 * s.z, m22 * s.z, p.z,
		0, 0, 0, 1.0f,
	};
}




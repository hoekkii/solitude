
typedef float3 point;
typedef float3 vector3;
typedef float3 bivector3;
struct float4x4fi
{
	float4x4 forward;
	float4x4 inverse;
};







/*
 * 
 * 
struct line3
{
	float3 d; // direction
	float3 m; // moment (bi-vector)
};
union plane3
{
	struct
	{
		float x;
		float y;
		float z;
		float w;
	};
	struct
	{
		float3 n; 
		float d;
	};
	float4 v;
	float e[4];
};
 * 
 * inline Line wedge(float3 p, float3 q)
{
	return {
		{
			q.x - p.x,
			q.y - p.y,
			q.z - p.z,
		},
		{
			p.y * q.z - p.z * q.y,
			p.z * q.x - p.x * q.z,
			p.x * q.y - p.y * q.x,
		},
	};
}
inline Line wedge(Plane f, Plane g)
{
	return {
		{
			f.y * g.z - f.z * g.y,
			f.z * g.x - f.x * g.z,
			f.x * g.y - f.y * g.x,
		},
		{
			g.x * f.w - f.x * g.w,
			g.y * f.w - f.y * g.w,
			g.z * f.w - f.z * g.w,
		},
	};
}
inline Plane wedge(Line l, float3 p)
{
	return {
		l.d.y * p.z - l.d.z * p.y + l.m.x,
		l.d.z * p.x - l.d.x * p.z + l.m.y,
		l.d.x * p.y - l.d.y * p.x + l.m.z,
		-l.m.x * p.x - l.m.y * p.y - l.m.z * p.z,
	};
}
inline Plane wedge(float3 p, Line l) { return wedge(l, p); }
inline float4 wedge(Line l, Plane f)
{
	return {
		l.m.y * f.z - l.m.z * f.y + l.d.x * f.w,
		l.m.z * f.x - l.m.x * f.z + l.d.y * f.w,
		l.m.x * f.y - l.m.y * f.x + l.d.z * f.w,
		-l.m.x * f.x - l.d.y * f.y - l.d.z * f.z,
	};
}
inline float4 wedge(Plane f, Line l) { return wedge(l, f); }
inline float wedge(Line a, Line b) { return -(dot(a.d, b.m) + dot(b.d, a.m)); }
inline float wedge(float3 p, Plane f) { return p.x * f.x + p.y * f.y + p.z * f.z + f.w; }
inline float wedge(Plane f, float3 p) { return -wedge(p, f); }
*/



/**
 * Clifford stuff
 * wedge
 * antiwedge
 *
 *
 */

/*inline rotor3 geometric(vector3 p, vector3 q)
{
	rotor3 result;
	result.xyz = wedge(q, p);
	result.scalar = 1.0f + dot(q, p);
	return result;
}*/




// Linear Algebra
// TODO: cross
// 




/*inline Line wedge(float3 p, float3 q)
{
	return {
		{
			q.x - p.x,
			q.y - p.y,
			q.z - p.z,
		},
		{
			p.y * q.z - p.z * q.y,
			p.z * q.x - p.x * q.z,
			p.x * q.y - p.y * q.x,
		},
	};
}
inline Line wedge(Plane f, Plane g)
{
	return {
		{
			f.y * g.z - f.z * g.y,
			f.z * g.x - f.x * g.z,
			f.x * g.y - f.y * g.x,
		},
		{
			g.x * f.w - f.x * g.w,
			g.y * f.w - f.y * g.w,
			g.z * f.w - f.z * g.w,
		},
	};
}
inline Plane wedge(Line l, float3 p)
{
	return {
		l.d.y * p.z - l.d.z * p.y + l.m.x,
		l.d.z * p.x - l.d.x * p.z + l.m.y,
		l.d.x * p.y - l.d.y * p.x + l.m.z,
		-l.m.x * p.x - l.m.y * p.y - l.m.z * p.z,
	};
}
inline Plane wedge(float3 p, Line l) { return wedge(l, p); }
inline float4 wedge(Line l, Plane f)
{
	return {
		l.m.y * f.z - l.m.z * f.y + l.d.x * f.w,
		l.m.z * f.x - l.m.x * f.z + l.d.y * f.w,
		l.m.x * f.y - l.m.y * f.x + l.d.z * f.w,
		-l.m.x * f.x - l.d.y * f.y - l.d.z * f.z,
	};
}
inline float4 wedge(Plane f, Line l) { return wedge(l, f); }
inline float wedge(Line a, Line b) { return -(dot(a.d, b.m) + dot(b.d, a.m)); }
inline float wedge(float3 p, Plane f) { return p.x * f.x + p.y * f.y + p.z * f.z + f.w; }
inline float wedge(Plane f, float3 p) { return -wedge(p, f); }
*/









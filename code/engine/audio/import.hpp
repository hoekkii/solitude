/**
ALSA is pretty bad, I leave it as this for the time being.
BSD uses OSS, internet says bad, but I beg to differ.
We probably need to support quite a wide range of audio api's.... :'(



*/
struct audio
{
	/**
	 * State of the audio thread.
	 * Negative values indicate an error
	 */
	static constexpr int INIT = 0;
	static constexpr int OPEN = 1;
	static constexpr int KILL = 2;
	static constexpr int DEAD = 3;
	
	struct share
	{
		volatile int state;
		int header;
		
	};
	struct api
	{
		static constexpr int UNKNOWN = 0;
		static constexpr int ALSA = 1;
		static constexpr int OSS = 2;
		static constexpr int PULSE = 3;
		typedef error initproc(struct api*);

		int type;
		void* handle;
		string_slice name;
		void (*dispose)(struct api*);
		uptr shared_size;
		void* shared;
	};
};

/**
 * Windows: WMME;DirectSound;WDM/KS;ASIO;WASAPI
 * Max OS X: Core Audio;JACK
 * Unix: JACK;OSS;ALSA;;ASIHPI
 * Android: OpenSL ES
 * 
 * 
 */

#if SOLITUDE_ALSA
#include "alsa.hpp"
#else
error init_alsa(audio::api*) { return error(0x00000001, "ALSA is marked disabled with the SOLITUDE_ALSA directive."); }
#endif

#if SOLITUDE_OSS
#include "oss.hpp"
#else
error init_oss(audio::api*) { return error(0x00000001, "OSS is marked disabled with the SOLITUDE_OSS directive."); }
#endif

#if SOLITUDE_PULSE
#include "pulse.hpp"
#else
error init_pulse(audio::api*) { return error(0x00000001, "Pulse is marked disabled with the SOLITUDE_PULSE directive."); }
#endif



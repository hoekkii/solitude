struct pulse : public audio::share
{
	static constexpr int HEADER = 0x9015EA0D;
	static constexpr int rate = 44100;
	int pid;
	void* handle;
};

static void dispose_pulse(audio::api* api)
{
	print("Disposing PULSE");
	api->type = audio::api::UNKNOWN;
	var pulse = (struct pulse*)api->shared;
	if (!pulse || pulse->header != pulse::HEADER)
		return;
	
	var now = time::monotonic();
	var timeout = time::s(1);
	if (pulse->state == audio::OPEN)
		pulse->state = audio::KILL;
	
	while (pulse->state == audio::KILL)
	{
		if ((time::monotonic() - now) > timeout)
			break;
	}
	
	api->shared = 0;
	deallocate_raw(pulse, api->shared_size);
	api->shared_size = 0;
}

static int dispose_thread_pulse(struct pulse* pulse, int code, const char* message)
{
	print("dispose_thread_pulse");
	if (pulse->handle)
	{
		dlclose(pulse->handle);
		pulse->handle = null;
	}
	pulse->state = audio::DEAD;
	if (message)
		print(message);
	_exit(code);
	return code;
}

static int init_thread_pulse(struct pulse* pulse)
{
	pulse->state = audio::INIT;
	pulse->header = pulse::HEADER;
	#if SOLITUDE_BSD
	var pid = rfork(rfork_proc | rfork_cfdg);
	#else
	var pid = vfork();
	#endif
	
	if (pid)
	{
		pulse->pid = pid;
		return pid;
	}
	
	// libpulse.so.0 / libpulse.so.2 --  libpulsecommon-10.0.so
	print("thread_pulse");
	var handle = dlopen("libpulse.so.0", RTLD_NOW|RTLD_LOCAL);
	if (!handle) return dispose_thread_pulse(pulse, 1, "Could not open libpulse.so using dlopen.");
	pulse->handle = handle;
	
	#define LDPSFN(name) {\
		reinterpret_cast<void*&>(pulse-> name) = dlsym(handle, #name);\
		if (!pulse-> name) return dispose_thread_pulse(pulse, 2, "Could not find " #name " in libpuse.so." FILE_LINE_FMT);\
	}
	//LDPSFN();
	#undef LDPSFN
	
	print("Loaded Audio API PULSE version %v", 0);
	return 0;
}

static error init_pulse(struct audio::api* api)
{
	api->type = audio::api::PULSE;
	api->name = "PULSE";
	api->dispose = dispose_pulse;
	api->shared = allocate_raw(&(api->shared_size = sizeof(pulse)), memory::SHARED | memory::CLEAR);
	var pulse = (struct pulse*)api->shared;
	init_thread_pulse(pulse);
	
	var now = time::monotonic();
	var timeout = time::s(1);
	while (pulse->state == audio::INIT)
	{
		if ((time::monotonic() - now) > timeout)
			break;
	}
	if (pulse->state == audio::OPEN)
	{
		print("!!!!USING PULSE");
		return error::ok;
	}

	// Cleanup mess when an error has ocurred
	int state = pulse->state;
	api->type = audio::api::UNKNOWN;
	deallocate_raw(api->shared, api->shared_size);
	return error(state, "[AUDIO:PULSE] Failed to initialize: %v", state);
}


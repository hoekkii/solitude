struct alsa : public audio::share
{
	static constexpr int HEADER = 0xA15AA15A;
	static constexpr int rate = 44100;
	int pid;
	void* handle;
	void* pcm;
	void* hwparams;

	// PCM procedures
	int  (*snd_pcm_open)(void**, const char*, flags32, int);
	int  (*snd_pcm_close)(void*);
	int  (*snd_pcm_hw_params)(void*, void*);
	int  (*snd_pcm_hw_params_any)(void*, void*);
	int  (*snd_pcm_hw_params_malloc)(void**);
	void (*snd_pcm_hw_params_free)(void*);
	int  (*snd_pcm_hw_params_set_rate_resample)(void*, void*, uint);
	int  (*snd_pcm_hw_params_set_access)(void*, void*, flags32);
	int  (*snd_pcm_hw_params_set_format)(void*, void*, flags32);
	int  (*snd_pcm_hw_params_set_channels)(void*, void*, uint);
	int  (*snd_pcm_hw_params_set_rate_near)(void*, void*, uint*, int*);

	// Other procedures
	int  (*snd_card_next)(int*);
	int  (*snd_card_get_name)(int, char**);
	int  (*snd_device_name_hint)(int, const char*, char***);
	char* (*snd_device_name_get_hint)(const char*, const char*);
	const char* (*snd_asoundlib_version)(void);
};


static void dispose_alsa(audio::api* api)
{
	print("Disposing ALSA");
	api->type = audio::api::UNKNOWN;
	var alsa = (struct alsa*)api->shared;
	if (!alsa || alsa->header != alsa::HEADER)
		return;

	var now = time::monotonic();
	var timeout = time::s(1);
	if (alsa->state == audio::OPEN)
		alsa->state = audio::KILL;

	while (alsa->state == audio::KILL)
	{
		if ((time::monotonic() - now) > timeout)
			break;
	}
	
	api->shared = 0;
	deallocate_raw(alsa, api->shared_size);
	api->shared_size = 0;
}

static int dispose_thread_alsa(struct alsa* alsa, int code, const char* message)
{
	print("dispose_thread_alsa");
	
	if (alsa->pcm)
	{
		alsa->snd_pcm_close(alsa->pcm);
		alsa->pcm = null;
	}
	
	if (alsa->handle)
	{
		dlclose(alsa->handle);
		alsa->handle = null;
	}

	alsa->state = audio::DEAD;
	if (message)
		print(message);
	_exit(code);
	return code;
}

static int init_thread_alsa(struct alsa* alsa)
{
	alsa->state = audio::INIT;
	alsa->header = alsa::HEADER;
	#if SOLITUDE_BSD
	var pid = rfork(rfork_proc | rfork_cfdg);
	#else
	var pid = vfork();
	#endif
	
	if (pid)
	{
		alsa->pid = pid;
		return pid;
	}
	
	print("thread_alsa");
	var handle = dlopen("libasound.so", RTLD_NOW|RTLD_LOCAL);
	if (!handle) return dispose_thread_alsa(alsa, 1, "Could not open libalsa.so using dlopen.");
	alsa->handle = handle;

	#define LDASFN(name) {\
		reinterpret_cast<void*&>(alsa-> name) = dlsym(handle, #name);\
		if (!alsa-> name) return dispose_thread_alsa(alsa, 2, "Could not find " #name " in libasouns.so." FILE_LINE_FMT);\
	}
	LDASFN(snd_pcm_open);
	LDASFN(snd_pcm_close);
	LDASFN(snd_pcm_hw_params);
	LDASFN(snd_pcm_hw_params_malloc);
	LDASFN(snd_pcm_hw_params_free);
	LDASFN(snd_pcm_hw_params_set_rate_resample);
	LDASFN(snd_pcm_hw_params_set_access);
	LDASFN(snd_pcm_hw_params_set_format);
	LDASFN(snd_pcm_hw_params_set_channels);
	LDASFN(snd_pcm_hw_params_set_rate_near);
	LDASFN(snd_card_next);
	LDASFN(snd_card_get_name);
	LDASFN(snd_device_name_hint);
	LDASFN(snd_device_name_get_hint);
	LDASFN(snd_asoundlib_version);
	#undef LDASFN
	
	int r;
	print("Loaded Audio API ALSA version %v", alsa->snd_asoundlib_version());

	if (r = alsa->snd_pcm_open(&alsa->pcm, "default", 0, 0))
		return dispose_thread_alsa(alsa, 3, "ALSA Could not open audio device (%v)." FILE_LINE_FMT);
	if (r = alsa->snd_pcm_hw_params_malloc(&alsa->hwparams))
		return dispose_thread_alsa(alsa, 4, "ALSA Could not allocate hardware parameters (%v)." FILE_LINE_FMT);
	if (r = alsa->snd_pcm_hw_params_any(alsa->pcm, alsa->hwparams))
		return dispose_thread_alsa(alsa, 5, "ALSA error (%v)." FILE_LINE_FMT);
	
	uint resample = 1;
	if (r = alsa->snd_pcm_hw_params_set_rate_resample(alsa->pcm, alsa->hwparams, resample))
		return dispose_thread_alsa(alsa, 6, "ALSA error (%v)." FILE_LINE_FMT);

	if (r = alsa->snd_pcm_hw_params_set_access(alsa->pcm, alsa->hwparams, 3)) // SND_PCM_ACCESS_RW_INTERLEAVED
		return dispose_thread_alsa(alsa, 7, "ALSA error (%v)." FILE_LINE_FMT);

	// Check if system is little endian or big endian
	if (r = alsa->snd_pcm_hw_params_set_format(alsa->pcm, alsa->hwparams, 2)) //SND_PCM_FORMAT_S16_LE
		return dispose_thread_alsa(alsa, 8, "ALSA error (%v)." FILE_LINE_FMT);

	if (r = alsa->snd_pcm_hw_params_set_channels(alsa->pcm, alsa->hwparams, 2))
		return dispose_thread_alsa(alsa, 9, "ALSA error (%v)." FILE_LINE_FMT);

	if (r = alsa->snd_pcm_hw_params(alsa->pcm, alsa->hwparams))
		return dispose_thread_alsa(alsa, 10, "ALSA error (%v)." FILE_LINE_FMT);
	
	/**
	 * All initialization has been done
	 */
	alsa->state = audio::OPEN;
	
	// TEST!!!!!
	if (alsa->state == audio::OPEN)
	{
		constexpr int bufs = 256;
		constexpr int segs = 128;
		constexpr int size = bufs * segs;
		short buf[size] {};
		short bufo[2 * bufs] {};
		union wav_header
		{
			struct
			{
				char riff[4];
				int size;
				char type[4];
				char fmt_[4];
				int fmt_len;
				short pcm;
				short channels;
				int sample_rate;
				int total_bit_rate;
				short ____;
				short sample_bits;
				char data[4];
				int length;
			};
			byte header[44];
		} wh;
		constexpr int riff_end = 0x52494646;
		var f = fopen("../assets/audio/24-form.wav", "rb");
		fread(wh.header, 1, sizeof(wav_header), f);
		print ("A %v; %v; %v; %v; %v; %v; %v; %v", wh.size, wh.fmt_len, wh.pcm, wh.channels, wh.sample_rate, wh.total_bit_rate, wh.sample_bits, wh.length);
		int l = 0;
		while (alsa->state == audio::OPEN)
		{
			int r = fread(buf, bufs, 2, f);
			l += r;
			for (var i = 0; i < bufs; i++)
				bufo[i * 1 + 0] = bufo[i * 1 + bufs] = buf[i];
			//write(fd, bufo, bufs * 2);
			if (l >= wh.length || !r)
				break;
		}
			
		fclose(f);
	}
	
	return dispose_thread_alsa(alsa, 0, 0);
}


error init_alsa(audio::api* api)
{
	api->type = audio::api::ALSA;
	api->name = "ALSA";
	api->dispose = dispose_alsa;
	api->shared = allocate_raw(&(api->shared_size = sizeof(alsa)), memory::SHARED | memory::CLEAR);
	var alsa = (struct alsa*)api->shared;
	init_thread_alsa(alsa);
	
	var now = time::monotonic();
	var timeout = time::s(1);
	while (alsa->state == audio::INIT)
	{
		if ((time::monotonic() - now) > timeout)
			break;
	}

	if (alsa->state == audio::OPEN)
	{
		print("!!!!!USING ALSA!!!");
		return error::ok;
	}

	// Cleanup mess when an error has ocurred
	int state = alsa->state;
	api->type = audio::api::UNKNOWN;
	deallocate_raw(api->shared, api->shared_size);
	return error(state, "[AUDIO:ALSA] Failed to initialize: %v", state);


	/*var alsa = &api->alsa;
	#define return_error(args...) {\
		dispose_alsa(api);\
		return error(args);\
	}

	/ *{
		int rcard = -1;
		for (int rcard = -1;;)
		{
			if (r = alsa->snd_card_next(&rcard))
				return return_error(0x00000001, "ALSA error (%v)." FILE_LINE_FMT, r));
			if (rcard == -1) break;
			print(rcard);
			char* name;
			if (r = alsa->snd_card_get_name(rcard, &name))
				return return_error(0x00000001, "ALSA error (%v)." FILE_LINE_FMT, r));
			
			print(name);

		}
	}* /
print("~~~~~~~");
	{
		char** hints;
		if (r = alsa->snd_device_name_hint(-1, "pcm", &hints))
			return_error(0x00000001, "ALSA error (%v)." FILE_LINE_FMT, r);

		char** hint = hints;
		while (*hint)
		{
			char* name = alsa->snd_device_name_get_hint(*hint, "NAME");
			print(name);
			hint++;
		}
	}


	// TODO: plughw:0,0 is the default device, make it possible to use usersettings
	api->type = audio::api::ALSA;
	api->name = "ALSA";
	api->dispose = dispose_alsa;
	
	return error::ok;
	#undef return_error
	*/
}





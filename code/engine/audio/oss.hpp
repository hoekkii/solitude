struct oss : public audio::share
{
	static constexpr int HEADER = 0x05513055;
	static constexpr int rate = 44100;
	//static constexpr int rate = 48000;
	
	int pid;
};

static void dispose_oss(audio::api* api)
{
	print("Disposing OSS");
	api->type = audio::api::UNKNOWN;
	var oss = (struct oss*)api->shared;
	if (!oss || oss->header != oss::HEADER)
		return;
	
	var now = time::monotonic();
	var timeout = time::s(1);
	if (oss->state == audio::OPEN)
		oss->state = audio::KILL;
		
	while (oss->state == audio::KILL)
	{
		if ((time::monotonic() - now) > timeout)
			break;
	}
	
	api->shared = 0;
	deallocate_raw(oss, api->shared_size);
	api->shared_size = 0;
}

// runs on seperate thread
static int dispose_thread_oss(struct oss* oss, int fd, int code, const char* message)
{
	print("dispose_thread_oss");
	if (fd) close(fd);// is not necessary
	oss->state = audio::DEAD;
	if (message)
		print(message);
	_exit(code);
	return code;
	
}
static int thread_oss(void* arg)
{
	print ("thread_oss");
	struct oss* oss = (struct oss*)arg;
	oss->state = audio::OPEN;
	print("BbbbbBBBbbbbbBBB");

	oss->state = audio::INIT;
	oss->header = oss::HEADER;
	int iores = 0;
	int fd = open("/dev/dsp", open_write, 0);
	if (fd == -1) return dispose_thread_oss(oss, 0, -1, "Failed to open device.");
	
	// Sample format
	constexpr int AFMT_S16_LE = 0x00000010;
	constexpr int AFMT_S16_BE = 0x00000020;
	int target_format = bigendian() ? AFMT_S16_BE : AFMT_S16_LE;
	int sample_format = target_format;
	if (iores = ioctl_io(fd, 'P', 5, &sample_format) || sample_format != target_format)
		return dispose_thread_oss(oss, fd, -2, "ioctl failed to apply sample format (0x%H) with code %v and parameter 0x%H" FILE_LINE_FMT); //target_format, iores, sample_format
	
	// Sample rate
	int sample_rate = oss::rate;
	if (iores = ioctl_io(fd, 'P', 2, &sample_rate) || sample_rate != oss::rate)
		return dispose_thread_oss(oss, fd, -3, "ioctl failed to apply sample rate (%v) with code %v and parameter %v" FILE_LINE_FMT); //oss::rate, iores, sample_rate
	
	constexpr int bufs = 256;
	constexpr int segs = 128;
	constexpr int size = bufs * segs;
	short buf[size] {};
	short bufo[2 * bufs] {};
	

	
	print("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
	if (oss->state == audio::OPEN)
	{
		union wav_header
		{
			struct
			{
				char riff[4];
				int size;
				char type[4];
				char fmt_[4];
				int fmt_len;
				short pcm;
				short channels;
				int sample_rate;
				int total_bit_rate;
				short ____;
				short sample_bits;
				char data[4];
				int length;
			};
			byte header[44];
		} wh;
		constexpr int riff_end = 0x52494646;
		var f = fopen("../assets/audio/24-form.wav", "rb");
		print("DDDD");
		fread(wh.header, 1, sizeof(wav_header), f);
		print("EEEE");
		if (riff_end == *(int*)wh.riff)
		{
			print ("B");
		}
		else
		{
			assert(endian_swap(riff_end) == *(int*)wh.riff);
			/*wh.size = endian_swap(wh.size);
			wh.fmt_len = endian_swap(wh.fmt_len);
			wh.pcm = endian_swap(wh.pcm);
			wh.channels = endian_swap(wh.channels);
			wh.sample_rate = endian_swap(wh.sample_rate);
			wh.total_bit_rate = endian_swap(wh.total_bit_rate);
			wh.sample_bits = endian_swap(wh.sample_bits);
			wh.length = endian_swap(wh.length);*/
			print ("A %v; %v; %v; %v; %v; %v; %v; %v", wh.size, wh.fmt_len, wh.pcm, wh.channels, wh.sample_rate, wh.total_bit_rate, wh.sample_bits, wh.length);
			int l = 0;
			while (oss->state == audio::OPEN)
			{
				int r = fread(buf, bufs, 2, f);
				l += r;
				for (var i = 0; i < bufs; i++)
					bufo[i * 1 + 0] = bufo[i * 1 + bufs] = buf[i];
				write(fd, bufo, bufs * 2);
			//	for (var i = 0; i < r; i++)
			//		buf[i] = (buf[i]);
				//for (var i = r; i < bufs; i++)
				//	buf[i] = 0;
				//write(fd, buf, bufs);
				if (l >= wh.length || !r)
					break;
			}
		}
		fclose(f);
	}
	return dispose_thread_oss(oss, fd, 0, 0);
}

static int thread_init_oss(struct oss* oss)
{
	oss->state = audio::INIT;
	oss->header = oss::HEADER;
	#if SOLITUDE_BSD
	var pid = rfork(rfork_proc | rfork_cfdg);
	#else
	var pid = vfork();
	#endif
	if (pid)
	{
		oss->pid = pid;
		return pid;
	}
	
	
	int iores = 0;
	int fd = open("/dev/dsp", open_write, 0);
	if (fd == -1) return dispose_thread_oss(oss, 0, 1, "Failed to open device.");
	
	// Sample format
	constexpr int AFMT_S16_LE = 0x00000010;
	constexpr int AFMT_S16_BE = 0x00000020;
	int target_format = bigendian() ? AFMT_S16_BE : AFMT_S16_LE;
	int sample_format = target_format;
	if (iores = ioctl_io(fd, 'P', 5, &sample_format) || sample_format != target_format)
		return dispose_thread_oss(oss, fd, 2, "ioctl failed to apply sample format (0x%H) with code %v and parameter 0x%H" FILE_LINE_FMT); //target_format, iores, sample_format
	
	// Sample rate
	int sample_rate = oss::rate;
	if (iores = ioctl_io(fd, 'P', 2, &sample_rate) || sample_rate != oss::rate)
		return dispose_thread_oss(oss, fd, 3, "ioctl failed to apply sample rate (%v) with code %v and parameter %v" FILE_LINE_FMT); //oss::rate, iores, sample_rate
	
	print ("thread_oss");
	oss->state = audio::OPEN;
	
	constexpr int bufs = 256;
	constexpr int segs = 128;
	constexpr int size = bufs * segs;
	short buf[size] {};
	short bufo[2 * bufs] {};
	
	if (oss->state == audio::OPEN)
	{
		double testvals[] {
			160.0,
			200.0,
			250.0,
			315.0,
			400.0,
			500.0,
			630.0,
			800.0,
			1000.0,
		};
		for (var tindex = 0; oss->state == audio::OPEN && tindex < 6; tindex++)
		{
			double hz = testvals[tindex];
			for (var i = 0; i < size; i++)
			{
				double x = saturate(sin((double)i / size  * PI) * 4.0f);
				double y = saturate(sin(((double)i * hz * PI / sample_rate)));
				short z = (short)(SHORTMAX * x * y);
				buf[i] = z;
			}
			for (var i = 0; i < 1; i++)
			for (var j = 0; j < segs; j++)
			{
				var o = bufs * j;
				for (var k = 0; k < bufs; k++)
					bufo[k] = bufo[k + bufs] = buf[k + o];
				write(fd, bufo, bufs * 2);
			}
		}
	}
	
	if (oss->state == audio::OPEN)
	{
		union wav_header
		{
			struct
			{
				char riff[4];
				int size;
				char type[4];
				char fmt_[4];
				int fmt_len;
				short pcm;
				short channels;
				int sample_rate;
				int total_bit_rate;
				short ____;
				short sample_bits;
				char data[4];
				int length;
			};
			byte header[44];
		} wh;
		constexpr int riff_end = 0x52494646;
		var f = fopen("../assets/audio/24-form.wav", "rb");
		fread(wh.header, 1, sizeof(wav_header), f);
		print ("A %v; %v; %v; %v; %v; %v; %v; %v", wh.size, wh.fmt_len, wh.pcm, wh.channels, wh.sample_rate, wh.total_bit_rate, wh.sample_bits, wh.length);
		int l = 0;
		while (oss->state == audio::OPEN)
		{
			int r = fread(buf, bufs, 2, f);
			l += r;
			for (var i = 0; i < bufs; i++)
				bufo[i * 1 + 0] = bufo[i * 1 + bufs] = buf[i];
			write(fd, bufo, bufs * 2);
			if (l >= wh.length || !r)
				break;
		}
			
		fclose(f);
	}
	
	return dispose_thread_oss(oss, fd, 0, 0);
}

static error init_oss(audio::api* api)
{
	api->type    = audio::api::OSS;
	api->name    = "OSS";
	api->dispose = dispose_oss;
	api->shared  = allocate_raw(&(api->shared_size = sizeof(oss)), memory::SHARED | memory::CLEAR);
	var oss = (struct oss*)api->shared;
	thread_init_oss(oss);
	
	var now = time::monotonic();
	var timeout = time::s(1);
	while (oss->state == audio::INIT)
	{
		if ((time::monotonic() - now) > timeout)
			break;
	}
	
	if (oss->state == audio::OPEN)
	{
		print("!!!!!!Using this");
		return error::ok;
	}
	
	// Cleanup on error
	int state = oss->state;
	api->type = audio::api::UNKNOWN;
	deallocate_raw(api->shared, api->shared_size);
	return error(state, "[AUDIO:OSS] Failed to initialize: %v", state);
}

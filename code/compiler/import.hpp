#if !COMPILER_MSVC && !COMPILER_LLVM
	#if _MSC_VER
	#undef COMPILER_MSVC
	#define COMPILER_MSVC 1
	#else
	#undef COMPILER_LLVM
	#define COMPILER_LLVM 1
	#endif
#endif

#define ARRAY_LEN(a) (sizeof(a)/sizeof(a[0]))

#define CRASH *(volatile int*)0=0
#if SOLITUDE_DEBUG
#define assert(expr) if (!(expr)) { *(volatile int*)0=0; }
#define s_assert(expr) static_assert(expr, "FAILED assertion: " #expr)
#define NotImplemented assert(!"NotImplemented")
#else
#define assert(expr)
#define s_assert(expr)
#define NotImplemented NotImplemented!
#endif

#define FILE_LINE __FILE__ "::" MACRO_AS_STRING(__LINE__)
#define FILE_LINE_FMT " (" __FILE__ "::" MACRO_AS_STRING(__LINE__) ")"
#define CONCAT_(a, b) a ## b
#define CONCAT(a, b) CONCAT_(a, b) 
#define MACRO_AS_STRING(x) MACRO_AS_STRING_(x)
#define MACRO_AS_STRING_(x) #x
#define VAR(name) CONCAT(name ## __SOLITUDE_MACRO_VAR_, __LINE__)
#define COMMA ,

#define ARGUMENT_AT_0(arg0, ...) arg0
#define ARGUMENT_AT_1(arg0, arg1, ...) arg1
#define ARGUMENT_AT_2(arg0, arg1, arg2, ...) arg2
#define ARGUMENT_AT_3(arg0, arg1, arg2, arg3, ...) arg3
#define ARGUMENT_AT_4(arg0, arg1, arg2, arg3, arg4, ...) arg4
#define ARGUMENT_AT_0_(tuple) ARGUMENT_AT_0 tuple
#define ARGUMENT_AT_1_(tuple) ARGUMENT_AT_1 tuple
#define ARGUMENT_AT_2_(tuple) ARGUMENT_AT_2 tuple
#define ARGUMENT_AT_3_(tuple) ARGUMENT_AT_3 tuple
#define ARGUMENT_AT_4_(tuple) ARGUMENT_AT_4 tuple

#define iterate(i, arr)   for (int i = 0, VAR(length) = len(arr); i < VAR(length); ++i)
#define iterate_r(i, arr) for (int i = len(arr) - 1; i >= 0; --i)
#define foreach(v, arr)   \
	for (int VAR(keep) = 1, VAR(i) = 0, VAR(length) = len(arr);\
         VAR(keep) && VAR(i) < VAR(length); VAR(keep) ^= 1, VAR(i)++)\
    		for (v = arr[VAR(i)]; VAR(keep); VAR(keep) ^= 1)

#define var auto
#define let auto
#define nil nullptr
#define null nullptr

#define ALIGN_POW2(v, a) (((v)+((a)-1)) & ~(((v)-(v))+(a)-1))
#define ALIGN_8(v) (((v)+7)&~7)
#define ALIGN_16(v) (((v)+15)&~15)
#define IS_POW2(v) (((v) & ~((v) - 1)) == (v))
#define KILOBYTE (1<<10)
#define MEGABYTE (1<<20)
#define GIGABYTE (1<<30)

// When cstddef is not included, create our own offsetof
#ifndef offsetof
#define offsetof(t, p) ((uptr)&(((t*)0)->p))
#endif




# Docs


## Vision
The engine has been designed in a couple layers: Platform-, Engine- and the Game layer.
Each of these layers have some components.

### Platform layer
A basic layer to be able to the minimal required stuff on all supported platforms:
 - IO: reading and writing to files and console
 - Error handling

### Engine layer
A component organized structed dependent on the platform layer that has platform dependent solutions.
 - Rendering: Vulkan

### Game layer

## Structure
There are three types of include files: import.hpp, types.hpp, impl.hpp.
Import handles everything: types and procedures.
Types handles structures and primitive procedures.
Iml handles complex procedures relying on complex types.
These three files are the only ones that are allowed to use the #include directive.


# Issues
- Ubuntu input causes a segmentation fault at random times: `xcb_io.c:269: poll_for_event: Assertion !xcb_xlib_threads_sequence_lost failed`.

# Changelog
## 0.0.1 alpha
- 2022-04-22 Rendering more complex models with perspective projection matrix and changed the architecture design.
- 2022-03-31 Compilable build for linux, freebsd and windows.




# TODO
- Figure out a way to support multiple graphic-APIs
- Repair Vulkan API









#if SOLITUDE_X64
typedef long unsigned int uptr;
typedef long   signed int sptr;
typedef long unsigned int flags64;
typedef long unsigned int u64;
typedef long   signed int s64;
#else
typedef           unsigned int uptr;
typedef             signed int sptr;
typedef long long unsigned int flags64;
typedef long long unsigned int u64;
typedef long long   signed int s64;
#endif

typedef         signed char sbyte;
typedef       unsigned char byte;
typedef       unsigned char uchar;
typedef short unsigned int  ushort;
typedef       unsigned int  uint;
typedef       unsigned int  flags32;
typedef       unsigned char u8;
typedef short unsigned int  u16;
typedef       unsigned int  u32;
typedef         signed char s8;
typedef short   signed int  s16;
typedef         signed int  s32;




// Special datatypes
typedef void* libhandle;
typedef long platform_long; 
typedef unsigned long platform_ulong;
//#define long error: specify size or explicitly use platform_long instead!







// EXTERNAL foreignS
// https://man7.org/linux/man-pages
// https://linux.die.net/man
#define foreign extern "C"

/** usr/include/sys/mman.h
 * - Allocate and free large blocks of memory
 */
#define PROT_NONE  0x00
#define PROT_READ  0x01
#define PROT_WRITE 0x02
#define PROT_EXEC  0x04

#if SOLITUDE_BSD
	#define MAP_SHARED    0x0001
	#define MAP_PRIVATE   0x0002
	#define MAP_ANONYMOUS 0x1000
	#define MAP_STACK     0x0400
#else
	#define MAP_SHARED    0x0001
	#define MAP_PRIVATE   0x0002
	#define MAP_ANONYMOUS 0x0020
	#define MAP_STACK     0x20000
#endif
foreign void* mmap(void*, uptr, int, int, int, sptr);
foreign int munmap(void*, uptr);
foreign int mprotect(void*, uptr, int);





/** usr/include/stdlib.h
 * @Obsolete malloc use mmap instead
 * - Allocating memory on the heap
 * - Exit the program from anywhere
 */
foreign void* malloc(uptr);
foreign void exit(int);



/** usr/include/sys/sysctl.h
 *
 */
foreign int sysctl(const int*, uint, void*, uptr*, const void*, u64);





/** usr/include/stdio.h
 * TODO: Try removing some included procedures
 * @Obsolete SEEK_SET convert to a constexpr inside sys struct
 * @Obsolete SEEK_CUR convert to a constexpr inside sys struct
 * @Obsolete SEEK_END convert to a constexpr inside sys struct
 * - Printing to console
 * - Reading and writing of files
 */
#define SEEK_SET 0
#define SEEK_CUR 1
#define SEEK_END 2
#if SOLITUDE_BSD
foreign void* __stdoutp;
#define stdout __stdoutp
#else
foreign void* stdout;
#endif
foreign void* fopen(const char*, const char*);
foreign int fclose(void*);
foreign int fflush(void*);
foreign uptr fread(void*, uptr, uptr, void*);
foreign uptr fwrite(const void*, uptr, uptr, void*);
foreign int fseek(void*, long, int);
foreign platform_long ftell(void*);


/** usr/include/time.h
 * - The time since the start of the program
 */
struct unix_timespec {
	#if SOLITUDE_BSD
	static constexpr int realtime = 0;
	static constexpr int monotonic = 4;
	static constexpr int precise = 11;
	static constexpr int fast = 12;
	#else
	static constexpr int realtime = 0;
	static constexpr int monotonic = 4;
	static constexpr int precise = 4;
	static constexpr int fast = 4;
	#endif

	platform_long seconds;
	platform_long nanoseconds;
};
foreign int clock_getres(int, unix_timespec*);
foreign int clock_gettime(int, unix_timespec*);


/** usr/include/fcntl.h
 * - Opening devices such as audio devices and joysticks
 */
static constexpr int open_read  = 0x00; // Open with read only flag
static constexpr int open_write = 0x01; // Open with write only flag
static constexpr int open_rw    = 0x02; // Open with read and write flag
foreign int open(const char*, int, ...);


/** usr/include/unistd.h
 * - Reading and writing of devices such as joysticks
 * - Uses the handle retrieved in the procedure of fcntl.h::open
 */
foreign sptr read(int, void*, uptr);
foreign sptr write(int, const void*, uptr);
foreign int  close(int);
foreign void _exit(int);
#if SOLITUDE_BSD
foreign platform_long sysconf(int);
inline int getpagesize() { return sysconf(47); } // _SC_PAGESIZE
#else
foreign int getpagesize(void);
#endif
foreign s64 readlink(const char*, void*, u64);


/** usr/include/sys/ioccom.h
 * - Reading and writing of devices such as joysticks
 * - fcntl.h open
 */
foreign int ioctl(int, unsigned long, ...);
template<typename T>
inline int ioctl_none(int fd, u64 group, u64 n, T* t)
{
	constexpr u64 none = 0x20000000U;
	constexpr u64 mask = (1 << 13) - 1;
	return ioctl(fd, none | (mask & sizeof(T)) << 16 | group << 8 | n, t);
}
template<typename T>
inline int ioctl_out(int fd, u64 group, u64 n, T* t)
{
	constexpr u64 out = 0x40000000U;
	constexpr u64 mask = (1 << 13) - 1;
	return ioctl(fd, out | (mask & sizeof(T)) << 16 | group << 8 | n, t);
}
inline int ioctl_out(int fd, u64 group, u64 n, u64 l, void* t) 
{
	constexpr u64 out = 0x40000000U;
	constexpr u64 mask = (1 << 13) - 1;
	return ioctl(fd, out | (mask & l) << 16 | group << 8 | n, t);
}
template<typename T>
inline int ioctl_in(int fd, u64 group, u64 n, T* t)
{
	constexpr u64 in = 0x80000000U;
	constexpr u64 mask = (1 << 13) - 1;
	return ioctl(fd, in | (mask & sizeof(T)) << 16 | group << 8 | n, t);
}
template<typename T>
inline int ioctl_io(int fd, u64 group, u64 n, T* t)
{
	constexpr u64 out = 0x40000000U;
	constexpr u64 in = 0x80000000U;
	constexpr u64 mask = (1 << 13) - 1;
	return ioctl(fd, (out | in) | (mask & sizeof(T)) << 16 | group << 8 | n, t);
}


/** usr/include/dlfcn.h
 * - Loading libraries with .so extension
 * - Finding procedures inside libraries
 */
#define RTLD_LAZY   0x00001
#define RTLD_NOW    0x00002
#define RTLD_GLOBAL 0x00100
#define RTLD_LOCAL  0x00000
foreign void* dlopen(const char*, int);
foreign void* dlsym(void*, const char*);
foreign int dlclose(void*);

/** usr/include/sys/unistd.h
 *
 */
#if SOLITUDE_BSD
constexpr int rfork_proc = 1 << 4;
constexpr int rfork_cfdg = 1 << 12;
#else
#endif

/** usr/include/unistd.h
 *
 */
foreign int access(const char*, int);
#if SOLITUDE_BSD
foreign int rfork(int) __attribute__((__returns_twice__));
#else
foreign int vfork(void);
#endif


/** usr/include/dirent.h
 */
struct dirent {
	static constexpr byte DIRECTORY = 4;
	
	#if SOLITUDE_BSD
	uptr inode;
	sptr position;
	ushort size;
	byte type;
	byte __pad0;
	ushort name_length;
	ushort __pad1;
	#else
	uptr inode;
	ushort name_length;
	byte ooo[6];
	byte something;
	byte __pad1;
	byte type;
	#endif
	char name[256];
};
foreign void* opendir(const void*);
foreign dirent* readdir(void*);
foreign int closedir(void*);



/** math.h
 * 
 */
#define __pure2 __attribute__((__const__))
foreign int	isinf(double);
foreign int	isnan(double);
foreign float	acosf(float);
foreign float	asinf(float);
foreign float	atanf(float);
foreign float	atan2f(float, float);
foreign float	cosf(float);
foreign float	sinf(float);
foreign float	tanf(float);
foreign float	coshf(float);
foreign float	sinhf(float);
foreign float	tanhf(float);
foreign float	exp2f(float);
foreign float	expf(float);
foreign float	expm1f(float);
foreign float	frexpf(float, int *);	/* fundamentally !__pure2 */
foreign int	ilogbf(float) __pure2;
foreign float	ldexpf(float, int);
foreign float	log10f(float);
foreign float	log1pf(float);
foreign float	log2f(float);
foreign float	logf(float);
foreign float	modff(float, float *);	/* fundamentally !__pure2 */
foreign float	powf(float, float);
foreign float	sqrtf(float);
foreign float	ceilf(float);
foreign float	fabsf(float) __pure2;
foreign float	floorf(float);
foreign float	fmodf(float, float);
foreign float	roundf(float);
foreign float	erff(float);
foreign float	erfcf(float);
foreign float	hypotf(float, float);
foreign float	lgammaf(float);
foreign float	tgammaf(float);
foreign float	acoshf(float);
foreign float	asinhf(float);
foreign float	atanhf(float);
foreign float	cbrtf(float);
foreign float	logbf(float);
foreign float	copysignf(float, float) __pure2;
foreign long long llrintf(float);
foreign long long llroundf(float);
foreign long	lrintf(float);
foreign long	lroundf(float);
foreign float	nanf(const char *) __pure2;
foreign float	nearbyintf(float);
foreign float	nextafterf(float, float);
foreign float	remainderf(float, float);
foreign float	remquof(float, float, int *);
foreign float	rintf(float);
foreign float	scalblnf(float, long);
foreign float	scalbnf(float, int);
foreign float	truncf(float);
foreign float	fdimf(float, float);
foreign float	fmaf(float, float, float);
foreign float	fmaxf(float, float) __pure2;
foreign float	fminf(float, float) __pure2;

#if SOLITUDE_BSD
foreign double	drem(double, double);
foreign int	finite(double) __pure2;
foreign int	isnanf(float) __pure2;
foreign double	gamma_r(double, int *);
foreign double	lgamma_r(double, int *);
foreign double	significand(double);
#else
inline int isnanf(float x) { return x != x; }
#endif

// TODO: Look if these can be replaced by intrinsics
inline float frac(float x)     { return x - floorf(x); }
inline float rsqrt(float x)    { return 1.0f / sqrtf(x); }
inline float saturate(float x) { return x < 0 ? 0 : x > 1 ? 1 : x; }
inline float radians(float x)  { return x * 57.29577951308232f; }
inline float degrees(float x)  { return x * 0.017453292519943295f; }



/**
 * HACK: Naming is unfortunate
 */
#define abs   fabsf
#define acos  acosf
#define asin  asinf
#define atan  atanf
#define atan2 atan2f
#define ceil  ceilf
#define cbrt  cbrtf
#define cos   cosf
#define cosh  coshf
#define exp   expf
#define exp2  exp2f
#define floor floorf
#define fmod  fmodf
#define log   logf
#define log10 log10f
#define log2  log2f
#define modf  modff
#define pow   powf
#define round roundf
#define sin   sinf
#define sinh  sinhf
#define sqrt  sqrtf
#define tan   tanf
#define tanh  tanhf
#define trunc truncf



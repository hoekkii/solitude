// TODO: Reevaluate the macros, try to use load_proc instead.
#define FNPTR(x) reinterpret_cast<void*&>(x)
#define LOAD_FNPTRP(val, name) \
	reinterpret_cast<void*&>(val -> name) = dlsym(handle, #name); \
	if (!val) { \
		dispose_lib(handle); \
		return error(0x00000001, "Could not find " #name " using dlsym." FILE_LINE_FMT); \
	}
#define LOAD_FNPTRN(val, name) \
	reinterpret_cast<void*&>(val) = dlsym(handle, #name); \
	if (!val) { \
		dispose_lib(handle); \
		return error(0x00000001, "Could not find " #name " using dlsym." FILE_LINE_FMT); \
	}
#define LOAD_FNPTR(name) \
	reinterpret_cast<void*&>(name) = dlsym(handle, #name); \
	if (!name) { \
		dispose_lib(handle); \
		return error(0x00000001, "Could not find " #name " using dlsym." FILE_LINE_FMT); \
	}

typedef s64 timestamp;
namespace time
{
	static constexpr timestamp resolution = 1e9;
	static constexpr double to(timestamp t, double s)   { return t * (s / resolution); }
	static constexpr timestamp from(double t, double s) { return (timestamp)(t * (resolution / s)); }

	constexpr double to_ns(timestamp t) { return to(t, 1e9); }
	constexpr double to_us(timestamp t) { return to(t, 1e6); }
	constexpr double to_ms(timestamp t) { return to(t, 1e3); }
	constexpr double to_s(timestamp t)  { return to(t, 1e0); }
	constexpr timestamp ns(double t)  { return from(t, 1e9); }
	constexpr timestamp us(double t)  { return from(t, 1e6); }
	constexpr timestamp ms(double t)  { return from(t, 1e3); }
	constexpr timestamp s(double t)   { return from(t, 1e0); }
	constexpr timestamp m(double t)   { return from(t, 1.0 / 60); }
	constexpr timestamp h(double t)   { return from(t, 1.0 / 3600); }
	
	static inline timestamp monotonic()
	{
		struct unix_timespec t;
		clock_gettime(unix_timespec::monotonic, &t);
		return t.seconds * resolution + t.nanoseconds;
	}
	static inline timestamp fast()
	{
		struct unix_timespec t;
		clock_gettime(unix_timespec::fast, &t);
		return t.seconds * resolution + t.nanoseconds;
	}
	static inline timestamp precise()
	{
		struct unix_timespec t;
		clock_gettime(unix_timespec::precise, &t);
		return t.seconds * resolution + t.nanoseconds;
	}
	static inline timestamp now()
	{
		struct unix_timespec t;
		clock_gettime(unix_timespec::realtime, &t);
		return t.seconds * resolution + t.nanoseconds;
	}
};

struct 
{
	unix_timespec time_rate_realtime;
	unix_timespec time_rate_monotonic;
	unix_timespec time_rate_precise;
	unix_timespec time_rate_fast;
	mutex memory_mutex;
	memory::block memory_sentinel;
	uptr pagesize;
	uptr pagemask;
	slice<char*> arguments; // TODO: Is this the correct place? since we know this when starting
} g_unix_state;

inline slice<char*> platform_arguments() { return g_unix_state.arguments; }

// TODO: Remove and use ceil_to_mask
inline u64 ceil_to_pagesize(u64 x)
{
	return x & g_unix_state.pagemask
		? (x | g_unix_state.pagemask) + 1
		: (x);
}
void dispose_lib(libhandle handle)
{
	dlclose(handle);
}
error load_lib(libhandle* handle, const char* name)
{
	return (*handle = dlopen(name, RTLD_NOW | RTLD_LOCAL))
		? error::ok
		: error(0x00000001, "Could not load %v using dlopen with arguments RTLD_NOW|RTLD_LOCAL.", name);
}
template<typename T>
error load_proc(libhandle handle, T& procedure, const char* name)
{
	if (!handle) return error(0x00000001, "Could not search for \"%v\" for the given handle is null.", name);
	var proc = dlsym(handle, name);
	if (!proc) return error(0x00000001, "Could not load \"%v\" for it was not found.", name);
	reinterpret_cast<void*&>(procedure) = proc;
	return error::ok;
}






error init_platform(int argc, char* argv[])
{
	g_unix_state = {};
	g_unix_state.arguments.length = argc;
	g_unix_state.arguments.elements = argv;
	g_unix_state.memory_sentinel._next = &g_unix_state.memory_sentinel;
	g_unix_state.memory_sentinel._previous = &g_unix_state.memory_sentinel;
	clock_getres(unix_timespec::realtime, &g_unix_state.time_rate_realtime);
	clock_getres(unix_timespec::monotonic, &g_unix_state.time_rate_monotonic);
	clock_getres(unix_timespec::precise, &g_unix_state.time_rate_precise);
	clock_getres(unix_timespec::fast, &g_unix_state.time_rate_fast);
	g_unix_state.pagesize = getpagesize();
	g_unix_state.pagemask = g_unix_state.pagesize - 1;
	return error::ok;
}

byte* allocate_raw(uptr* size_, flags32 flags)
{
	constexpr int mmap_protection = PROT_READ | PROT_WRITE;
	int mmap_flags = MAP_ANONYMOUS;
	mmap_flags |= flags & memory::SHARED ? MAP_SHARED : MAP_PRIVATE;
	if (flags & memory::STACK)
		mmap_flags |= MAP_STACK;
	
	var size = *size_;
	if (size & g_unix_state.pagemask)
		*size_ = size = (size | g_unix_state.pagemask) + 1;
	
	var data = mmap(0, size, mmap_protection, mmap_flags, -1, 0);
	if (flags & memory::CLEAR)
	{
		var bytes = (byte*)data;
		for (var i = 0; i < size; i++)
			bytes[i] = 0;
	}
	
	#if SOLITUDE_DEBUG
	{
		s64 pagesize = g_unix_state.pagesize;
		if ((size % pagesize) != 0)
		{
			print("allocated(%v) total size (%v) should be a multiple of %v", flags, size, pagesize);
			print_stacktrace();
		}
		else print("allocated(%v) %v bytes", flags, size);
	}
	#endif
	
	return (byte*)data;
}

void deallocate_raw(void* block, uptr size)
{
	munmap(block, size);
}

memory::block* allocate(uptr size, flags32 flags)
{
	constexpr int mmap_protection = PROT_READ | PROT_WRITE;
	int mmap_flags = MAP_ANONYMOUS;
	mmap_flags |= flags & memory::SHARED ? MAP_SHARED : MAP_PRIVATE;
	
	u64 pagesize = g_unix_state.pagesize;
	u64 pagemask = g_unix_state.pagemask;
	u64 block_size = 64;// sizeof(memory::block);
	u64 start_offset = block_size;
	u64 total_size = size + start_offset;
	if (total_size & pagemask)
		total_size = (total_size | pagemask) + 1;
	
	u64 capacity = total_size - block_size;
	#if OUTSIDE_MEMORY_CHECK
	{
		start_offset = 2 * pagesize;
		total_size = total_size + 3 * pagesize;
	}
	#endif
	
	var data = mmap(0, size, mmap_protection, mmap_flags, -1, 0);
	if (flags & memory::CLEAR)
	{
		var bytes = (byte*)data;
		for (var i = 0; i < size; i++)
			bytes[i] = 0;
	}
	
	#if SOLITUDE_DEBUG
	{
		s64 pagesize = g_unix_state.pagesize;
		if ((size % pagesize) != 0)
		{
			print("allocated(%v) total size (%v) should be a multiple of %v", flags, size, pagesize);
			print_stacktrace();
		}
		else print("allocated(%v) %v bytes", flags, size);
	}
	#endif
	
	var block = (memory::block*)data;
	block->data = (byte*)block + start_offset;
	
	var sentinel = &g_unix_state.memory_sentinel;
	block->_next = sentinel;
	block->capacity = capacity;
	block->flags = flags;
	
	
	#if OUTSIDE_MEMORY_CHECK
	{
		int success;
		success = mprotect((byte*)block + pagesize, pagesize, PROT_NONE);
		assert(success == 0);
		success = mprotect(block->data + block->capacity, pagesize, PROT_NONE);
		assert(success == 0);
	}
	#endif
	
	lock(&g_unix_state.memory_mutex);
	{
		block->_previous = sentinel->_previous;
		block->_previous->_next = block;
		block->_next->_previous = block;
	}
	unlock(&g_unix_state.memory_mutex);

	return block;
}

void deallocate(memory::block* block)
{
	var capacity = block->capacity;
	var total_size = capacity + 64;
	
	lock(&g_unix_state.memory_mutex);
	{
		block->_previous->_next = block->_next;
		block->_next->_previous = block->_previous;
	}
	unlock(&g_unix_state.memory_mutex);
	munmap(block, total_size);
}

void error_popup(error err)
{
	//Video video {};
	//if (init(&video)) CRASH;

}




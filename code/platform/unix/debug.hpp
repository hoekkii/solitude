typedef void (*proc_sighandler)(int);

#if SOLITUDE_DEBUG
/** usr/include/sys/signal.h
 * - register procedure pointer to application signal (such as segmentation violation).
 */
foreign proc_sighandler signal(int, proc_sighandler);

/** usr/include/execinfo.h
 * - retrieve stacktrace.
 */
foreign int backtrace(void**, int);
foreign char** backtrace_symbols(void*const*, int);

/** ? probably stdlib??
 * - the address of the errno variable for the current thread.
 */
foreign int* __errno_location (void);

void print_stacktrace()
{
	constexpr int capacity = 64;
	void* arr[capacity];
	var size  = backtrace(arr, capacity);
	var trace = backtrace_symbols(arr, size);
	if (trace)
	{
		for (var i = 0; i < size; i++)
		{
			var entry = trace[i];
			if (!entry) break;
			print(">>>> %v", entry);
		}
	}
	else
	{
		print(">>>> No trace found.");
	}
}
void crash_handler(int sig)
{
	constexpr int capacity = 64;
	void* arr[capacity];
	var size  = backtrace(arr, capacity);
	var trace = backtrace_symbols(arr, size);

	print("Crash handler (%v):", sig);
	if (trace)
	{
		for (var i = 0; i < size; i++)
		{
			var entry = trace[i];
			if (!entry) break;
			print(">>>> %v", entry);
		}
	}
	else
	{
		print(">>>> No trace found.");
	}
	
	exit(1);
}
#else
proc_sighandler signal(int a, proc_sighandler b) { return b; }
void crash_handler(int a){}
void print_stacktrace(){}
#define __errno_location KeywordReserverd!
#endif

#define backtrace KeywordReserverd!
#define backtrace_symbold KeywordReserverd!





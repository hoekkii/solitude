extern "C"
{
	#if 1 // Hint NVIDIA and AMD to run on GPU
	int NvOptimusEnablement = 1;
	int AmdPowerXpressRequestHighPerformance = 1;
	#endif
}

namespace x11
{

	static constexpr int NoEventMask = 0L;
	static constexpr int KeyPressMask = (1L<<0);
	static constexpr int KeyReleaseMask = (1L<<1);
	static constexpr int ButtonPressMask = (1L<<2);
	static constexpr int ButtonReleaseMask = (1L<<3);
	static constexpr int EnterWindowMask = (1L<<4);
	static constexpr int LeaveWindowMask = (1L<<5);
	static constexpr int PointerMotionMask = (1L<<6);
	static constexpr int PointerMotionHintMask = (1L<<7);
	static constexpr int Button1MotionMask = (1L<<8);
	static constexpr int Button2MotionMask = (1L<<9);
	static constexpr int Button3MotionMask = (1L<<10);
	static constexpr int Button4MotionMask = (1L<<11);
	static constexpr int Button5MotionMask = (1L<<12);
	static constexpr int ButtonMotionMask = (1L<<13);
	static constexpr int KeymapStateMask = (1L<<14);
	static constexpr int ExposureMask = (1L<<15);
	static constexpr int VisibilityChangeMask = (1L<<16);
	static constexpr int StructureNotifyMask = (1L<<17);
	static constexpr int ResizeRedirectMask = (1L<<18);
	static constexpr int SubstructureNotifyMask = (1L<<19);
	static constexpr int SubstructureRedirectMask = (1L<<20);
	static constexpr int FocusChangeMask = (1L<<21);
	static constexpr int PropertyChangeMask = (1L<<22);
	static constexpr int ColormapChangeMask = (1L<<23);
	static constexpr int OwnerGrabButtonMask = (1L<<24);


	// https://code.woboq.org/qt5/include/X11/X.h.html
	// https://github.com/mirror/libX11/blob/623b77d4f30b47258a40f89262e5aa5d25e95fa7/include/X11/Xlib.h
	// https://tronche.com/gui/x/xlib/events/structures.html#XEvent
	union Event
	{
		static constexpr int KeyPress = 2;
		static constexpr int KeyRelease = 3;
		static constexpr int ButtonPress = 4;
		static constexpr int ButtonRelease = 5;
		static constexpr int MotionNotify = 6;
		static constexpr int EnterNotify = 7;
		static constexpr int LeaveNotify = 8;
		static constexpr int FocusIn = 9;
		static constexpr int FocusOut = 10;
		static constexpr int KeymapNotify = 11;
		static constexpr int Expose = 12;
		static constexpr int GraphicsExpose = 13;
		static constexpr int NoExpose = 14;
		static constexpr int VisibilityNotify = 15;
		static constexpr int CreateNotify = 16;
		static constexpr int DestroyNotify = 17;
		static constexpr int UnmapNotify = 18;
		static constexpr int MapNotify = 19;
		static constexpr int MapRequest = 20;
		static constexpr int ReparentNotify = 21;
		static constexpr int ConfigureNotify = 22;
		static constexpr int ConfigureRequest = 23;
		static constexpr int GravityNotify = 24;
		static constexpr int ResizeRequest = 25;
		static constexpr int CirculateNotify = 26;
		static constexpr int CirculateRequest = 27;
		static constexpr int PropertyNotify = 28;
		static constexpr int SelectionClear = 29;
		static constexpr int SelectionRequest = 30;
		static constexpr int SelectionNotify = 31;
		static constexpr int ColormapNotify = 32;
		static constexpr int ClientMessage = 33;
		static constexpr int MappingNotify = 34;
		static constexpr int GenericEvent = 35;
		static constexpr int LASTEvent = 36;

		struct {
			int type;		/* KeyPress or KeyRelease */
			unsigned long serial;	/* # of last request processed by server */
			int send_event;	/* true if this came from a SendEvent request */
			void* display;	/* Display the event was read from */
			uptr window;		/* ``event'' window it is reported relative to */
			uptr root;		/* root window that the event occurred on */
			uptr subwindow;	/* child window */
			unsigned long time;		/* milliseconds */
			int x, y;		/* pointer x, y coordinates in event window */
			int x_root, y_root;	/* coordinates relative to root */
			unsigned int state;	/* key or button mask */
			unsigned int keycode;	/* detail */
			int same_screen;	/* same screen flag */
		} key;
		struct {
			int type;		/* MotionNotify */
			unsigned long serial;	/* # of last request processed by server */
			int send_event;	/* true if this came from a SendEvent request */
			void* display;	/* Display the event was read from */
			uptr window;		/* ``event'' window reported relative to */
			uptr root;		/* root window that the event occurred on */
			uptr subwindow;	/* child window */
			unsigned long time;		/* milliseconds */
			int x, y;		/* pointer x, y coordinates in event window */
			int x_root, y_root;	/* coordinates relative to root */
			unsigned int state;	/* key or button mask */
			char is_hint;		/* detail */
			int same_screen;	/* same screen flag */
		} motion;

		int type;
		long padding[24];
	};
	
	
	static constexpr byte keyboard[] = {
		Keyboard::unk, // 000
		Keyboard::unk, // 001
		Keyboard::unk, // 002
		Keyboard::unk, // 003
		Keyboard::unk, // 004
		Keyboard::unk, // 005
		Keyboard::unk, // 006
		Keyboard::unk, // 007
		Keyboard::unk, // 008
		Keyboard::esc, // 009
		Keyboard::unk, // 010
		Keyboard::unk, // 011
		Keyboard::unk, // 012
		Keyboard::unk, // 013
		Keyboard::unk, // 014
		Keyboard::unk, // 015
		Keyboard::unk, // 016
		Keyboard::unk, // 017
		Keyboard::unk, // 018
		Keyboard::unk, // 019
		Keyboard::unk, // 020
		Keyboard::unk, // 021
		Keyboard::unk, // 022
		Keyboard::unk, // 023
		Keyboard::q, // 024
		Keyboard::w, // 025
		Keyboard::e, // 026
		Keyboard::unk, // 027
		Keyboard::unk, // 028
		Keyboard::unk, // 029
		Keyboard::unk, // 030
		Keyboard::unk, // 031
		Keyboard::unk, // 032
		Keyboard::unk, // 033
		Keyboard::unk, // 034
		Keyboard::unk, // 035
		Keyboard::unk, // 036
		Keyboard::unk, // 037
		Keyboard::a, // 038
		Keyboard::s, // 039
		Keyboard::d, // 040
		Keyboard::unk, // 041
		Keyboard::unk, // 042
		Keyboard::unk, // 043
		Keyboard::unk, // 044
		Keyboard::unk, // 045
		Keyboard::unk, // 046
		Keyboard::unk, // 047
		Keyboard::unk, // 048
		Keyboard::unk, // 049
		Keyboard::shift, // 050
		Keyboard::unk, // 051
		Keyboard::unk, // 052
		Keyboard::unk, // 053
		Keyboard::unk, // 054
		Keyboard::unk, // 055
		Keyboard::unk, // 056
		Keyboard::unk, // 057
		Keyboard::unk, // 058
		Keyboard::unk, // 059
		Keyboard::unk, // 060
		Keyboard::unk, // 061
		Keyboard::shift, // 062
		Keyboard::unk, // 063
		Keyboard::unk, // 064
	};
}


struct video
{
	libhandle libx11;
	uptr window;
	void* display;
	int screen;

	// X11 Procedures
	void* (*XOpenDisplay)(char*);
	int   (*XDefaultScreen)(void*);
	uptr  (*XRootWindow)(void*, int);
	uptr  (*XBlackPixel)(void*, int);
	uptr  (*XWhitePixel)(void*, int);
	uptr  (*XCreateSimpleWindow)(void*, uptr, int, int, uint, uint, uint, uptr, uptr);
	void  (*XSelectInput)(void*, uptr, s64);
	void  (*XMapWindow)(void*, uptr);
	int   (*XPending)(void*);
	void  (*XNextEvent)(void*, x11::Event*);
	void  (*XCloseDisplay)(void*);
};

error show(video* v)
{
	return error::ok;
}

error init(video* v)
{
	error err;
	if (!v->libx11)
	{
		libhandle handle;
		if ((err = load_lib(&handle, "libX11.so")) && load_lib(&handle, "libX11.so.6"))
			return err;
		
		v->libx11 = handle;
		LOAD_FNPTRP(v, XOpenDisplay);
		LOAD_FNPTRP(v, XDefaultScreen);
		LOAD_FNPTRP(v, XRootWindow);
		LOAD_FNPTRP(v, XBlackPixel);
		LOAD_FNPTRP(v, XWhitePixel);
		LOAD_FNPTRP(v, XCreateSimpleWindow);
		LOAD_FNPTRP(v, XSelectInput);
		LOAD_FNPTRP(v, XMapWindow);
		LOAD_FNPTRP(v, XPending);
		LOAD_FNPTRP(v, XNextEvent);
		LOAD_FNPTRP(v, XCloseDisplay);
	}
	
	

	// TODO: replace null with user defined display	
	var display = v->XOpenDisplay(null);
	if (display == null)
		return error(0x0000001, "Cannot open display using XOpenDisplay(null).");
	
	var screen = v->XDefaultScreen(display);
	var root = v->XRootWindow(display, screen);
	var black = v->XBlackPixel(display, screen);
	var white = v->XWhitePixel(display, screen);
	var window = v->XCreateSimpleWindow(display, root, 10, 10, 100, 100, 1, black, white);
	v->XSelectInput(display, window, x11::ExposureMask
		| x11::KeyPressMask
		| x11::KeyReleaseMask
		| x11::ButtonPressMask
		| x11::ButtonReleaseMask
		| x11::EnterWindowMask
		| x11::LeaveWindowMask
		| x11::PointerMotionMask);
	v->XMapWindow(display, window);
	
	print("AADD");
	v->window = window;
	v->display = display;
	v->screen = screen;
	return error::ok;
}

InputEvent poll(video* v)
{
	InputEvent result {};
	x11::Event event;
	if (v->XPending(v->display))
	{
		result.type = -1;
		v->XNextEvent(v->display, &event);
		switch (event.type)
		{
			case x11::Event::KeyPress:
			{
				result.type = InputEvent::Key;
				result.key.code = x11::keyboard[event.key.keycode];
				result.key.value = 1;
				print("I pressed key: %v", event.key.keycode);
			} break;
			case x11::Event::KeyRelease:
			{
				result.type = InputEvent::Key;
				result.key.code = x11::keyboard[event.key.keycode];
				result.key.value = 0;
			} break;
			case x11::Event::ButtonPress:
			{
				result.type = InputEvent::Key;
				result.key.code = event.key.keycode;
				result.key.value = 0;
				print("I pressed a button: %v", event.key.keycode);
			} break;
			case x11::Event::ButtonRelease:
			{
				result.type = InputEvent::Key;
				result.key.code = event.key.keycode;
				result.key.value = 0;
			} break;
			case x11::Event::MotionNotify:
			{
				/*
					For input: Change X11 to X Input, to try and get more
					accurate value. Until now, (current) X11 cannot do
					sub-pixel pointer events. And also isn't able to
					access different devices such as a gamepad and midi-controller.
				*/
				//print("Motion value; x:%v, y:%v b:%v", event.motion.x, event.motion.y, event.motion.is_hint);
				result.type = InputEvent::Position;
				result.position.code = 1;
				result.position.x = event.motion.x;
				result.position.y = event.motion.y;

			} break;
		}
	}
	return result;
}

void dispose(video* v)
{
	if (v->display)
	{
		v->XCloseDisplay(v->display);
		v->XCloseDisplay = null;
		v->display = null;
	}

	if (v->libx11)
	{
		dispose_lib(v->libx11);
		v->libx11 = null;
	}
}









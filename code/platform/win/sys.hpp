#if SOLITUDE_X64
#define null nullptr
typedef long long unsigned int uptr;
typedef long long signed int sptr;
typedef long long unsigned int flags64;
typedef long long unsigned int  u64;
typedef long long   signed int  s64;
#else
#define null 0
typedef unsigned int uptr;
typedef signed int sptr;
typedef long long unsigned int flags64;
typedef long long unsigned int  u64;
typedef long long   signed int  s64;
#endif

typedef         signed char sbyte;
typedef       unsigned char byte;
typedef       unsigned char uchar;
typedef short unsigned int  ushort;
typedef       unsigned int  uint;
typedef       unsigned int  flags32;
typedef       unsigned char u8;
typedef short unsigned int  u16;
typedef       unsigned int  u32;
typedef         signed char s8;
typedef short   signed int  s16;
typedef         signed int  s32;



// Special datatypes
typedef HMODULE libhandle;
typedef long platform_long;
typedef unsigned long platform_ulong;

#define foreign extern "C"

//#include <stdio.h>

foreign void* __cdecl __acrt_iob_func(unsigned _Ix);
#define stdout (__acrt_iob_func(1))


foreign void* malloc(uptr);
foreign void  free(void*);


#define SEEK_SET 0
#define SEEK_CUR 1
#define SEEK_END 2

foreign void* fopen(const char*, const char*);
foreign int fclose(void*);
foreign int fflush(void*);
foreign uptr fread(void*, uptr, uptr, void*);
foreign uptr fwrite(const void*, uptr, uptr, void*);
foreign int fputc(int, void*);
foreign int fputs(const char*, void*);
foreign int fseek(void*, long, int);
foreign platform_long ftell(void*);


foreign int open(const char*, int, ...);
foreign sptr read(int, void*, uptr);
foreign int ioctl(int, unsigned long, ...);





foreign short __cdecl _dtest(_In_ double* _Px);
foreign short __cdecl _ldtest(_In_ long double* _Px);
foreign short __cdecl _fdtest(_In_ float* _Px);
inline bool isinf(float x) { return _fdtest(&x) == 1; }
inline bool isinf(double x) { return _dtest(&x) == 1; }
inline bool isinf(long double x) { return _ldtest(&x) == 1; }
inline bool isnan(float x) { return _fdtest(&x) == 2; }
inline bool isnan(double x) { return _dtest(&x) == 2; }
inline bool isnan(long double x) { return _ldtest(&x) == 2; }



foreign float	acosf(float);
foreign float	asinf(float);
foreign float	atanf(float);
foreign float	atan2f(float, float);
foreign float	cosf(float);
foreign float	sinf(float);
foreign float	tanf(float);
foreign float	coshf(float);
foreign float	sinhf(float);
foreign float	tanhf(float);
foreign float	exp2f(float);
foreign float	expf(float);
foreign float	expm1f(float);
foreign float	frexpf(float, int *);	/* fundamentally !__pure2 */
foreign float	ldexpf(float, int);
foreign float	log10f(float);
foreign float	log1pf(float);
foreign float	log2f(float);
foreign float	logf(float);
foreign float	modff(float, float *);	/* fundamentally !__pure2 */
foreign float	powf(float, float);
foreign float	sqrtf(float);
foreign float	ceilf(float);
foreign float	fabsf(float);
foreign float	floorf(float);
foreign float	fmodf(float, float);
foreign float	roundf(float);
foreign float	erff(float);
foreign float	erfcf(float);
foreign float	hypotf(float, float);
foreign float	lgammaf(float);
foreign float	tgammaf(float);
foreign float	acoshf(float);
foreign float	asinhf(float);
foreign float	atanhf(float);
foreign float	cbrtf(float);
foreign float	logbf(float);
foreign long long llrintf(float);
foreign long long llroundf(float);
foreign long	lrintf(float);
foreign long	lroundf(float);
foreign float	nearbyintf(float);
foreign float	nextafterf(float, float);
foreign float	remainderf(float, float);
foreign float	remquof(float, float, int *);
foreign float	rintf(float);
foreign float	scalblnf(float, long);
foreign float	scalbnf(float, int);
foreign float	truncf(float);
foreign float	fdimf(float, float);
foreign float	fmaf(float, float, float);


// TODO: Look if these can be replaced by intrinsics
inline float frac(float x)     { return x - floorf(x); }
inline float rsqrt(float x)    { return 1.0f / sqrtf(x); }
inline float saturate(float x) { return x < 0 ? 0 : x > 1 ? 1 : x; }
inline float radians(float x)  { return x * 57.29577951308232f; }
inline float degrees(float x)  { return x * 0.017453292519943295f; }



#define abs   fabsf
#define acos  acosf
#define asin  asinf
#define atan  atanf
#define atan2 atan2f
#define ceil  ceilf
#define cbrt  cbrtf
#define cos   cosf
#define cosh  coshf
#define exp   expf
#define exp2  exp2f
#define floor floorf
#define fmod  fmodf
#define log   logf
#define log10 log10f
#define log2  log2f
#define modf  modff
#define pow   powf
#define round roundf
#define sin   sinf
#define sinh  sinhf
#define sqrt  sqrtf
#define tan   tanf
#define tanh  tanhf
#define trunc truncf

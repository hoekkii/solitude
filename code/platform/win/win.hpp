#define FNPTR(x) reinterpret_cast<void*&>(x)
#define LOAD_FNPTR(name) \
	reinterpret_cast<void*&>(name) = dlsym(handle, #name); \
	if (!name) { \
		dlclose(handle); \
		return make_error("Could not find " #name " using dlsym."); \
	}

// TODO: should we remove the global scope and use the api design we apply to other things?
struct 
{
	mutex memory_mutex;
	memory::block memory_sentinel;
	LARGE_INTEGER time_rate;
	uptr pagesize;
	uptr pagemask;
	slice<char*> arguments; // TODO: Is this the correct place? since we know this when starting
} g_win_state;

inline slice<char*> platform_arguments() { return g_win_state.arguments; }

// Use cel_to_mask instead
inline u64 ceil_to_pagesize(u64 x)
{
	return x & g_win_state.pagemask
		? (x | g_win_state.pagemask) + 1
		: (x);
}


error init_platform(int argc, char* argv[])
{
	g_win_state = {};
	g_win_state.arguments.length = argc;
	g_win_state.arguments.elements = argv;
	g_win_state.memory_sentinel._next = &g_win_state.memory_sentinel;
	g_win_state.memory_sentinel._previous = &g_win_state.memory_sentinel;
	QueryPerformanceFrequency(&g_win_state.time_rate);
	
	SYSTEM_INFO info;
	GetSystemInfo(&info);
	g_win_state.pagesize = info.dwPageSize;
	g_win_state.pagemask = info.dwPageSize - 1;
	
	return error::ok;
}

byte* allocate_raw(uptr* size_, flags32 flags)
{
	constexpr int alloc_protection = PAGE_READWRITE;
	constexpr int alloc_flags = MEM_RESERVE | MEM_COMMIT;
	
	var size = *size_;
	if (size & g_win_state.pagemask)
		*size_ = size = (size | g_win_state.pagemask - 1) + 1;
		
	var data = VirtualAlloc(0, size, alloc_flags, alloc_protection);
	return (byte*)data;
}

void deallocate_raw(void* block, uptr size)
{
	VirtualFree(block, size, MEM_RELEASE);
}

#if SOLITUDE_DEBUG && 1
#define OUTSIDE_MEMORY_CHECK 1
#endif
// allocate(&(my_size = 64), 0)

/**
 * @brief 
 * 
 * @param size 
 * @param flags 
 * @return memory::block* 
 */
memory::block* allocate(uptr size, flags32 flags)
{
	constexpr int alloc_protection = PAGE_READWRITE;
	constexpr int alloc_flags = MEM_RESERVE | MEM_COMMIT;
	
	u64 pagesize = g_win_state.pagesize;
	u64 block_size = 64;// sizeof(memory::block);
	u64 start_offset = block_size;
	u64 total_size = size + start_offset;
	if (total_size % pagesize)
		total_size = (total_size | (pagesize - 1)) + 1;
	
	u64 capacity = total_size - block_size;
	#if OUTSIDE_MEMORY_CHECK
	{
		start_offset = 2 * pagesize;
		total_size = total_size + 3 * pagesize;
	}
	#endif
	
	var block = (memory::block*)VirtualAlloc(0, total_size, alloc_flags, alloc_protection);
	block->data = (byte*)block + start_offset;
	
	var sentinel = &g_win_state.memory_sentinel;
	block->_next = sentinel;
	block->capacity = capacity;
	block->flags = flags;
	
	
	#if OUTSIDE_MEMORY_CHECK
	{
        DWORD p = 0;
		BOOL success;
		success = VirtualProtect((byte*)block + pagesize, pagesize, PAGE_NOACCESS, &p);
		assert(success);
		success = VirtualProtect(block->data + block->capacity, pagesize, PAGE_NOACCESS, &p);
		assert(success);
	}
	#endif
	
	lock(&g_win_state.memory_mutex);
	{
		block->_previous = sentinel->_previous;
		block->_previous->_next = block;
		block->_next->_previous = block;
	}
	unlock(&g_win_state.memory_mutex);

	return block;
}

void deallocate(memory::block* block)
{
	var capacity = block->capacity;
	var total_size = capacity + 64;//sizeof(memory::block);
	
	lock(&g_win_state.memory_mutex);
	{
		block->_previous->_next = block->_next;
		block->_next->_previous = block->_previous;
	}
	unlock(&g_win_state.memory_mutex);
	VirtualFree(block, total_size, MEM_RELEASE);
}



void error_popup(error err)
{
	var message = err.message;
	var data = message.data;
	var length = message.length;
	if (data[length])
	{
		if (length < 4096)
		{
			char tmp[4096];
			copy(tmp, data, length);
			tmp[length] = 0;
			MessageBoxA(0, tmp, "Solitude Error!", 0);
		}
	}
	else
	{
		MessageBoxA(0, (LPCSTR)data, "Solitude Error!", 0);
	}
}

void platform_assert()
{
	DWORD last_error = GetLastError();
	if (last_error)
	{
		LPCSTR data;
		FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER |  FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			last_error,
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			(LPTSTR) &data,
			0,
			NULL);
		
		MessageBoxA(0, data, "Solitude Error!", 0);
		ExitProcess(last_error);
	}
}

error load_lib(libhandle* handle, const char* name)
{
	var result = *handle = LoadLibraryA(name);
	return result ? error::ok : error(0x00000001, "Could not open \"%v\" using LoadLibrary." , name);
}
void dispose_lib(libhandle handle)
{
	FreeLibrary(handle);
}
template<typename T>
error load_proc(libhandle handle, T& procedure, const char* name)
{
	if (!handle) return error(0x00000001, "Could not search for \"%v\" for the given handle is null.", name);
	var proc = GetProcAddress(handle, name);
	if (!proc) return error(0x00000001, "Could not load \"%v\" for it was not found.", name);
	reinterpret_cast<void*&>(procedure) = proc;
	return error::ok;
}




typedef s64 timestamp;
namespace time
{
	static constexpr double resolution_d = 1e9;
	static constexpr timestamp resolution = 1000000000;
	static constexpr double to(timestamp t, double s)   { return t * (s / resolution); }
	static constexpr timestamp from(double t, double s) { return (timestamp)(t * (resolution / s)); }

	constexpr double to_ns(timestamp t) { return to(t, 1e9); }
	constexpr double to_us(timestamp t) { return to(t, 1e6); }
	constexpr double to_ms(timestamp t) { return to(t, 1e3); }
	constexpr double to_s(timestamp t)  { return to(t, 1e0); }
	constexpr timestamp ns(double t)  { return from(t, 1e9); }
	constexpr timestamp us(double t)  { return from(t, 1e6); }
	constexpr timestamp ms(double t)  { return from(t, 1e3); }
	constexpr timestamp s(double t)   { return from(t, 1e0); }
	constexpr timestamp m(double t)   { return from(t, 1.0 / 60); }
	constexpr timestamp h(double t)   { return from(t, 1.0 / 3600); }
	
	static inline timestamp monotonic()
	{
		LARGE_INTEGER now;
		QueryPerformanceCounter(&now);
		return (timestamp)((resolution_d * now.QuadPart) / g_win_state.time_rate.QuadPart);
	}
	static inline timestamp fast() { return monotonic(); }
	static inline timestamp precise() { return monotonic(); }
	static inline timestamp now() { return monotonic(); }
}



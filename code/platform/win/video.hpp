
struct video
{
	HWND window;
	HINSTANCE display;


	static constexpr byte keyboard[] = {
		Keyboard::unk, // 000
		Keyboard::unk, // 001
		Keyboard::unk, // 002
		Keyboard::unk, // 003
		Keyboard::unk, // 004
		Keyboard::unk, // 005
		Keyboard::unk, // 006
		Keyboard::unk, // 007
		Keyboard::unk, // 008
		Keyboard::unk, // 009
		Keyboard::unk, // 010
		Keyboard::unk, // 011
		Keyboard::unk, // 012
		Keyboard::unk, // 013
		Keyboard::unk, // 014
		Keyboard::unk, // 015
		Keyboard::shift, // 016
		Keyboard::unk, // 017
		Keyboard::unk, // 018
		Keyboard::unk, // 019
		Keyboard::unk, // 020
		Keyboard::unk, // 021
		Keyboard::unk, // 022
		Keyboard::unk, // 023
		Keyboard::unk, // 024
		Keyboard::unk, // 025
		Keyboard::unk, // 026
		Keyboard::esc, // 027
		Keyboard::unk, // 028
		Keyboard::unk, // 029
		Keyboard::unk, // 030
		Keyboard::unk, // 031
		Keyboard::unk, // 032
		Keyboard::unk, // 033
		Keyboard::unk, // 034
		Keyboard::unk, // 035
		Keyboard::unk, // 036
		Keyboard::unk, // 037
		Keyboard::unk, // 038
		Keyboard::unk, // 039
		Keyboard::unk, // 040
		Keyboard::unk, // 041
		Keyboard::unk, // 042
		Keyboard::unk, // 043
		Keyboard::unk, // 044
		Keyboard::unk, // 045
		Keyboard::unk, // 046
		Keyboard::unk, // 047
		Keyboard::unk, // 048
		Keyboard::unk, // 049
		Keyboard::unk, // 050
		Keyboard::unk, // 051
		Keyboard::unk, // 052
		Keyboard::unk, // 053
		Keyboard::unk, // 054
		Keyboard::unk, // 055
		Keyboard::unk, // 056
		Keyboard::unk, // 057
		Keyboard::unk, // 058
		Keyboard::unk, // 059
		Keyboard::unk, // 060
		Keyboard::unk, // 061
		Keyboard::unk, // 062
		Keyboard::unk, // 063
		Keyboard::unk, // 064
		Keyboard::a, // 065
		Keyboard::b, // 066
		Keyboard::c, // 067
		Keyboard::d, // 068
		Keyboard::e, // 069
		Keyboard::f, // 070
		Keyboard::g, // 071
		Keyboard::h, // 072
		Keyboard::i, // 073
		Keyboard::j, // 074
		Keyboard::k, // 075
		Keyboard::l, // 076
		Keyboard::m, // 077
		Keyboard::n, // 078
		Keyboard::o, // 079
		Keyboard::p, // 080
		Keyboard::q, // 081
		Keyboard::r, // 082
		Keyboard::s, // 083
		Keyboard::t, // 084
		Keyboard::u, // 085
		Keyboard::v, // 086
		Keyboard::w, // 087
		Keyboard::x, // 088
		Keyboard::y, // 089
		Keyboard::z, // 090
	};
};


static LRESULT CALLBACK Win32MainWindowCallback(HWND window, UINT message, WPARAM wparam, LPARAM lparam)
{
	return DefWindowProc(window, message, wparam, lparam);
}

void fullscreen(video* v)
{
	var window = v->window;
	int w = GetSystemMetrics(SM_CXSCREEN);
	int h = GetSystemMetrics(SM_CYSCREEN);
	SetWindowLongPtrA(window, GWL_STYLE, WS_VISIBLE | WS_POPUP);
	SetWindowPos(window, HWND_TOP, 0, 0, w, h, SWP_FRAMECHANGED);
}

error init(video* v)
{
	HINSTANCE instance = GetModuleHandle(0);
	WNDCLASSA window_class = {};
	window_class.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	window_class.lpfnWndProc = Win32MainWindowCallback;
	window_class.cbClsExtra = 0;
	window_class.cbWndExtra = 0;
	window_class.hInstance = instance;
	window_class.hIcon = LoadIcon(0, IDC_ARROW);
	window_class.hCursor = LoadCursor(0, IDC_ARROW);
	window_class.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	window_class.lpszMenuName = 0;
	window_class.lpszClassName = "SolitudeWindowClass";
	if (!RegisterClassA(&window_class))
		return error(0x00000001, "Failed to call RegisterClassA");

	HWND window = CreateWindowExA(
		0, // WS_EX_TOPMOST|WS_EX_LAYERED,
		window_class.lpszClassName,
		"Solitude",
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		0,
		0,
		instance,
		0);
	
	if (!window)
		return error(0x00000001, "Failed to call CreateWindowExA");
	
	v->window = window;
	v->display = instance;
	return error::ok;
}

static error show(video* video)
{
	ShowWindow(video->window, SW_SHOW);
	return error::ok;
}


InputEvent poll(video* v)
{
	InputEvent result {};
	MSG event;
	if (PeekMessageA(&event, v->window, 0, 0, PM_REMOVE) > 0)
	{
		TranslateMessage(&event);
		DispatchMessageA(&event);
		switch (event.message)
		{
			case WM_SYSKEYDOWN:
			{
				result.type = InputEvent::Key;
				result.key.code = event.wParam;
				result.key.value = 1;
			} break;
			case WM_SYSCHAR: { } break;
			case WM_SYSKEYUP: { } break;
			case WM_KEYDOWN:
			{
				result.type = InputEvent::Key;
				result.key.code = event.wParam < sizeof(video::keyboard)
					? video::keyboard[event.wParam]
					: event.wParam;
				result.key.value = 1;
				print("WM_KEYDOWN: %v %v", event.wParam, event.lParam);
			} break;
			case WM_KEYUP:
			{
				result.type = InputEvent::Key;
				result.key.code = event.wParam < sizeof(video::keyboard)
					? video::keyboard[event.wParam]
					: event.wParam;
				result.key.value = 0;
			} break;
			case WM_CHAR: { } break;
			case WM_QUIT:
			{
				result.type = InputEvent::Event;
				result.event.code = SystemEvent::quit;
				result.event.value = 1;
			} break;
			default:
			{
				//print("UNKOWN: %v", event.message);
			} break;
		}
	}
	return result;
}


void dispose(video* v)
{

}
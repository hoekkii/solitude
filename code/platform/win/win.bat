@echo off
SETLOCAL

set /A release=0
set /A run=0
set /A shaders=0
set /A help=0
set /A win=0
set /A wasm=0
set /A todo=0
for %%x in (%*) do (
	if "%%x" == "-e" set /A release=1
	if "%%x" == "--release" set /A release=1
	if "%%x" == "-r" set /A run=1
	if "%%x" == "--run" set /A run=1
	if "%%x" == "-s" set /A shaders=1
	if "%%x" == "--shaders" set /A shaders=1
	if "%%x" == "-b" set /A win=1
	if "%%x" == "--windows" set /A win=1
	if "%%x" == "-w" set /A wasm=1
	if "%%x" == "--wasm" set /A wasm=1
	if "%%x" == "-t" set /A todo=1
	if "%%x" == "--todo" set /A todo=1
	if "%%x" == "?" set /A help=1
	if "%%x" == "-h" set /A help=1
	if "%%x" == "--help" set /A help=1
)

if %help% GTR 0 (
	echo -e --release  Export a build of the game
	echo -r --run      Run the game after compilation
	echo -s --shaders  Build all shaders
	echo -b --windows  Add the current Windows system as targeted build
	echo -w --wasm     Add WebAssembly as targeted build using emscripten
	echo -t --todo     Scan the project for todos
	echo -h --help     Print available commands
	goto __exit
)

if %todo% GTR 0 (
	echo TODO:
	findstr /L /S /I /N "TODO: INCOMPLETE:" *
	REM findstr /L /S /I /N "TODO: INCOMPLETE:" * | findstr "\/\/ \#"
	echo:
	echo:
	echo:
	echo NEGLECT:
	findstr /L /S /I /N "HACK: BUG:" *
	goto __exit
)

if %shaders% GTR 0 (
	for /r %~dp0shaders\ %%x in (*.*) do (
		echo C:\usr\lib\VulkanSDK\1.2.189.2\Bin32\glslc.exe %%x -o %~dp0..\assets\shaders\%%~nx%%~xx.spv
		start C:\usr\lib\VulkanSDK\1.2.189.2\Bin32\glslc.exe %%x -o %~dp0..\assets\shaders\%%~nx%%~xx.spv
	)
	goto __exit
)


if %win% GTR 0 (
	echo Building from commandline is not yet supported!
	echo Use the solution in '/solutions/windows'.
)

if %wasm% GTR 0 (
	start emcc -o "../builds/wasm/index.html" "./solitude.cpp" -O3 -Wno-parentheses --shell-file "./wasm.html" -s NO_EXIT_RUNTIME=1 -s "EXPORTED_RUNTIME_METHODS=['ccall']"

)


:__exit
ENDLOCAL

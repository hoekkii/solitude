struct
{
	mach_timebase_info_data_t nsclock_info;
} g_apple_state;

error init_platform()
{
	g_apple_sate = {};
	mach_timebase_info(&g_apple_state.nsclock_info);

}

inline u64 nsclock()
{
	u64 now;
	now = mach_absolute_time();
	now *= g_apple_state.nsclock_info.numer;
	now /= g_apple_state.nsclock_info.denom;
	return now;
}


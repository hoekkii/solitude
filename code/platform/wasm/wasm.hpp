
struct
{
	u64 time_rate_realtime;
	u64 time_rate_monotonic;
	u64 time_rate_precise;
	u64 time_rate_fast;
	mutex memory_mutex;
	memory::block memory_sentinel;
	uptr pagesize;
	void* main_state;
} g_wasm_state;

namespace time
{
	static constexpr int platform_realtime = 0;
	static constexpr int platform_monotonic = 4;
	static constexpr int platform_precise = 4;
	static constexpr int platform_fast = 4;
	
	
	static constexpr s64 resolution = 1e9;
	static constexpr double to(s64 t, double s)   { return t * (s / resolution); }
	static constexpr s64 from(double t, double s) { return (s64)(t * (resolution / s)); }

	constexpr double to_ns(s64 t) { return to(t, 1e9); }
	constexpr double to_us(s64 t) { return to(t, 1e6); }
	constexpr double to_ms(s64 t) { return to(t, 1e3); }
	constexpr double to_s(s64 t)  { return to(t, 1e0); }
	constexpr s64 ns(double t)  { return from(t, 1e9); }
	constexpr s64 us(double t)  { return from(t, 1e6); }
	constexpr s64 ms(double t)  { return from(t, 1e3); }
	constexpr s64 s(double t)   { return from(t, 1e0); }
	constexpr s64 m(double t)   { return from(t, 1.0 / 60); }
	constexpr s64 h(double t)   { return from(t, 1.0 / 3600); }
	
	static inline s64 monotonic()
	{
		u64 t;
		__wasi_clock_time_get(platform_monotonic, g_wasm_state.time_rate_monotonic, &t);
		return (s64)t;
	}
	static inline s64 fast()
	{
		u64 t;
		__wasi_clock_time_get(platform_fast, g_wasm_state.time_rate_fast, &t);
		return (s64)t;
	}
	static inline s64 precise()
	{
		u64 t;
		__wasi_clock_time_get(platform_precise, g_wasm_state.time_rate_precise, &t);
		return (s64)t;
	}
	static inline s64 now()
	{
		u64 t;
		__wasi_clock_time_get(platform_realtime, g_wasm_state.time_rate_realtime, &t);
		return (s64)t;
	}
};



error init_platform()
{
	g_wasm_state = {};
	g_wasm_state.memory_sentinel._next = &g_wasm_state.memory_sentinel;
	g_wasm_state.memory_sentinel._previous = &g_wasm_state.memory_sentinel;
	__wasi_clock_res_get(time::platform_realtime, &g_wasm_state.time_rate_realtime);
	__wasi_clock_res_get(time::platform_monotonic, &g_wasm_state.time_rate_monotonic);
	__wasi_clock_res_get(time::platform_precise, &g_wasm_state.time_rate_precise);
	__wasi_clock_res_get(time::platform_fast, &g_wasm_state.time_rate_fast);
	g_wasm_state.pagesize = 65536;
	return error::ok;
}

byte* allocate_raw(uptr* size_, flags32 flags)
{
	constexpr int mmap_protection = PROT_READ | PROT_WRITE;
	int mmap_flags = MAP_ANONYMOUS;
	mmap_flags |= flags & memory::SHARED ? MAP_SHARED : MAP_PRIVATE;
	if (flags & memory::STACK)
		mmap_flags |= MAP_STACK;
	
	var size = *size_;
	if (size % g_wasm_state.pagesize)
		*size_ = size = (size | (g_wasm_state.pagesize - 1)) + 1;
	
	var data = mmap(0, size, mmap_protection, mmap_flags, -1, 0);
	if (flags & memory::CLEAR)
	{
		var bytes = (byte*)data;
		for (var i = 0; i < size; i++)
			bytes[i] = 0;
	}
	
	#if SOLITUDE_DEBUG
	{
		s64 pagesize = g_wasm_state.pagesize;
		if ((size % pagesize) != 0)
		{
			print("allocated(%v) total size (%v) should be a multiple of %v", flags, size, pagesize);
			print_stacktrace();
		}
		else print("allocated(%v) %v bytes", flags, size);
	}
	#endif
	
	return (byte*)data;
}

void deallocate_raw(void* block, uptr size)
{
	munmap(block, size);
}

memory::block* allocate(uptr size, flags32 flags)
{
	var offset = 64;
	var sentinel = &g_wasm_state.memory_sentinel;
	var block = (memory::block*)allocate_raw(&size, flags);
	block->data = (byte*)block + offset;
	block->capacity = size - offset;
	block->length = 0;
	block->flags = flags;
	block->previous = null;
	block->_previous = null;
	block->_next = sentinel;
	
	#if SOLITUDE_DEBUG
	{
		if (offset < sizeof(memory::block))
			print("ERROR: header of allocated memory should ");
	}
	#endif
	
	lock(&g_wasm_state.memory_mutex);
	{
		block->_previous = sentinel->_previous;
		block->_previous->_next = block;
		block->_next->_previous = block;
	}
	unlock(&g_wasm_state.memory_mutex);
	return block;
}

void deallocate(memory::block* block)
{
	var capacity = block->capacity;
	var total_size = capacity + 64;
	
	lock(&g_wasm_state.memory_mutex);
	{
		block->_previous->_next = block->_next;
		block->_next->_previous = block->_previous;
	}
	unlock(&g_wasm_state.memory_mutex);
	munmap(block, total_size);
}

void error_popup(error err)
{
	//Video video {};
	//if (init(&video)) CRASH;

}

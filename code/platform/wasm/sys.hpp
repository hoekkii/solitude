#if SOLITUDE_X64
//#define null nullptr
typedef long unsigned int uptr;
typedef long   signed int sptr;
typedef long unsigned int flags64;
typedef long unsigned int u64;
typedef long   signed int s64;
#else
//#define null 0
typedef           unsigned int uptr;
typedef             signed int sptr;
typedef long long unsigned int flags64;
typedef long long unsigned int u64;
typedef long long   signed int s64;
#endif

typedef         signed char sbyte;
typedef       unsigned char byte;
typedef       unsigned char uchar;
typedef short unsigned int  ushort;
typedef       unsigned int  uint;
typedef       unsigned int  flags32;
typedef       unsigned char u8;
typedef short unsigned int  u16;
typedef       unsigned int  u32;
typedef         signed char s8;
typedef short   signed int  s16;
typedef         signed int  s32;

typedef void* libhandle;
typedef long platform_long; 
typedef unsigned long platform_ulong;
typedef u16 wasm_error;


#define foreign extern "C"
#define EXPORT extern "C" __attribute__((used))
#define EM_foreign(NAME) __attribute__((import_module("env"), import_name(#NAME)))


foreign void* stdout;
foreign uptr fwrite(const void*, uptr, uptr, void*);
foreign int fflush(void*);

/**
 * emsripten
 * 
 */
typedef void (*em_callback_func)(void);
typedef void (*em_arg_callback_func)(void*);
typedef void (*em_str_callback_func)(const char *);
foreign void emscripten_console_log(const char*);
foreign void emscripten_console_warn(const char*);
foreign void emscripten_console_error(const char*);
foreign void emscripten_run_script(const char*);
foreign int emscripten_run_script_int(const char*);
foreign char *emscripten_run_script_string(const char*);
foreign void emscripten_set_main_loop(em_callback_func func, int fps, int simulate_infinite_loop);
foreign char *emscripten_get_window_title();
foreign void emscripten_set_window_title(const char*);
foreign void emscripten_get_screen_size(int *width, int *height);
foreign void emscripten_hide_mouse(void);
foreign double emscripten_get_now(void);
foreign float emscripten_random(void);

/** wasi
 * - The time since the start of the program
 */
foreign wasm_error __wasi_clock_res_get(u32, u64*) __attribute__((
    __import_module__("wasi_snapshot_preview1"),
    __import_name__("clock_res_get")
));
foreign wasm_error __wasi_clock_time_get(u32, u64, u64*) __attribute__((
    __import_module__("wasi_snapshot_preview1"),
    __import_name__("clock_time_get")
));



#define PROT_NONE  0x00
#define PROT_READ  0x01
#define PROT_WRITE 0x02
#define PROT_EXEC  0x04
#define MAP_SHARED    0x0001
#define MAP_PRIVATE   0x0002
#define MAP_ANONYMOUS 0x0020
#define MAP_STACK     0x20000
foreign void* mmap(void*, uptr, int, int, int, s64);
foreign int munmap(void*, uptr);



/** math.h
 * 
 */
#define __pure2 __attribute__((__const__))
foreign int	isinf(double);
foreign int	isnan(double);
foreign float	acosf(float);
foreign float	asinf(float);
foreign float	atanf(float);
foreign float	atan2f(float, float);
foreign float	cosf(float);
foreign float	sinf(float);
foreign float	tanf(float);
foreign float	coshf(float);
foreign float	sinhf(float);
foreign float	tanhf(float);
foreign float	exp2f(float);
foreign float	expf(float);
foreign float	expm1f(float);
foreign float	frexpf(float, int *);	/* fundamentally !__pure2 */
foreign int	ilogbf(float) __pure2;
foreign float	ldexpf(float, int);
foreign float	log10f(float);
foreign float	log1pf(float);
foreign float	log2f(float);
foreign float	logf(float);
foreign float	modff(float, float *);	/* fundamentally !__pure2 */
foreign float	powf(float, float);
foreign float	sqrtf(float);
foreign float	ceilf(float);
foreign float	fabsf(float) __pure2;
foreign float	floorf(float);
foreign float	fmodf(float, float);
foreign float	roundf(float);
foreign float	erff(float);
foreign float	erfcf(float);
foreign float	hypotf(float, float);
foreign float	lgammaf(float);
foreign float	tgammaf(float);
foreign float	acoshf(float);
foreign float	asinhf(float);
foreign float	atanhf(float);
foreign float	cbrtf(float);
foreign float	logbf(float);
foreign float	copysignf(float, float) __pure2;
foreign long long llrintf(float);
foreign long long llroundf(float);
foreign long	lrintf(float);
foreign long	lroundf(float);
foreign float	nanf(const char *) __pure2;
foreign float	nearbyintf(float);
foreign float	nextafterf(float, float);
foreign float	remainderf(float, float);
foreign float	remquof(float, float, int *);
foreign float	rintf(float);
foreign float	scalblnf(float, long);
foreign float	scalbnf(float, int);
foreign float	truncf(float);
foreign float	fdimf(float, float);
foreign float	fmaf(float, float, float);
foreign float	fmaxf(float, float) __pure2;
foreign float	fminf(float, float) __pure2;

// TODO: Look if these can be replaced by intrinsics
inline int isnanf(float x) { return x != x; }
inline float frac(float x)     { return x - floorf(x); }
inline float rsqrt(float x)    { return 1.0f / sqrtf(x); }
inline float saturate(float x) { return x < 0 ? 0 : x > 1 ? 1 : x; }
inline float radians(float x)  { return x * 57.29577951308232f; }
inline float degrees(float x)  { return x * 0.017453292519943295f; }

/**
 * HACK: Naming is unfortunate
 */
#define abs   fabsf
#define acos  acosf
#define asin  asinf
#define atan  atanf
#define atan2 atan2f
#define ceil  ceilf
#define cbrt  cbrtf
#define cos   cosf
#define cosh  coshf
#define exp   expf
#define exp2  exp2f
#define floor floorf
#define fmod  fmodf
#define log   logf
#define log10 log10f
#define log2  log2f
#define modf  modff
#define pow   powf
#define round roundf
#define sin   sinf
#define sinh  sinhf
#define sqrt  sqrtf
#define tan   tanf
#define tanh  tanhf
#define trunc truncf

/**
 * PLATFORM_VAR(windows, unix, ...)
 */
#if SOLITUDE_EDITOR
#define EDITOR_PARAM(x) x
#else
#define EDITOR_PARAM(x)
#endif

#if _WIN64 || _WIN32
	#define SOLITUDE_WIN 1
	#if _WIN64
		#define SOLITUDE_PLATFORM "Windows x64"
		#define SOLITUDE_WIN64 1
		#define SOLITUDE_X64 1
		#define _AMD64_
	#else
		#define SOLITUDE_PLATFORM "Windows x32"
		#define SOLITUDE_WIN32 1
		#define SOLITUDE_X32 1
		#define _X86_
	#endif
	#define _CRT_SECURE_NO_WARNINGS
	#define NOMINMAX
	#define WIN32_LEAN_AND_MEAN
	#define PLATFORM_VAR(x, ...) x
	#include <Windows.h>
	#include "win/sys.hpp"
	#include "shared/import.hpp"
	#include "win/win.hpp"
	#include "win/video.hpp"
	#include "win/input.hpp"
	
	
#elif __wasm__
	void print_stacktrace();
	#define SOLITUDE_X32 1
	#define SOLITUDE_WASM 1
	#include "wasm/sys.hpp"
	#include "shared/import.hpp"
	#include "wasm/wasm.hpp"
	#include "wasm/debug.hpp"
	// TODO: Add input and video layer
	struct video
	{
		
	};
	error init(video*) { return error::ok; }
	void dispose(video*) { }
	InputEvent poll(video*) { return {0}; }
	struct input
	{
		
	};
	error init(input*) { return error::ok; }
	void dispose(input*) { }
	void gamepad_update(input*) { }
	
	
#elif __APPLE__
	#define SOLITUDE_APPLE 1
	#define HAVE_MACH_TIMER
	#include <mach/mach_time.h>
	#error Apple/iOS/osX not supported
	
	
	
#elif __ANDROID__
	// TODO: platform detection x86 ARM x32
	void print_stacktrace();
	#define SOLITUDE_ANDROID 1
	#define SOLITUDE_ANDROID64 1
	#define SOLITUDE_X64 1
	#define PLATFORM_VAR(_0, x, ...) x
	#include <jni.h>
	#include "android/sys.hpp"
	#include "shared/import.hpp"
	#include "android/android.hpp"
	#include "android/debug.hpp"
	struct video
	{
		
	};
	error init(video*) { return error(1, "not supported"); }
	void dispose(video*) { }
	InputEvent poll(video*) { return {0}; }
	struct input
	{
		
	};
	error init(input*) { return error(1, "not supported"); }
	void dispose(input*) { }
	void gamepad_update(input*) { }
	
	
	
#elif __FreeBSD__
	#define SOLITUDE_UNIX 1
	#define SOLITUDE_BSD 1
	#if __x86_64__ || __ppc64__
		#define SOLITUDE_BSD64 1
		#define SOLITUDE_X64 1
	#else
		#define SOLITUDE_BSD32 1
		#define SOLITUDE_X32 1
		#error BSD x32 is not yet supported
	#endif
	#define PLATFORM_VAR(_0, x, ...) x
	void print_stacktrace();
	#include "unix/sys.hpp"
	#include "shared/import.hpp"
	#include "unix/unix.hpp"
	#include "unix/video.hpp"
	#include "unix/input.hpp"
	#if SOLITUDE_DEBUG
		#include "unix/debug.hpp"
	#endif
	
	
	
#elif __unix__
	#define SOLITUDE_UNIX 1
	#define SOLITUDE_LINUX 1
	#if __x86_64__ || __ppc64__
		#define SOLITUDE_LINUX64 1
		#define SOLITUDE_X64 1
	#else
		#define SOLITUDE_LINUX32 1
		#define SOLITUDE_X32 1
	#endif
	#define PLATFORM_VAR(_0, x, ...) x
	void print_stacktrace();
	#include "unix/sys.hpp"
	#include "shared/import.hpp"
	#include "unix/unix.hpp"
	#include "unix/video.hpp"
	#include "unix/input.hpp"
	#if SOLITUDE_DEBUG
		#include "unix/debug.hpp"
	#endif
#else
	#error This is an unknown and unsupported platform
#endif


union int3
{
	struct
	{
		union
		{
			int2 xy;
			struct { int x, y; };
		};
		int z;
	};
	struct { int __0; int2 yz; };
	struct { int r, g, b; };
	int e[3];
};
constexpr int hashcode(int3 v) { return v.x + v.y * 0x0BD4BCB5 + v.z * 0x0063D68D; }

inline int3 operator - (int3 l) { return { -l.x, -l.y, -l.z }; }
inline int3 operator - (int3 l, int3 r) { return { l.x - r.x, l.y - r.y, l.z - r.z }; }
inline int3 operator - (int r, int3 l) { return { r - l.x, r - l.y, r - l.z }; }
inline int3 operator - (int3 l, int r) { return { l.x - r, l.y - r, l.z - r }; }
inline int3 operator + (int3 l, int3 r) { return { l.x + r.x, l.y + r.y, l.z + r.z }; }
inline int3 operator + (int r, int3 l) { return { r + l.x, r + l.y, r + l.z }; }
inline int3 operator + (int3 l, int r) { return { l.x + r, l.y + r, l.z + r }; }
inline int3 operator * (int3 l, int3 r) { return { l.x * r.x, l.y * r.y, l.z * r.z }; }
inline int3 operator * (int l, int3 r) { return { l * r.x, l * r.y, l * r.z }; }
inline int3 operator * (int3 l, int r) { return { l.x * r, l.y * r, l.z * r }; }

inline int3& operator -= (int3& l, int3 r)
{
	l.x -= r.x;
	l.y -= r.y;
	l.z -= r.z;
	return l;
}
inline int3& operator -= (int3& l, int r)
{
	l.x -= r;
	l.y -= r;
	l.z -= r;
	return l;
}
inline int3& operator += (int3& l, int3 r)
{
	l.x += r.x;
	l.y += r.y;
	l.z += r.z;
	return l;
}
inline int3& operator += (int3& l, int r)
{
	l.x += r;
	l.y += r;
	l.z += r;
	return l;
}
inline int3& operator *= (int3& l, int3 r)
{
	l.x *= r.x;
	l.y *= r.y;
	l.z *= r.z;
	return l;
}
inline int3& operator *= (int3& l, int r)
{
	l.x *= r;
	l.y *= r;
	l.z *= r;
	return l;
}

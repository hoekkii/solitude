namespace memory
{
	constexpr flags32 NONE              = 0;
	constexpr flags32 OWNER             = 1 << 0;
	constexpr flags32 EXHAUSTED         = 1 << 1;
	constexpr flags32 CLEAR             = 1 << 2;
	constexpr flags32 TEMP              = 1 << 3;
	constexpr flags32 SHARED            = 1 << 4;
	constexpr flags32 STACK             = 1 << 5;
	constexpr flags32 COPY              = 1 << 6;
	
	constexpr flags32 DEFAULT_FLAGS     = NONE;
	constexpr flags32 DEFAULT_ALIGNMENT = 64;

	constexpr flags32 JOB_FREE          = 0;
	constexpr flags32 JOB_WORKING       = 1 << 0;
	constexpr flags32 JOB_DONE          = 1 << 1;

	/**
	 * Unmanaged dynamic buffer type:
	 * The buffer grows when reserving without fancy pooling.
	 * No data is written in de allocated buffer
	 * The owner is responsible for not sharing the data
	 */
	struct unmanaged : public buffer_d
	{
		flags32 flags;
	};
	


	/**
	 * Managed dynamic buffer type:
	 * Used in conjuction with memory::pool
	 * When the content is overflowing a new block will be allocated.
	 * Block data is stored on the (beginning of the) allocated data.
	 */
	struct block : public buffer_d
	{
		flags32 flags;
		block* previous;  // Previous block in the pool
		block* _previous; // Platform memory map previous
		block* _next;     // Platform memory map next
	};

	struct pool
	{
		block* current;
		flags32 flags;
		int ensured_capacity;
	};
	
	struct temp : public buffer_d
	{
		struct pool* pool;
		struct block* block;
		uptr offset;
	};
	
	struct job
	{
		flags32 flags;
		struct pool pool;
		struct temp temp;
	};

	
	inline pool default_pool()
	{
		pool result;
		result.current = null;
		result.flags = CLEAR;
		result.ensured_capacity = 1 * MEGABYTE;
		return result;
	}
};

// TODO: Create an allocator



// Platform specific procedures, implementation is in /platform/<platform>/<platform>.hpp
memory::block* allocate(uptr, flags32);
void deallocate(memory::block* block);
byte* allocate_raw(uptr* size_, flags32 flags);
void deallocate_raw(void*, uptr);

static inline u64 ceil_to_mask(u64 x, u64 mask) { return x & mask ? (x | mask) + 1 : x; }






static uptr reserve(memory::unmanaged* buffer, uptr size, flags32 flags = memory::DEFAULT_FLAGS | memory::COPY)
{
	if (size > buffer->length)
	{
		var data = allocate_raw(&size, flags);
		if (flags & memory::COPY)
			copy(data, buffer->data, buffer->length);
		else buffer->length = 0;

		if (buffer->data)
			deallocate_raw(buffer->data, 0);

		buffer->data = data;
		buffer->capacity = size;
	}

	return buffer->capacity;
}













/**
 *
 *
 *
 *
 *
 */
inline uptr prepare_push(memory::pool* pool, uptr initial_size, flags32 alignment)
{
	assert(alignment <= 128);
	assert(IS_POW2(alignment));
	
	var size = initial_size;
	var current = pool->current;
	if (current) 
	{
		var ptr = (uptr)current->data + current->length;
		var mask = ptr & (alignment - 1);
		if (mask)
			size += alignment - mask;
	}
	
	// ! Allocated a new block of memory
	if (!current || ((current->length + size) > current->capacity))
	{
		// TODO: Check if in the current block is still some room left to be reused and put next in queue
		size = initial_size;
		var block_size = size < pool->ensured_capacity ? pool->ensured_capacity : size;
		current = allocate(block_size, pool->flags);
		current->previous = pool->current;
		pool->current = current;
	}

	uptr offset = current->length;
	var ptr = (uptr)current->data + offset;
	var mask = ptr & (alignment - 1);
	if (mask)
		offset += alignment - mask;
	
	return offset;
}

inline buffer_d push(memory::pool* pool, uptr initial_size, flags32 flags = memory::DEFAULT_FLAGS, flags32 alignment = memory::DEFAULT_ALIGNMENT)
{
	uptr offset = prepare_push(pool, initial_size, alignment);
	var current = pool->current;
	current->length = offset + initial_size;
	
	buffer_d result;
	result.data = current->data + offset;
	result.capacity = initial_size;
	result.length = 0;
	
	if (flags & memory::CLEAR)
		clear(result);

	return result;
}

inline void free(memory::pool* pool, memory::block* block)
{
	if (block == pool->current)
	{
		pool->current = block->previous;
	}
	else
	{
		var current = pool->current;
		while(current)
		{
			var child = current->previous;
			if (child == block)
			{
				current->previous = child->previous;
				goto __done;
			}
			
			current = child;
		}
		
		assert(!"Block not found in pool");
	}
	
	__done:
	deallocate(block);
}

inline void replace(memory::pool* pool, memory::block* old, memory::block* replacement)
{
	replacement->previous = old->previous;
	
	if (old == pool->current)
	{
		pool->current = replacement;
	}
	else
	{
		var current = pool->current;
		while(current)
		{
			var child = current->previous;
			if (child == old)
			{
				current->previous = replacement;
				goto __done;
			}
			
			current = child;
		}
		
		assert(!"Block not found in pool");
	}
	
	__done:
	deallocate(old);
}

inline memory::temp temp(memory::pool* pool, uptr ensured_capacity = 0, flags32 flags = memory::DEFAULT_FLAGS, flags32 alignment = memory::DEFAULT_ALIGNMENT)
{
	assert(!(pool->flags & memory::TEMP));

	uptr offset = prepare_push(pool, ensured_capacity, alignment);
	pool->flags |= memory::TEMP;
	var current = pool->current;

	memory::temp result;
	result.data = current->data + offset;
	result.capacity = current->capacity - offset;
	result.length = 0;
	result.pool = pool;
	result.block = pool->current;
	result.offset = offset;
	
	if (flags & memory::CLEAR)
		clear(result);
	
	return result;
}

inline void reserve(memory::temp* temp, uptr size, flags32 flags = memory::DEFAULT_FLAGS, flags32 alignment = memory::DEFAULT_ALIGNMENT)
{
	assert(temp->pool->flags & memory::TEMP);
	
	if (size > temp->capacity)
	{
		var pool = temp->pool;
		var block_size = size < pool->ensured_capacity ? pool->ensured_capacity : size;
		var current = allocate(block_size, pool->flags);
		
		if (temp->offset)
		{
			current->previous = pool->current;
			pool->current = current;
		}
		else
		{
			var block = temp->block;
			copy(current->data, block->data, block->length);
			replace(temp->pool, temp->block, current);
		}
	}
}

inline void keep(memory::temp* temp)
{
	assert(temp->pool->flags & memory::TEMP);
	
	var block = temp->block;
	block->length = temp->offset + temp->length;
	temp->capacity = temp->length;
	temp->pool->flags &= ~memory::TEMP;
}


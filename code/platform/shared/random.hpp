// https://xoshiro.di.unimi.it/xoshiro256starstar.c

namespace random
{
	/** Squirrel Eiserloh
	 * Fast hash function to use in noise generation
	 */
	static inline uint hash(uint m, uint seed) // 
	{
		m = (m * 0x68E31DA4) + seed;
		m = (m ^ (m >> 8)) + 0xB5297A4D;
		m = (m ^ (m << 8)) * 0x1B56C4E9;
		return m ^ (m >> 8);
	}

	/** Murmurhash2
	 * General use for internal hash generation
	 */
	static inline uint murmur(uint k, uint seed)
	{
		constexpr uint m = 0x5BD1E995;
		uint h = seed;
		k *= m;
		k ^= k >> 24;
		k *= m;
		h *= m;
		h ^= k;
		h ^= h >> 13;
		h *= m;
		h ^= h >> 15;
		return h;
	}


	/** Murmurhash2
	 * General use for internal hash generation
	 */
	static uint murmur(const byte* data, uint l, uint seed)
	{
		constexpr uint m = 0x5BD1E995;
		uint h = seed ^ l;
		while (l >= 4)
		{
			uint k = *reinterpret_cast<const uint*>(data);
			k *= m;
			k ^= k >> 24;
			k *= m;
			h *= m;
			h ^= k;
			data += 4;
			l -= 4;
		}
		switch (l)
		{
			case 3: h ^= data[2] << 16;
			case 2: h ^= data[1] << 8;
			case 1: h ^= data[0];
			h *= m;
		}
		h ^= h >> 13;
		h *= m;
		h ^= h >> 15;
		return h;
	}

	/** LCG
	 * Fast state accumulated random number generator.
	 * This is quite low quality, use with care.
	 */
	static inline uint lcg(uint state) { return ((state * 0x0019660D) + 0x3C6EF25F); }

	/** Xorshift32 https://en.wikipedia.org/wiki/Xorshift
	 * Fast -but slower than lcg- state accumulated random number generator.
	 * This is quite low quality, use with care.
	 */
	static inline uint xorshift(uint state)
	{
		state ^= state << 13;
		state ^= state >> 17;
		state ^= state <<  5;
		return state;
	}

	/** Xorshift128 https://en.wikipedia.org/wiki/Xorshift
	 * Fast -but slower than lcg- state accumulated random number generator.
	 * Has okay results.
	 */
	static inline uint xorshift(uint state[4])
	{
		uint t = state[3];
		uint s = state[0];
		state[3] = state[2];
		state[2] = state[1];
		state[1] = s;
		t ^= t << 11;
		t ^= t >>  8;
		return state[0] = t ^ s ^ (s >> 19);
	}




	static inline float noise(int v, uint seed)
	{
		uint x = (0x3f800000 | (0x007fffff & hash(v, seed)));
		return int_float::utf(x) - 1;
 	}
	static inline float noise(uint v, uint seed)
	{
		uint x = (0x3f800000 | (0x007fffff & hash(v, seed)));
		return int_float::utf(x) - 1;
 	}
	static inline float noise(float v, uint seed)
	{
		int xmin = v < 0 ? (int)(v - 1) : (int)v;
		var xvar = noise(xmin, seed);
		return lerp(xvar, noise(xmin + 1, seed), v - xmin);
	}
	static inline float noise(float2 v, uint seed)
	{
		float2x2 q;
		int2 m {
			v.x < 0 ? (int)(v.x - 1) : (int)v.x,
			v.y < 0 ? (int)(v.y - 1) : (int)v.y,
		};
		q.m00 = noise(hashcode(m + int2 { 0, 0 }), seed);
		q.m01 = noise(hashcode(m + int2 { 1, 0 }), seed);
		q.m10 = noise(hashcode(m + int2 { 0, 1 }), seed);
		q.m11 = noise(hashcode(m + int2 { 1, 1 }), seed);
		return bilinear(v - m, q);
	}
	static inline float noise(float3 v, uint seed)
	{
		float2x2 q1, q2;
		int3 m {
			v.x < 0 ? (int)(v.x - 1) : (int)v.x,
			v.y < 0 ? (int)(v.y - 1) : (int)v.y,
			v.z < 0 ? (int)(v.z - 1) : (int)v.z,
		};
		q1.m00 = noise(hashcode(m + int3 { 0, 0, 0 }), seed);
		q1.m01 = noise(hashcode(m + int3 { 1, 0, 0 }), seed);
		q1.m10 = noise(hashcode(m + int3 { 0, 1, 0 }), seed);
		q1.m11 = noise(hashcode(m + int3 { 1, 1, 0 }), seed);
		q2.m00 = noise(hashcode(m + int3 { 0, 0, 1 }), seed);
		q2.m01 = noise(hashcode(m + int3 { 1, 0, 1 }), seed);
		q2.m10 = noise(hashcode(m + int3 { 0, 1, 1 }), seed);
		q2.m11 = noise(hashcode(m + int3 { 1, 1, 1 }), seed);
		return lerp(
			bilinear(v.xy - m.xy, q1),
			bilinear(v.xy - m.xy, q2),
			v.z - m.z);
	}
};



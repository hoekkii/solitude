// layout - Row Major - m[row][col]
//   |   0     1
// --+------------
// 0 | 0:m00 1:m01
// 1 | 2:m10 3:m11
union float2x2
{
	struct
	{
		float m00, m01;
		float m10, m11;
	};

	float m[2][2];
	float e[4];
	float2 columns[2];
	
	static const float2x2 zero;
	static const float2x2 identity;
};
const float2x2 float2x2::zero = float2x2 {
	0.0f, 0.0f,
	0.0f, 0.0f,
};
const float2x2 float2x2::identity = float2x2 {
	1.0f, 0.0f,
	0.0f, 1.0f,
};

inline float2x2 transpose(float2x2 x)
{
	return {
		x.m00, x.m10,
		x.m01, x.m11,
	};
}

inline float determinant(float2x2 x)
{
	return x.m00 * x.m11
		 - x.m01 * x.m10;
}

inline float2x2 inverse(float2x2 x)
{
	var determinant = x.m00 * x.m11 - x.m01 * x.m10;
	assert(determinant != 0);

	var inv_determinant = 1.0f / determinant;
	return {
		+ x.m11 * inv_determinant,
		- x.m01 * inv_determinant,
		- x.m10 * inv_determinant,
		+ x.m00 * inv_determinant,
	};
}

inline float bilinear(float2 v, float2x2 q) 
{
	float2 w = 1 - v;
    return 
        q.m00 * w.x * w.y +
        q.m01 * v.x * w.y +
        q.m10 * w.x * v.y +
        q.m11 * v.x * v.y;
}

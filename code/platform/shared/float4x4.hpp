// layout - Row Major - m[row][col]
//   |    0      1      2      3
// --+----------------------------
// 0 |  0:m00  1:m01  2:m02  3:m03
// 1 |  4:m10  5:m11  6:m12  7:m13
// 2 |  8:m20  9:m21 10:m22 11:m23
// 3 | 12:m30 13:m31 14:m32 15:m33
union float4x4
{
	struct
	{
		float m00, m01, m02, m03;
		float m10, m11, m12, m13;
		float m20, m21, m22, m23;
		float m30, m31, m32, m33;
	};

	float m[4][4];
	float e[16];
	float4 columns[4];
	
	static const float4x4 zero;
	static const float4x4 identity;
};
const float4x4 float4x4::zero = float4x4 {
	0.0f, 0.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 0.0f, 0.0f,
};
const float4x4 float4x4::identity = float4x4 {
	1.0f, 0.0f, 0.0f, 0.0f,
	0.0f, 1.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 1.0f, 0.0f,
	0.0f, 0.0f, 0.0f, 1.0f,
};


inline float4x4 operator * (float4x4 l, float4x4 r)
{
	return {
		l.m00 * r.m00 + l.m01 * r.m10 + l.m02 * r.m20 + l.m03 * r.m30,
		l.m00 * r.m01 + l.m01 * r.m11 + l.m02 * r.m21 + l.m03 * r.m31,
		l.m00 * r.m02 + l.m01 * r.m12 + l.m02 * r.m22 + l.m03 * r.m32,
		l.m00 * r.m03 + l.m01 * r.m13 + l.m02 * r.m23 + l.m03 * r.m33,

		l.m10 * r.m00 + l.m11 * r.m10 + l.m12 * r.m20 + l.m13 * r.m30,
		l.m10 * r.m01 + l.m11 * r.m11 + l.m12 * r.m21 + l.m13 * r.m31,
		l.m10 * r.m02 + l.m11 * r.m12 + l.m12 * r.m22 + l.m13 * r.m32,
		l.m10 * r.m03 + l.m11 * r.m13 + l.m12 * r.m23 + l.m13 * r.m33,

		l.m20 * r.m00 + l.m21 * r.m10 + l.m22 * r.m20 + l.m23 * r.m30,
		l.m20 * r.m01 + l.m21 * r.m11 + l.m22 * r.m21 + l.m23 * r.m31,
		l.m20 * r.m02 + l.m21 * r.m12 + l.m22 * r.m22 + l.m23 * r.m32,
		l.m20 * r.m03 + l.m21 * r.m13 + l.m22 * r.m23 + l.m23 * r.m33,

		l.m30 * r.m00 + l.m31 * r.m10 + l.m32 * r.m20 + l.m33 * r.m30,
		l.m30 * r.m01 + l.m31 * r.m11 + l.m32 * r.m21 + l.m33 * r.m31,
		l.m30 * r.m02 + l.m31 * r.m12 + l.m32 * r.m22 + l.m33 * r.m32,
		l.m30 * r.m03 + l.m31 * r.m13 + l.m32 * r.m23 + l.m33 * r.m33
	};
}
inline float4 operator * (float4x4 l, float4 r)
{
	return {
		l.m00 * r.x + l.m01 * r.y + l.m02 * r.z + l.m03 * r.w,
		l.m10 * r.x + l.m11 * r.y + l.m12 * r.z + l.m13 * r.w,
		l.m20 * r.x + l.m21 * r.y + l.m22 * r.z + l.m23 * r.w,
		l.m30 * r.x + l.m31 * r.y + l.m32 * r.z + l.m33 * r.w,
	};
}





inline float4x4 transpose(float4x4 x)
{
	return {
		x.m00, x.m10, x.m20, x.m30,
		x.m01, x.m11, x.m21, x.m31,
		x.m02, x.m12, x.m22, x.m32,
		x.m03, x.m13, x.m23, x.m33,
	};
}

inline float determinant(float4x4 x)
{
	var a0 = x.m00 * x.m11 - x.m10 * x.m01;
	var a1 = x.m00 * x.m21 - x.m20 * x.m01;
	var a2 = x.m00 * x.m31 - x.m30 * x.m01;
	var a3 = x.m10 * x.m21 - x.m20 * x.m11;
	var a4 = x.m10 * x.m31 - x.m30 * x.m11;
	var a5 = x.m20 * x.m31 - x.m30 * x.m21;

	var b0 = x.m02 * x.m13 - x.m12 * x.m03;
	var b1 = x.m02 * x.m23 - x.m22 * x.m03;
	var b2 = x.m02 * x.m33 - x.m32 * x.m03;
	var b3 = x.m12 * x.m23 - x.m22 * x.m13;
	var b4 = x.m12 * x.m33 - x.m32 * x.m13;
	var b5 = x.m22 * x.m33 - x.m32 * x.m23;

	return a0 * b5 - a1 * b4 + a2 * b3 + a3 * b2 - a4 * b1 + a5 * b0;
}

inline float4x4 inverse(float4x4 x)
{
	var a0 = x.m00 * x.m11 - x.m10 * x.m01;
	var a1 = x.m00 * x.m21 - x.m20 * x.m01;
	var a2 = x.m00 * x.m31 - x.m30 * x.m01;
	var a3 = x.m10 * x.m21 - x.m20 * x.m11;
	var a4 = x.m10 * x.m31 - x.m30 * x.m11;
	var a5 = x.m20 * x.m31 - x.m30 * x.m21;
	
	var b0 = x.m02 * x.m13 - x.m12 * x.m03;
	var b1 = x.m02 * x.m23 - x.m22 * x.m03;
	var b2 = x.m02 * x.m33 - x.m32 * x.m03;
	var b3 = x.m12 * x.m23 - x.m22 * x.m13;
	var b4 = x.m12 * x.m33 - x.m32 * x.m13;
	var b5 = x.m22 * x.m33 - x.m32 * x.m23;
	
	var determinant = a0 * b5 - a1 * b4 + a2 * b3 + a3 * b2 - a4 * b1 + a5 * b0;
	assert(determinant != 0);

	var inv_determinant = 1.0f / determinant;
	return {
		( x.m11 * b5 - x.m21 * b4 + x.m31 * b3) * inv_determinant,
		(-x.m01 * b5 + x.m21 * b2 - x.m31 * b1) * inv_determinant,
		( x.m01 * b4 - x.m11 * b2 + x.m31 * b0) * inv_determinant,
		(-x.m01 * b3 + x.m11 * b1 - x.m21 * b0) * inv_determinant,

		(-x.m10 * b5 + x.m20 * b4 - x.m30 * b3) * inv_determinant,
		( x.m00 * b5 - x.m20 * b2 + x.m30 * b1) * inv_determinant,
		(-x.m00 * b4 + x.m10 * b1 - x.m30 * b0) * inv_determinant,
		( x.m00 * b3 - x.m10 * b1 + x.m20 * b0) * inv_determinant,

		( x.m13 * a5 - x.m23 * a4 + x.m33 * a3) * inv_determinant,
		(-x.m03 * a5 + x.m23 * a2 - x.m33 * a1) * inv_determinant,
		( x.m03 * a4 - x.m13 * a2 + x.m33 * a0) * inv_determinant,
		(-x.m03 * a3 + x.m13 * a1 - x.m23 * a0) * inv_determinant,

		(-x.m12 * a5 + x.m22 * a4 - x.m32 * a3) * inv_determinant,
		( x.m02 * a5 - x.m22 * a2 + x.m32 * a1) * inv_determinant,
		(-x.m02 * a4 + x.m12 * a2 - x.m32 * a0) * inv_determinant,
		( x.m02 * a3 - x.m12 * a1 + x.m22 * a0) * inv_determinant,
	};
}

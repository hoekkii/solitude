template<typename T>
struct slice
{
	T* elements;
	int length;
	
	slice() = default;
	slice(buffer_f b) : elements((T*)b.data), length(b.length / sizeof(T)) {
		assert(length * sizeof(T) == b.length);
	}
	template<int N> slice(T (&elements)[N]) : elements(elements), length(N) {}

	inline T const& operator[](int index) const { return elements[index]; }
	inline T& operator[](int index) { return elements[index]; }
};
template<typename T> inline int len(slice<T> s) { return s.length; }
template<typename T> inline int cap(slice<T> s) { return s.length; }


template<typename T>
struct array : public slice<T>
{
	int capacity;
	flags32 flags;
	// TODO: Add an allocator (or with a new type)
};
template<typename T> inline int len(array<T> a) { return a.length; }
template<typename T> inline int cap(array<T> a) { return a.capacity; }


template<typename T, uptr N>
struct fixed_slice
{
	T elements[N];

	inline T const& operator[](int index) const { return elements[index]; }
	inline T& operator[](int index) { return elements[index]; }
};
template<typename T, uptr N> inline uptr len(fixed_slice<T, N> a) { return N; }
template<typename T, uptr N> inline uptr cap(fixed_slice<T, N> a) { return N; }


template<typename T, uptr N>
struct fixed_array : public fixed_slice<T, N>
{
	int length;
};
template<typename T, uptr N> inline uptr len(fixed_array<T, N> a) { return a.length; }
template<typename T, uptr N> inline uptr cap(fixed_array<T, N> a) { return N; }





// Primary types to get the shared platform layer working
#include "primary.hpp"
#include "numbers.hpp"
#include "buffer.hpp"
#include "array.hpp"
#include "string.hpp"
#include "io.hpp"
#include "error.hpp"

// Complex types
#include "thread.hpp"
#include "memory.hpp"

// Extended math types
#include "int2.hpp"
#include "int3.hpp"
#include "int4.hpp"
#include "uint2.hpp"
#include "uint3.hpp"
#include "uint4.hpp"
#include "float2.hpp"
#include "float3.hpp"
#include "float4.hpp"
#include "float2x2.hpp"
#include "float3x3.hpp"
#include "float4x4.hpp"
#include "float16.hpp"

// Extended math storage types
#include "short2.hpp"

// Extended platform functions
#include "random.hpp"



// TODO: Place to correct location !!!!


union mutable_const_ptr
{
	const void* c;
	void* m;
};

inline bool bigendian()
{
	union {
		short s;
		byte b[2];
	} v { 0x0102 };
	return v.b[0] == 1;
}
template<typename T>
inline T endian_swap(T v)
{
	union {
		T t;
		byte b[sizeof(T)];
	} x, y { v };
	for (var i = 0; i < sizeof(T); i++)
		x.b[i] = y.b[sizeof(T) - i - 1];
	return x.t;
}

// Events
struct SystemEvent
{
	static constexpr byte unk = 0;
	static constexpr byte quit = 1;
};
struct Keyboard
{
	static constexpr byte unk = 0;
	static constexpr byte esc = 1;
	static constexpr byte shift = 2;
	static constexpr byte a = 'a';
	static constexpr byte b = 'b';
	static constexpr byte c = 'c';
	static constexpr byte d = 'd';
	static constexpr byte e = 'e';
	static constexpr byte f = 'f';
	static constexpr byte g = 'g';
	static constexpr byte h = 'h';
	static constexpr byte i = 'i';
	static constexpr byte j = 'j';
	static constexpr byte k = 'k';
	static constexpr byte l = 'l';
	static constexpr byte m = 'm';
	static constexpr byte n = 'n';
	static constexpr byte o = 'o';
	static constexpr byte p = 'p';
	static constexpr byte q = 'q';
	static constexpr byte r = 'r';
	static constexpr byte s = 's';
	static constexpr byte t = 't';
	static constexpr byte u = 'u';
	static constexpr byte v = 'v';
	static constexpr byte w = 'w';
	static constexpr byte x = 'x';
	static constexpr byte y = 'y';
	static constexpr byte z = 'z';
};
struct InputEvent
{
	static constexpr int Key = 1;
	static constexpr int Axis = 2;
	static constexpr int Position = 3;
	static constexpr int Event = 4;
	int type;

	union
	{
		struct
		{
			int code;
			int value;
		} key;
		struct
		{
			int code;
			float value;
		} axis;
		struct
		{
			int code;
			int x;
			int y;
		} position;
		struct
		{
			int code;
			int value;
		} event;
	};
};


using string_slice = buffer_f;
using string  = buffer_d;




/**
 * ASSUMPTIONS:
 * - a is not null
 * - b is not null
 * - a is not b
 * - does not contain @[\^_{|}~
 */
inline bool equals_ignore_case_fast(const char* a, const char* b, int l)
{
	static constexpr char mask = 0b1011111;
	for (; l > 0; l--, a++, b++)
	{
		if ((*a & mask) != (*b & mask))
			return false;
	}
	return true;
}
inline bool contains_ignore_case_fast(const char* a, int al, const char* b, int bl)
{
	static constexpr char mask = 0b1011111;
	al -= bl;
	for (; al > 0; al--, a++)
	{
		var c = b;
		int cl = bl;
		var d = a;
		for (; cl > 0; cl--, c++, d++)
		{
			if ((*c & mask) != (*d & mask))
				goto __continue;
		}
		return true;
		__continue:;
	}
	return false;
}

inline bool contains(const char* a, int al, const char* b, int bl)
{
	al -= bl;
	for (; al > 0; al--, a++)
	{
		var c = b;
		int cl = bl;
		var d = a;
		for (; cl > 0; cl--, c++, d++)
		{
			if (*c != *d)
				goto __continue;
		}
		return true;
		__continue:;
	}
	return false;
}

inline string_slice substring(string_slice str, u64 index)
{
	string_slice result;
	result.data = str.data + index;
	result.length = str.length - index;
	return result;
}
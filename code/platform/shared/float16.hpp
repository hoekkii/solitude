union float16
{
	ushort u;
};
inline float to_float32(float16 input)
{
	constexpr uint shifted_exp = 0x0f800000;
	var magic = int_float::utf(0x38800000);
	var result = int_float::fu((input.u & 0x7fff) << 13);
	uint exp = shifted_exp & result.u;
	result.u += (127 - 15) << 23;
	
	if (exp == shifted_exp) result.u += (128 - 16) << 23;
	else if (exp == 0)
	{
		result.u += 1 << 23;
		result.f -= magic;
	}
	result.u |= (input.u & 0x8000) << 16;
	return result.f;
}
inline float16 to_float16(float input)
{
	constexpr uint f32inf = 0x7f800000;
	constexpr uint f16inf = 0x0f800000;
	constexpr uint smask  = 0x80000000;
	constexpr uint rmask  = 0xFFFFF000;
	int_float magic;
	magic.u = 0x7800000;

	float16 result;
	int_float v { input };
	uint sign = v.u & smask;
	v.u ^= sign;
	if (v.u < f32inf)
	{
		v.u &= rmask;
		v.f *= magic.f;
		v.u -= rmask;
		if (v.u > f16inf)
			v.u = f16inf;
		result.u = v.u >> 13;
	}
	else if (v.u > f32inf) result.u = 0x7e00;
	else result.u = 0x7c00;
	result.u |= sign >> 16;
	return result;
}

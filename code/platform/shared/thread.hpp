#if COMPILER_MSVC
	#define READ_BARRIER _ReadBarrier()
	#define WRITE_BARRIER _WriteBarrier()
	#define ATOMIC_COMPARE_EXCHANGE(v, n, e) _InterlockedCompareExchange((long volatile *)v, n, e)
	#define ATOMIC_EXCHANGE(v, n) _InterlockedExchange64((__int64 volatile *)v, n)
	#define ATOMIC_ADD(v, a) _InterlockedExchangeAdd64((__int64 volatile *)v, a)
	inline uint current_thread_id()
	{
		var thread_id = (byte*)__readgsqword(0x30);
		return *(uint*)(thread_id + 0x48);
	}
#elif COMPILER_LLVM
	#define READ_BARRIER asm volatile("" ::: "memory")
	#define WRITE_BARRIER asm volatile("" ::: "memory")
	#define ATOMIC_COMPARE_EXCHANGE(v, n, e) __sync_val_compare_and_swap(v, e, n)
	#define ATOMIC_EXCHANGE(v, n) __sync_lock_test_and_set(v, n)
	#define ATOMIC_ADD(v, a) __sync_fetch_and_add(v, a)
	inline uint current_thread_id()
	{
		uint thread_id;
		#if defined(__APPLE__) && defined(__x86_64__)
		asm("mov %%gs:0x00,%0" : "=r"(thread_id));
		#elif defined(__i386__)
		asm("mov %%gs:0x08,%0" : "=r"(thread_id));
		#elif defined(__x86_64__)
		asm("mov %%fs:0x10,%0" : "=r"(thread_id));
		#elif SOLITUDE_DEBUG
		thread_id = 0;
		#else
		#error Unsupported architecture
		#endif
		return thread_id;
	}
#else
	#error Compiler not supported!
#endif

/**
 * False sharing warning: The volatile keyword is added,
 * but is this enough to avoid false sharing. If not,
 * add edditional padding between ticket and serving.
 * Additionally this will be probably platform dependent
 * how much padding is needed (e.g. 64 bytes padding etc.)
 */
struct mutex
{
	u64 volatile ticket;
	u64 volatile serving;
};
inline void lock(mutex* m)
{
	var ticket = ATOMIC_ADD(&m->ticket, 1);
	while (ticket != m->serving);
}
inline void unlock(mutex* m)
{
	ATOMIC_ADD(&m->serving, 1);
}






/**
- freebsd: rfork

unix: clone, fork, vfork


*/






struct fmt
{
	struct writer
	{
		error (*write)(writer*, buffer_f);
		union
		{
			struct buffer_d buffer;
			void* handle;
		};
	} writer;
	flags32 flags;      // 
	int preceding;      // How many 0's to prepend a number 4 of 21 gives 0021
	int precision_min;  // How digits to round a number of 4 of 2.34567 gives 2.3457
	int precision_max;  // How digits to round a number of 4 of 2.34567 gives 2.3457
	
	fmt() = default;
	fmt(struct buffer_d);
	fmt to(struct buffer_d) const;

	static const char* numbers;
	static const char* lower_hex;
	static const char* upper_hex;

	static constexpr flags32 SIGN            = 1 <<  2; // Explicit sign
	static constexpr flags32 TYPE            = 1 <<  3; // Type of the value
	static constexpr flags32 UPPER           = 1 <<  4; // Use upper case
	static constexpr flags32 SCIENTIFIC      = 1 <<  5; // Scientific notation
	static constexpr flags32 HEX             = 1 <<  6; // Base 16 notation
	static constexpr flags32 OCT             = 1 <<  7; // Base 8 notation
	static constexpr flags32 BIN             = 1 <<  8; // Base 2 noration
	static constexpr flags32 BOOL            = 1 <<  9; // true or false
	static constexpr flags32 ESCAPE          = 1 << 10; // Escape a string
	static constexpr flags32 VALUE           = 1 << 11; // If it is a pointer, dereference if possible
	
	static error write_buffer(struct writer* w, buffer_f v);
	static error write_file(struct writer* w, buffer_f v);
};
const char* fmt::numbers = "0123456789"; 
const char* fmt::lower_hex = "0123456789abcdef";
const char* fmt::upper_hex = "0123456789ABCDEF";
error fmt::write_buffer(struct writer* w, buffer_f v) { return append(&w->buffer, v); }
error fmt::write_file(struct writer* w, buffer_f v)
{
	fwrite(v.data, v.length, sizeof(byte), w->handle);
	fflush(w->handle);
	return error::ok;
}


fmt::fmt(struct buffer_d b)
{
	writer.write = write_buffer;
	writer.buffer = b;
	flags = 0;
	preceding = 0;
	precision_min = 0;
	precision_max = 0;
}
fmt fmt::to(struct buffer_d b) const
{
	fmt result = *this;
	result.writer.write = write_buffer;
	result.writer.buffer = b;
	return result;
}

/** write
 * 
 * 
 * 
 * 
 */


inline error write(fmt* fmt, buffer_f b) { return fmt->writer.write(&fmt->writer, b); }
static error writea(fmt* fmt, const char* value) { return write(fmt, value); }
static error writec(fmt* fmt, char value)
{
	buffer_f v(&value, 1);
	return write(fmt, v);
}
static error writeu(fmt* fmt, s64 value)
{
	char buffer[128];
	char* ptr = buffer;
	
	var flags = fmt->flags;
	if (flags & fmt::BOOL)
	{
		auto data = flags & fmt::UPPER
			? (value ? "TRUE" : "FALSE")
			: (value ? "true" : "false");
		return write(fmt, data);
	}
	
	var base = 10;
	if (flags & fmt::HEX) base = 16;
	if (flags & fmt::OCT) base = 8;
	if (flags & fmt::BIN) base = 2;
	
	var numbers = flags & fmt::UPPER
		? fmt::upper_hex
		: fmt::lower_hex;
	if (flags & fmt::SIGN)
		*ptr++ = '+';
	
	var tmp = value;
	do {
		*ptr++ = '0';
		tmp /= base;
	} while(tmp || ptr - buffer < fmt->preceding);
	
	var length = ptr - buffer;
	do {
		var next = value / base;
		*--ptr = numbers[value - next * base];
		value = next;
	} while(value);
	
	buffer_f v(buffer, length);
	return write(fmt, v);
}
static error writes(fmt* fmt, s64 value)
{
	char buffer[128];
	char* ptr = buffer;
	
	var flags = fmt->flags;
	if (flags & fmt::BOOL)
	{
		var data = flags & fmt::UPPER
			? (value ? "TRUE" : "FALSE")
			: (value ? "true" : "false");
		return writea(fmt, data);
	}
	
	var base = 10;
	if (flags & fmt::HEX) base = 16;
	if (flags & fmt::OCT) base = 8;
	if (flags & fmt::BIN) base = 2;
	
	var numbers = flags & fmt::UPPER
		? fmt::upper_hex
		: fmt::lower_hex;
	
	if (value < 0)
	{
		*ptr++ = '-';
		value = -value;
	}
	else if (flags & fmt::SIGN)
		*ptr++ = '+';
	
	var tmp = value;
	do {
		*ptr++ = '0';
		tmp /= base;
	} while(tmp || ptr - buffer < fmt->preceding);
	
	var length = ptr - buffer;
	do {
		var next = value / base;
		*--ptr = numbers[value - next * base];
		value = next;
	} while(value);
	
	buffer_f v(buffer, length);
	return write(fmt, v);
}
static error writef(struct fmt* fmt, float value)
{
	var flags = fmt->flags;
	var base = 10;
	if (flags & (fmt::HEX | fmt::OCT | fmt::BIN))
	{
		//to_string(fmt, x.u);
		return error::unsupported;
	}
	
	if (isnan(value))
		return writea(fmt, "NaN");

	var numbers = fmt::lower_hex;
	if (flags & fmt::UPPER) numbers = fmt::upper_hex;

	union
	{
		float f;
		int i;
	} x {value};
	
	char buffer[64];
	var ptr = buffer;
	short exp2 = (uchar)(x.i >> 23) - SBYTEMAX;
	u64 mantissa = (x.i & 0xFFFFFF) | (1 << 23);
	u64 frac_val = 0;
	u64 int_val = 0;
	int safe_shift = ~exp2;
	u64 safe_mask = ULONGMAX >> (64 - 24 - safe_shift); 

	if (x.i < 0) *ptr++ = '-';
	else if (flags & fmt::SIGN) *ptr++ = '+';

	if (exp2 < -36)
	{
		var preceed = fmt->preceding;
		do *ptr++ = '0';
		while (--preceed > 0);

		var precision = fmt->precision_max;
		if (precision)
		{
			*ptr++ = '.';
			do *ptr++ = '0';
			while (--preceed > 0);
		}

		buffer_f v(buffer, ptr - buffer);
		return write(fmt, v);
	}
	else if (isinf(value))
	{
		buffer_f v(buffer, ptr - buffer);
		write(fmt, v);
		if (flags & fmt::UPPER)
			return writea(fmt, "INFINITY");
		return writea(fmt, "infinity");
	}
	else
	{
		if (exp2 >= 64) int_val = ULONGMAX;
		else if (exp2 >= 23) int_val = mantissa << (exp2 - 23);
		else if (exp2 >= 0)
		{
			int_val = mantissa >> (23 - exp2);
			frac_val = mantissa & safe_mask;
		}
		else frac_val = mantissa & 0xFFFFFF;

		var preceed = fmt->preceding;
		var int_tmp = int_val;
		do {
			*ptr++ = '0';
			int_tmp /= base;
		} while(int_tmp || ptr - buffer < preceed);
		
		var resume_ptr = ptr;
		var length = ptr - buffer;
		do {
			int_tmp = int_val / base;
			*--ptr = numbers[int_val - int_tmp * base];
			int_val = int_tmp;
		} while(int_val);
		ptr = resume_ptr;
		

		int precision = fmt->precision_max;
		if (frac_val)
		{
			*ptr++ = '.';
			var prec = precision;
			if (!prec)
			{
				prec = 4;
			}
			else if (prec > 8) prec = 8;
			while(--prec > 0)
			{
				frac_val *= base;
				*ptr++ = numbers[frac_val >> (24 + safe_shift)];
				frac_val &= safe_mask;
			}
			// We do not round to safe complexity: 99.9999 or hex values
			if (!precision)	
			{
				while (*(ptr-1) == '0')
				{
					ptr--;
					if (*(ptr-1)=='.')
					{
						ptr--;
						break;
					}
				}
			}
		}
		else if (precision)
		{
			if (!frac_val) *ptr++= '.';
			resume_ptr = ptr;
			while (precision--) *ptr++ = '0';
		}
		
		buffer_f v(buffer, ptr - buffer);
		return write(fmt, v);
	}
}
static error writed(struct fmt* fmt, double value) { return error::unsupported; }





/** str
 * 
 * 
 * 
 * 
 */
 

void str(struct fmt* fmt, s64 value)
{
	var flags = fmt->flags;
	if (flags & fmt::TYPE)
	{
		if (flags & fmt::UPPER) writea(fmt, "LONG");
		else writea(fmt, "long");
	}
	else
	{
		writes(fmt, value);
	}
}
void str(struct fmt* fmt, u64 value)
{
	var flags = fmt->flags;
	if (flags & fmt::TYPE)
	{
		if (flags & fmt::UPPER) writea(fmt, "ULONG");
		else writea(fmt, "ulong");
	}
	else
	{
		writeu(fmt, value);
	}
}
void str(struct fmt* fmt, int value)
{
	var flags = fmt->flags;
	if (flags & fmt::TYPE)
	{
		if (flags & fmt::UPPER) writea(fmt, "INT");
		else writea(fmt, "int");
	}
	else
	{
		writes(fmt, (s64)value);
	}
}
void str(struct fmt* fmt, uint value)
{
	var flags = fmt->flags;
	if (flags & fmt::TYPE)
	{
		if (flags & fmt::UPPER) writea(fmt, "UINT");
		else writea(fmt, "uint");
	}
	else
	{
		writeu(fmt, (u64)value);
	}
}
void str(struct fmt* fmt, short value)
{
	var flags = fmt->flags;
	if (flags & fmt::TYPE)
	{
		if (flags & fmt::UPPER) writea(fmt, "SHORT");
		else writea(fmt, "short");
	}
	else
	{
		writes(fmt, (s64)value);
	}
}
void str(struct fmt* fmt, ushort value)
{
	var flags = fmt->flags;
	if (flags & fmt::TYPE)
	{
		if (flags & fmt::UPPER) writea(fmt, "USHORT");
		else writea(fmt, "ushort");
	}
	else
	{
		writeu(fmt, (u64)value);
	}
}
void str(struct fmt* fmt, sbyte value)
{
	var flags = fmt->flags;
	if (flags & fmt::TYPE)
	{
		if (flags & fmt::UPPER) writea(fmt, "SBYTE");
		else writea(fmt, "sbyte");
	}
	else
	{
		writes(fmt, (s64)value);
	}
}
void str(struct fmt* fmt, byte value)
{
	var flags = fmt->flags;
	if (flags & fmt::TYPE)
	{
		if (flags & fmt::UPPER) writea(fmt, "BYTE");
		else writea(fmt, "byte");
	}
	else
	{
		writeu(fmt, (u64)value);
	}
}
void str(struct fmt* fmt, bool value)
{
	var flags = fmt->flags;
	if (flags & fmt::TYPE)
	{
		if (flags & fmt::UPPER) writea(fmt, "BOOL");
		else writea(fmt, "bool");
	}
	else
	{
		writeu(fmt, (u64)value);
	}
}
void str(struct fmt* fmt, float value)
{
	var flags = fmt->flags;
	if (flags & fmt::TYPE)
	{
		if (flags & fmt::UPPER) writea(fmt, "FLOAT");
		else writea(fmt, "float");
	}
	else writef(fmt, value);
}
void str(struct fmt* fmt, double value)
{
	var flags = fmt->flags;
	if (flags & fmt::TYPE)
	{
		if (flags & fmt::UPPER) writea(fmt, "DOUBLE");
		else writea(fmt, "double");
	}
	else writed(fmt, value);
}
void str(struct fmt* fmt, char c)
{
	var flags = fmt->flags;
	if (flags & fmt::TYPE)
	{
		if (flags & fmt::UPPER) writea(fmt, "CHAR");
		else writea(fmt, "char");
	}
	else writec(fmt, c);
}
void str(struct fmt* fmt, const char* c)
{
	var flags = fmt->flags;
	if (flags & fmt::TYPE)
	{
		if (flags & fmt::UPPER) writea(fmt, "CSTRING");
		else writea(fmt, "cstring");
	}
	else writea(fmt, c);
}
void str(struct fmt* fmt, buffer_f arg)
{
	var flags = fmt->flags;
	if (flags & fmt::TYPE)
	{
		if (flags & fmt::UPPER) writea(fmt, "STRING");
		else writea(fmt, "string");
	}
	else
	{
		write(fmt, arg);
	}
}




/** print
 * 
 * 
 * 
 * 
 */

void format(struct fmt* fmt, const char* const f)
{
	const char* traverse = f;
	while (*traverse && *traverse != '%') traverse++;
	var length = traverse - f;
	if (length > 0)
	{
		buffer_f v(f, length);
		write(fmt, v);
	}
	assert(!*traverse);
}

template<typename T, typename... Args>
void format(struct fmt* fmt, const char* const f, T arg, Args... args)
{
	const char* traverse = f;
	while (*traverse && *traverse != '%') traverse++;
	var length = traverse - f;
	if (length > 0)
	{
		buffer_f v(f, length);
		write(fmt, v);
	}
	if (!*traverse)
		return;

	flags32 flags = 0;
	while (*++traverse)
	{
		var c = *traverse;
		switch (c)
		{
			/*case '#': flags.sharp = true; break;
			case '0': flags.zero = !flags.minus; break;
			case '+': flags.plus = true; break;
			case '-': flags.minus = true; flags.zero = false; break;
			case ' ': flags.space = true; break;
			*/
			
			case 'h': flags |= fmt::VALUE | fmt::HEX; goto write_arg;
			case 'H': flags |= fmt::VALUE | fmt::HEX | fmt::UPPER; goto write_arg;
			case 't': flags |= fmt::TYPE; goto write_arg;
			case 'v': flags |= fmt::VALUE; goto write_arg;

			/*default:
			{
				if ('a' <= c && c <= 'z')
				{
					flags.type = c;
					__print_arg(p, flags, arg);
					traverse++;
				}
			} goto exit_flags;*/
		}
	}
	write_arg:
	traverse++;
	fmt->flags = flags;
	str(fmt, arg);
	exit_flags:
	format(fmt, traverse, args...);
}



/*template<typename T, typename... Args>
void print(struct fmt::writer w, const char* const f, T arg, Args... args)
{
	struct fmt fmt {};
	copy(&fmt, &w, sizeof(fmt::writer));
	format(&fmt, f, arg, args...);
	writec(&fmt, '\n');
}

template<typename T>
void print(struct fmt::writer w, T arg)
{
	struct fmt fmt {};
	copy(&fmt, &w, sizeof(fmt::writer));
	str(&fmt, arg);
	writec(&fmt, '\n');
}*/

template<typename T, typename... Args>
void print(const char* const f, T arg, Args... args)
{
	struct fmt fmt {};
	fmt.writer.write = fmt::write_file;
	fmt.writer.handle = stdout;
	format(&fmt, f, arg, args...);
	writec(&fmt, '\n');
}

template<typename T>
void print(T arg)
{
	struct fmt fmt {};
	fmt.writer.write = fmt::write_file;
	fmt.writer.handle = stdout;
	str(&fmt, arg);
	writec(&fmt, '\n');
}






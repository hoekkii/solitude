/**
 * Primary types with a dependencyloop are defined here.
 * - buffer_f
 * - buffer_d
 * - error
 */


template<uptr N, class T>
constexpr uptr len(T(&)[N]) { return N; }
constexpr uptr len(int i) { return i; }
inline int len(const char* c)
{
	int result = 0;
	if (!c) return result;
	while(c[result]) ++result;
	return result;
}

template<uptr N, class T>
constexpr uptr cap(T(&)[N]) { return N; }
constexpr uptr cap(int i) { return i; }
inline int cap(const char* c)
{
	int result = 0;
	if (!c) return result;
	while(c[result++]);
	return result;
}

struct buffer_f
{
	byte* data;
	uptr length;

	buffer_f() = default;
	buffer_f(byte* c, uptr l)
	{
		this->data = c;
		this->length = l;
	}
	buffer_f(const char* c, uptr l)
	{
		this->data = (byte*)c;
		this->length = l;
	}
	buffer_f(const char* c)
	{
		this->data = (byte*)c;
		this->length = len(c);
	}
	template<typename T> explicit buffer_f(T* v, uptr l)
	{
		this->data = (byte*)v;
		this->length = sizeof(T) * l;
	}
	template<typename T> explicit buffer_f(T* v)
	{
		this->data = (byte*)v;
		this->length = sizeof(T);
	}

	inline byte const& operator[](int index) const { return data[index]; }
	inline byte& operator[](int index) { return data[index]; }
	inline byte const& operator[](u64 index) const { return data[index]; }
	inline byte& operator[](u64 index) { return data[index]; }
};

struct buffer_d : public buffer_f
{
	uptr capacity;

	buffer_d() = default;
	
	template<uptr N>
	buffer_d(byte(&x)[N]) : buffer_f(x, 0)  { capacity = N; }

	template<typename T>
	explicit buffer_d(T* v, uptr l, uptr c) : buffer_f(v, l) {
		this->capacity = sizeof(T) * c;
	}
};



struct error
{
	int code;
	buffer_f message;
	
	error() = default;
	error(int c, buffer_f m) : code(c), message(m) {}

	template<typename T, typename... Args>
	error(int c, const char* const f, T t, Args... args);
	
	operator int() const { return code; }
	static const error ok;
	static const error unsupported;
	static const error depricated;
};
const error error::ok = error { };
const error error::unsupported = error(0x0F0F0000, "Procedure not implemented or supported.");
const error error::depricated  = error(0x0F0F0001, "Procedure is depricated.");



union int_float
{
	float f;
	int i;
	uint u;
	byte b[4];

	inline static constexpr int_float ff(float v)
	{
		int_float r{};
		r.f = v;
		return r;
	};
	inline static constexpr int_float fi(int v)
	{
		int_float r{};
		r.i = v;
		return r;
	};
	inline static constexpr int_float fu(uint v)
	{
		int_float r{};
		r.u = v;
		return r;
	};
	inline static constexpr float utf(uint v)
	{
		int_float r{};
		r.u = v;
		return r.f;
	}
};


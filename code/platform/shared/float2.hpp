union float2
{
	struct { float x, y; };
	float e[2];
	
	static const float2 zero;
	static const float2 one;
	static const float2 left;
	static const float2 right;
	static const float2 down;
	static const float2 up;
};
const float2 float2::zero  = { 0.0f, 0.0f };
const float2 float2::one   = { 1.0f, 1.0f };
const float2 float2::left  = {-1.0f, 0.0f };
const float2 float2::right = { 1.0f, 0.0f };
const float2 float2::down  = { 0.0f,-1.0f };
const float2 float2::up    = { 0.0f, 1.0f };

/*void to_string(io::fmt* fmt, float2 arg)
{
	var flags = fmt->flags;
	if (flags & io::TYPE)
	{
		if (flags & io::UPPER) write(fmt, "FLOAT2");
		else write(fmt, "float2");
	}
	else
	{
		IO_CUSTOM_FMT(64);
		write(&custom, '(');
		write_r32(&custom, arg.x);
		write(&custom, ", ");
		write_r32(&custom, arg.y);
		write(&custom, ')');
		write(fmt, custom.buffer);
	}
}*/


inline float2 operator - (float2 l) { return { -l.x, -l.y }; }
inline float2 operator - (float2 l, float2 r) { return { l.x - r.x, l.y - r.y }; }
inline float2 operator - (float r, float2 l) { return { r - l.x, r - l.y }; }
inline float2 operator - (float2 l, float r) { return { l.x - r, l.y - r }; }
inline float2 operator + (float2 l, float2 r) { return { l.x + r.x, l.y + r.y }; }
inline float2 operator + (float r, float2 l) { return { r + l.x, r + l.y }; }
inline float2 operator + (float2 l, float r) { return { l.x + r, l.y + r }; }
inline float2 operator * (float2 l, float2 r) { return { l.x * r.x, l.y * r.y }; }
inline float2 operator * (float l, float2 r) { return { l * r.x, l * r.y }; }
inline float2 operator * (float2 l, float r) { return { l.x * r, l.y * r }; }
inline float2 operator / (float2 l, float r) { return { l.x / r, l.y / r }; }



inline float2& operator -= (float2& l, float2 r)
{
	l.x -= r.x;
	l.y -= r.y;
	return l;
}
inline float2& operator -= (float2& l, float r)
{
	l.x -= r;
	l.y -= r;
	return l;
}
inline float2& operator += (float2& l, float2 r)
{
	l.x += r.x;
	l.y += r.y;
	return l;
}
inline float2& operator += (float2& l, float r)
{
	l.x += r;
	l.y += r;
	return l;
}
inline float2& operator *= (float2& l, float2 r)
{
	l.x *= r.x;
	l.y *= r.y;
	return l;
}
inline float2& operator *= (float2& l, float r)
{
	l.x *= r;
	l.y *= r;
	return l;
}
inline float2& operator /= (float2& l, float r)
{
	l.x /= r;
	l.y /= r;
	return l;
}






inline float2 operator - (float2 l, int2 r) { return { l.x - r.x, l.y - r.y }; }
inline float2 operator + (float2 l, int2 r) { return { l.x + r.x, l.y + r.y }; }
inline float2 operator * (float2 l, int2 r) { return { l.x * r.x, l.y * r.y }; }
inline float2& operator -= (float2& l, int2 r)
{
	l.x -= r.x;
	l.y -= r.y;
	return l;
}
inline float2& operator += (float2& l, int2 r)
{
	l.x += r.x;
	l.y += r.y;
	return l;
}
inline float2& operator *= (float2& l, int2 r)
{
	l.x *= r.x;
	l.y *= r.y;
	return l;
}






inline float2 modf(float2 l, float2* r) { return { modf(l.x, &r->x), modf(l.y, &r->y) }; }
inline float2 clamp(float2 x, float2 min, float2 max)
{
	return {
		x.x < min.x ? min.x : x.x > max.x ? max.x : x.x,
		x.y < min.y ? min.y : x.y > max.y ? max.y : x.y,
	};
}
inline float distance(float2 l, float2 r)
{
	float dx = l.x - r.x;
	float dy = l.y - r.y;
	return sqrt(dx * dx + dy * dy);
}
inline float dot(float2 l, float2 r) { return l.x * r.x + l.y * r.y; }
inline float2 faceforward(float2 n, float2 i, float2 g) { return dot(i, g) < 0 ? n : -n; }
inline float2 fixnan(float2 x, float2 value = {}) { return { isnan(x.x) ? value.x : x.x, isnan(x.y) ? value.y : x.y }; }
inline float length(float2 x) { return sqrt(x.x * x.x + x.y * x.y); }
inline float2 lerp(float2 l, float2 r, float t) { return { lerp(l.x, r.x, t), lerp(l.y, r.y, t) }; }
inline float2 lerps(float2 l, float2 r, float t) { return { lerps(l.x, r.x, t), lerps(l.y, r.y, t) }; }
inline float magnitude(float2 x) { return sqrt(x.x * x.x + x.y * x.y); }
inline float2 max(float2 l, float2 r) { return { l.x > r.x ? l.x : r.x, l.y > r.y ? l.y : r.y }; }
inline float2 min(float2 l, float2 r) { return { l.x < r.x ? l.x : r.x, l.y < r.y ? l.y : r.y }; }
inline float2 normalize(float2 v)
{
	auto length = v.x * v.x + v.y * v.y;
	if (length < EPSILON)
		return { 0.0f, 0.0f };

	length = rsqrt(length);
	return { v.x * length, v.y * length };
}
inline float2 reflect(float2 direction, float2 normal) { return -2.0f * dot(direction, normal) * normal + direction; }
inline float2 refract(float2 i, float2 n, float eta)
{
	auto cosi = dot(-i, n);
	auto cost2 = 1.0f - eta * eta * (1.0f - cosi * cosi);
	return cost2 > 0 ? eta * i + ((eta * cosi - sqrt(abs(cost2))) * n) : float2{};
}
inline float2 smoothstep(float2 min, float2 max, float2 x) { return { smoothstep(min.x, max.x, x.x), smoothstep(min.y, max.y, x.y) }; }
inline float sqrlength(float2 x) { return x.x * x.x + x.y * x.y; }
inline float sqrmagnitude(float2 x) { return x.x * x.x + x.y * x.y; }


#define float2_comp_proc(proc) inline float2 proc(float2 v) {\
	return { proc(v.x), proc(v.y) };\
}
#define float2_comp_proc_vec1(proc) inline float2 proc(float2 v, float r) {\
	return { proc(v.x, r), proc(v.y, r) };\
}
#define float2_comp_proc_vec2(proc) inline float2 proc(float2 v, float2 r) {\
	return { proc(v.x, r.x), proc(v.y, r.y) };\
}

float2_comp_proc(abs)
float2_comp_proc(acos)
float2_comp_proc(asin)
float2_comp_proc(atan)
float2_comp_proc_vec1(atan2)
float2_comp_proc_vec2(atan2)
float2_comp_proc(ceil)
float2_comp_proc(cbrt)
float2_comp_proc(cos)
float2_comp_proc(cosh)
float2_comp_proc(degrees)
float2_comp_proc(exp)
float2_comp_proc(exp2)
float2_comp_proc(floor)
float2_comp_proc_vec1(fmod)
float2_comp_proc_vec2(fmod)
float2_comp_proc(frac)
float2_comp_proc(log)
float2_comp_proc(log10)
float2_comp_proc(log2)
float2_comp_proc_vec1(pow)
float2_comp_proc_vec2(pow)
float2_comp_proc(radians)
float2_comp_proc(round)
float2_comp_proc(rsqrt)
float2_comp_proc(saturate)
float2_comp_proc(sin)
float2_comp_proc(sinh)
float2_comp_proc(sqrt)
float2_comp_proc(tan)
float2_comp_proc(tanh)
float2_comp_proc(trunc)
	


/** Primary datatype
 * Go to primary.hpp for the buffer_d and buffer_f definition.
 * 
 * 
 */

inline uptr len(buffer_f b) { return b.length; }
inline uptr cap(buffer_f b) { return b.length; }
inline uptr cap(buffer_d b) { return b.capacity; }

inline void clear(void* ptr, uptr size)
{
	var bytes = (byte*)ptr;
	while (size--) *bytes++ = 0;
}
inline void clear(buffer_f b)
{
	var size = b.length;
	while(size--) *b.data++ = 0;
}
inline void clear(buffer_d b)
{
	var size = b.capacity;
	while(size--) *b.data++ = 0;
}


inline void copy(void* dest, const void* source, uptr length)
{
	#if 0
	// https://elixir.bootlin.com/linux/latest/source/lib/string.c#L875
	byte* d;
	byte* s;
	uint* dw = (uint*)dest;
	uint* sw = (uint*)source;
	
	if (!((uptr)dw & 3) && !((uptr)sw & 3))
	{
		for (; len >= 4; len -= 4)
			*dw++ = *sw++;
	}

	d = (byte*)dw;
	s = (byte*)sw;
	for (; len >= 1; len -= 1)
		*d++ = *s++;
	#else
	var d = (byte*)dest;
	var s = (byte*)source;
	while(length--)
		*d++ = *s++;
	#endif
}

/**
 * Little endian and big endian will not produce the same result.
 * Therefore, only use this internally. 
 * https://sites.google.com/site/murmurhash/
 */
inline uint hash(const void* src, uint len, uint seed = 0)
{
	constexpr uint m = 0x5BD1E995;
	constexpr int r = 24;
	const byte* data = (const byte*)src;
	uint h = seed ^ len;
	
	while (len >= 4)
	{
		uint k = *(const uint*)data;
		k *= m;
		k ^= k >> r;
		k *= m;
		h *= m;
		h ^= k;
		data += 4;
		len -= 4;
	}
	
	switch (len)
	{
		case 3: h ^= data[2] << 16;
		case 2: h ^= data[1] << 8;
		case 1: h ^= data[0];
		h *= m;
	}
	
	h ^= h >> 13;
	h *= m;
	h ^= h >> 15;
	
	return h;
}

inline bool equals(const void* lhs, const void* rhs, uptr len)
{
	const byte* l = (const byte*)lhs;
	const byte* r = (const byte*)rhs;
	while (len >= 4)
	{
		uint ld = *(const uint*)l;
		uint rd = *(const uint*)r;
		if (ld != rd)
			return false;

		l += 4;
		r += 4;
		len -= 4;
	}
	
	switch (len)
	{
		case 3: if (l[2] != r[2]) return false;
		case 2: if (l[1] != r[1]) return false;
		case 1: if (l[0] != r[0]) return false;
	}
	
	return true;
}
inline bool equals(buffer_f a, buffer_f b) { return a.length == b.length && equals(a.data, b.data, a.length); }





inline error append(buffer_d* dst, const byte* buffer, uptr length)
{
	if (dst->length + length >= dst->capacity)
		return error(1, "Cannot write to buffer, insufficient capacity. [" FILE_LINE "]");

	var d = dst->data + dst->length;
	dst->length += length;
	while(length--)
		*d++ = *buffer++;

	return error::ok;
}
inline error append(buffer_d* dst, buffer_f v) { return append(dst, v.data, v.length); }
inline error append(buffer_d* dst, const char* buffer, uptr length) { return append(dst, (byte*)buffer, length); }
inline error append(buffer_d* dst, const char* buffer) { return append(dst, (byte*)buffer, len(buffer)); }
inline error append(buffer_d* dst, byte value)
{
	if (dst->length + 1 >= dst->capacity)
		return error(1, "Cannot write to buffer, insufficient capacity. [" FILE_LINE "]");
	
	dst->data[dst->length++] = value;
	return error::ok;
}
inline error append(buffer_d* dst, char value) { return append(dst, (byte)value); }
inline error terminate(buffer_d dst)
{
	if (dst.length < dst.capacity)
	{
		dst.data[dst.length] = 0;
		return error::ok;
	}
	return error(1, "Cannot write to buffer, insufficient capacity. [" FILE_LINE "]");
}






/** Primary datatype
 * Go to primary.hpp for the error definition.
 * 
 * 
 */


namespace __error_helper
{
	constexpr int temp_capacity = 1024;
	constexpr int temp_length = (1 << 5);
	constexpr int temp_mask = temp_length - 1;
	static volatile int temp_index = 0;
	static byte temp_data[temp_capacity][temp_length];
}


template<typename T, typename... Args>
error::error(int c, const char* const f, T t, Args... args)
{
	var temp_index = (__error_helper::temp_index + 1) & __error_helper::temp_mask;
	var temp = __error_helper::temp_data[temp_index];
	buffer_d buffer(temp, 0, __error_helper::temp_capacity); 
	fmt fmt(buffer);
	format(&fmt, f, t, args...);

	code = c;
	message = fmt.writer.buffer;
}


void str(struct fmt* f, error arg)
{
	var flags = f->flags;
	if (flags & fmt::TYPE)
	{
		if (flags & fmt::UPPER) writea(f, "ERROR");
		else writea(f, "error");
	}
	else
	{
		byte data[2048];
		fmt tmp = f->to(data);
		tmp.flags &= ~(fmt::BIN|fmt::OCT);
		tmp.flags |= fmt::HEX;
		writec(&tmp, 'E');
		writes(&tmp, (s64)arg.code);
		writec(&tmp, ' ');
		write(&tmp, arg.message);
		write(f, tmp.writer.buffer);
	}
}


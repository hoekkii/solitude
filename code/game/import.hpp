#include "types.hpp"



void begin_input(struct game* game)
{
	//game->look_x = 0;
	//game->look_y = 0;
}
void handle_input(struct game* game, InputEvent event)
{
	if (event.type == InputEvent::Key)
	{
		var k = event.key;
		var v = k.value;
		//print("%v (%v)", k.code, (int)'a');
		switch (k.code) 
		{
			case Keyboard::d: game->move_right = v; break;
			case Keyboard::a: game->move_left = v; break;
			case Keyboard::w: game->move_forward = v; break;
			case Keyboard::s: game->move_backward = v; break;
			case Keyboard::e: game->move_up = v; break;
			case Keyboard::q: game->move_down = v; break;
			case Keyboard::shift: game->move_fast = v; break;
		}
		
	}
	if (event.type == InputEvent::Position)
	{
		var k = event.position;
		if (game->_prev_a)
		{
			game->look_x += (k.x - game->_prev_x) * 0.015f;
			game->look_y -= (k.y - game->_prev_y) * 0.015f;
		}
		game->_prev_x = k.x;
		game->_prev_y = k.y;
		game->_prev_a++;
	}
}

void str(struct fmt* f, float3 v)
{
	var flags = f->flags;
	if (flags & fmt::TYPE)
	{
		if (flags & fmt::UPPER) writea(f, "ERROR");
		else writea(f, "error");
	}
	else
	{
		byte data[2048];
		fmt tmp = f->to(data);
		writec(&tmp, '(');
		writef(&tmp, v.x);
		writea(&tmp, ", ");
		writef(&tmp, v.y);
		writea(&tmp, ", ");
		writef(&tmp, v.z);
		writec(&tmp, ')');
		write(f, tmp.writer.buffer);
	}
}


void fixed_update(float deltatime, struct engine* engine, struct game* game, struct physics_state* physics_state)
{
	// Camera
	{
		// TODO: Camera rotation + rotation matrix

		float3x3 view; {
			rotor3 r = angle_y(game->look_x) * angle_x(game->look_y);
			view = to_matrix3x3(r);
		}


		float speed = game->move_fast ? 12.0f : 5.0f;
		float3 velocity {};
		velocity.x -= game->move_left;
		velocity.x += game->move_right;
		velocity.y -= game->move_down;
		velocity.y += game->move_up;
		velocity.z -= game->move_backward;
		velocity.z += game->move_forward;

		// Temp fix
		velocity.y = -velocity.y;
		velocity.z = -velocity.z;
		velocity = view * velocity;
		


		velocity = normalize(velocity);
		velocity = velocity * (deltatime * speed);
		// TODO: velocity smoothing + multipling rotation matrix with the velocity
		physics_state->camera.position += velocity;
	}

}


// TODO: Change vulkan to generic rendering API
void render(struct engine* engine, struct game* game, struct physics_state* physics_state)
{
	float4x4fi projection; {
		float aspect = 1.0f;
		float length = 3.0f;
		float nearcp = 0.2f;
		float farcp = 1000.0f;

		float x = 1.0f;
		float a = aspect;
		float l = length;
		float n = nearcp;
		float f = farcp;
		float d = (n + f) / (n - f);
		float e = (2.0f * f * n) / (n - f);
		float xl = x * l;
		float al = a * l;
		projection.forward = {
			xl,  0,  0, 0,
			 0, al,  0, 0,
			 0,  0,  d, e,
			 0,  0, -1, 0,
		};
		projection.inverse = {
			1/xl, 0, 0, 0,
			0, 1/al, 0, 0,
			0, 0, 0, -1,
			0, 0, 1/e, d/e,
		};
	}

	float4x4 view = {
		1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f,
	};
	
	{
		rotor3 r = angle_y(game->look_x) * angle_x(game->look_y);
		point t = physics_state->camera.position;
		vector3 s { 1.0f, 1.0f, 1.0f };
		view = inverse(RTS(r, t));
	}
	
	
	float4x4 model = {
		1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, -8.0f,
		0.0f, 0.0f, 0.0f, 1.0f,
	};
	
	{
		
		float a = time::to_s(engine->internal_time);
		vector3 from {0.0f, 0.0f, 1.0f};
		vector3 to { sin(a), 0.0f, cos(a) };
		rotor3 r = geometric(from, to);
		point t { 0.0f, 0.0f, -8.0f };
		vector3 s { 1.0f, 1.0f, 1.0f };
		rotor3 r2 = angle_plane(a, normalize(vector3{1.0f, 1.0f, 0.0f}));
		//t += rotate(r1, vector3{0.0f, 0.0f, 1.0f});
		t += rotate(r2, vector3{0.0f, 0.0f, 1.0f});
		model = (RTS(r, t, s));
	}
	
	
	#if SOLITUDE_VULKAN
	var api = engine->graphics_api;
	if (api.type == graphics::api::VULKAN)
	{
		var vk = (struct vulkan*)engine->graphics_api.shared;
		vulkan::push_constant push_constant{};
		push_constant.mvp = projection.forward * view * model;
		
		VkPtr vertexBuffers[] = {vk->vertex_buffer.handle};
		u64 offsets[] = {0};

		vk->vkCmdPushConstants(vk->command_buffer, vk->pipeline_layout, VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(vulkan::push_constant), &push_constant);
		vk->vkCmdBindVertexBuffers(vk->command_buffer, 0, 1, vertexBuffers, offsets);
		vk->vkCmdBindIndexBuffer(vk->command_buffer, vk->index_buffer.handle, 0, VK_INDEX_TYPE_UINT16);
		vk->vkCmdDrawIndexed(vk->command_buffer, 2304, 1, 0, 0, 0);
		
		
		
		model.m03 += 4.0f;
		push_constant.mvp = projection.forward * view * model;
		vk->vkCmdPushConstants(vk->command_buffer, vk->pipeline_layout, VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(vulkan::push_constant), &push_constant);
		vk->vkCmdBindVertexBuffers(vk->command_buffer, 0, 1, vertexBuffers, offsets);
		vk->vkCmdBindIndexBuffer(vk->command_buffer, vk->index_buffer.handle, 0, VK_INDEX_TYPE_UINT16);
		vk->vkCmdDrawIndexed(vk->command_buffer, 2304, 1, 0, 0, 0);
	}
	#endif
}







struct physics_state
{
	struct
	{
		float3 velocity;
		float3 position;
		float3 angle;
	} camera;
};

void copy(physics_state* from, physics_state* to)
{
	copy(to, from, sizeof(physics_state));
}


struct game
{
	// Input
	bool move_right;
	bool move_left;
	bool move_forward;
	bool move_backward;
	bool move_up;
	bool move_down;
	bool move_fast;


	int _prev_a;
	int _prev_x;
	int _prev_y;
	float look_x;
	float look_y;


};





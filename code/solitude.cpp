#define SOLITUDE_EDITOR 1
#define SOLITUDE_DEBUG 1
#define SOLITUDE_DEBUG_SHADERS 1
#define SOLITUDE_APP_NAME "solitude"

// Video
#define SOLITUDE_GL 1
#define SOLITUDE_GLES 0
#define SOLITUDE_VULKAN 0

// Audio
#define SOLITUDE_ALSA 0
#define SOLITUDE_PULSE 0
#define SOLITUDE_OSS 0
#define SOLITUDE_JACK 0
#define SOLITUDE_CORE 0

// Assets
#define SOLITUDE_WINFS 1
#define SOLITUDE_UNIXFS 1


#include "compiler/import.hpp"
#include "platform/import.hpp"
#include "engine/import.hpp"
#include "game/import.hpp"
#include "test/import.hpp"

struct main_state
{
	// Change this to separate the platform/engine/game, now it is mixed up, this doesn't look good.
	// Or change the ordering.
	struct input input;
	struct engine engine;
	struct game game;
	struct physics_state physics_real_state;
	struct physics_state physics_temp_state;
};
void str(fmt* fmt, main_state* arg)
{
	write(fmt, "main_state (str not implemented)");
}



#if SOLITUDE_ANDROID
/**
 * Android call procedures are as follows
 * extern "C" JNIEXPORT jobject JNICALL Java_<app_identifier>_<activity>_<procedure>(JNIEnv* env, jobject this)
 */
extern "C" JNIEXPORT void* JNICALL Java_com_hoekkii_solitude_MainActivity_testing(JNIEnv* env, void*)
{
	return env->NewStringUTF("AAAAAAAAAAAAAAAAAAAA");
}

extern "C" JNIEXPORT jstring JNICALL
Java_com_hoekkii_solitude_MainActivity_stringFromJNI(JNIEnv* env, jobject /* this */) {
	auto hello = "This is a test world thing!";
	return env->NewStringUTF(hello);
}
#elif SOLITUDE_WASM
void wasm_main_loop(void)
{
	var state = (struct main_state*)g_wasm_state.main_state;
	var video = &state->video;
	var input = &state->input;
	var engine = &state->engine;
	var game = &state->game;
	var physics_real_state = &state->physics_real_state;
	var physics_temp_state = &state->physics_temp_state;


	begin_frame(engine);
	render(engine, game, physics_temp_state);
	present_frame(engine);
}
#endif



int main(int argc, char* argv[])
{



	#if SOLITUDE_DEBUG && SOLITUDE_UNIX
	signal(10, crash_handler); // SIGBUS  bus error
	signal(11, crash_handler); // SIGSEGV segmentation violation
	test_all();
	#endif
	
	// 
	var state = new main_state();
	var input = &state->input;
	var engine = &state->engine;
	var game = &state->game;
	var physics_real_state = &state->physics_real_state;
	var physics_temp_state = &state->physics_temp_state;


	// 
	error err;
	print("Running program with arguments: ");
	for (int i = 0; i < argc; i++)
		print("    %v: %v", i, argv[i]);
	
	if (1)
	{
		if (err = init_platform(argc, argv))
		{
			print("An error occured while initializing platform: %v", err);
			return err;
		}
		if (err = init(engine))
		{
			print("An error occured while initializing game engine: %v", err);
			return err;
		}

		var file_job = temp(&engine->asset_api, "/glincludes/solitude.gl", 0);
		print(*file_job);
		done(file_job);



		const char* path = "U:\\solitude\\assets\\fonts\\inconsolata.ttf";
		font_generator generator;
		if (err = init(engine, &generator))
		{
			dispose(&generator);
			print("An error occured while initializing font generator: %v", err);
			return err;
		}
		
		if (err = load_font(&generator, path))
		{
			dispose(&generator);
			print("An error occured while initializing font generator: %v", err);
			return err;
		}
		
		dispose(&generator);
		dispose(engine);
		print("Exited with: %v", err);
		return 0;
	}
	
	if (err = init_platform(argc, argv))
	{
		print("An error occured while initializing platform: %v", err);
		return err;
	}
	if (err = init(input))
	{
		print("An error occured while initializing input devices: %v", err);
		return err;
	}
	if (err = init(engine))
	{
		print("An error occured while initializing game engine: %v", err);
		return err;
	}
	
	print("Starting main loop..");
	#if SOLITUDE_WASM
	g_wasm_state.main_state = state;
	emscripten_set_main_loop(wasm_main_loop, 0, 1);
	return 0;
	#else
	for(bool run = true; run;)
	{
		// Handle input
		begin_input(game);
		for (var i = 0; i < 1024; i++)
		{
			var event = poll(&engine->video);
			if ((event.type == InputEvent::Key && event.key.code == Keyboard::esc) ||
				(event.type == InputEvent::Event && event.event.code == SystemEvent::quit))
			{
				run = false;
				break;
			}

			if (event.type == 0)
				break;

			handle_input(game, event);
		}
		gamepad_update(input);

		// Handle gameplay
		{
			var monotonic_time = time::precise();
			var deltatime = monotonic_time - engine->monotonic_time;
			if (deltatime > engine::max_timestep)
				deltatime = engine::max_timestep;
			var internal_time = engine->internal_time += deltatime;
			engine->monotonic_time = monotonic_time;
			engine->frame++;
			engine->deltatime = time::to_s(deltatime);
			for (;;)
			{
				var next_fixed_frame = engine->fixed_frame + 1;
				var next_fixed_time = next_fixed_frame * engine::fixed_step;
				if (next_fixed_time > internal_time)
					break;
				engine->fixed_frame = next_fixed_frame;
				fixed_update(1.0f / engine::fixed_rate, engine, game, physics_real_state);
			}
			
			var tf = engine->fixed_frame * engine::fixed_step;
			copy(physics_real_state, physics_temp_state);
			fixed_update(time::to_s(internal_time - tf), engine, game, physics_temp_state);
		}
		
		// Handle rendering
		{
			begin_frame(engine);
			render(engine, game, physics_temp_state);
			present_frame(engine);
		}
	}
	#endif
	
	print("Closing..");
	dispose(input);
	dispose(engine);
	return 0;
}



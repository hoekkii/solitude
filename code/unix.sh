#!/bin/sh

# Start
start_time=`date +%s`
print_time () {
	echo "time elapsed: $(( `date +%s`-start_time ))s"
	echo 
}

# Target specific commands
platform=`uname`
if [ $platform = "FreeBSD" ]
then
	code_dir=$(dirname $0)
	root_dir="$code_dir/.."
else
	code_dir=$(dirname "${BASH_SOURCE[0]}")
	code_dir=$(dirname $0)
	root_dir="$code_dir/.."
fi


# Determine arguments
any=0
run=0
release=0
shaders=0
unix=0
wasm=0
err=0
for arg_i in "$@"
do
	if [ "$arg_i" = "-e" ] || [ "$arg_i" = "--release" ]
	then
		release=1
	fi
	if [ "$arg_i" = "-r" ] || [ "$arg_i" = "--run" ]
	then
		run=1
	fi
	if [ "$arg_i" = "-s" ] || [ "$arg_i" = "--shaders" ]
	then
		any=1
		shaders=1
	fi
	if [ "$arg_i" = "-b" ] || [ "$arg_i" = "--unix" ]
	then
		any=1
		unix=1
	fi
	if [ "$arg_i" = "-w" ] || [ "$arg_i" = "--wasm" ]
	then
		any=1
		wasm=1
	fi
	if [ "$arg_i" = "-h" ] || [ "$arg_i" = "--help" ]
	then
		echo "-e --release  Export a build of the game"
		echo "-r --run      Run the game after compilation"
		echo "-s --shaders  Build all shaders"
		echo "-b --unix     Add $platform (current Unix system) as targeted build using g++"
		echo "-w --wasm     Add WebAssembly as targeted build using emscripten"
		echo "-h --help     Print available commands"
		exit 0
	fi
done

if [ $any = 0 ]
then
	echo "No arguments have been given that will do any work. For help use the --help argument."
fi

# Compile shaders
if [ $err = 0 ] && [ $shaders = 1 ]
then
	shader_dir_i="$code_dir/shaders/*"
	shader_dir_o="$root_dir/assets/"
	for f in $shader_dir_i
	do
		name=${f#*/}
		fo=$shader_dir_o$name".spv"
		echo "Processing $name"
		if glslc $f -o $fo
		then
			echo "SUCCESS"
			echo ""
		else
			echo "FAILED"
			echo ""
		fi
	done
fi


# Compile targeting current Unix system
if [ $unix = 1 ]
then
	if [ $err = 0 ]
	then
		compiler="/usr/local/bin/g++"
		build_i="$code_dir/solitude.cpp"
		build_o="$root_dir/builds/unix"
		build_f="$build_o/solitude"
		optimization=""
		lexecinfo=""
		if [ $release = 1 ]
		then
			optimization="-O3"
		fi

		if [ $platform = "FreeBSD" ]
		then
			lexecinfo="-lexecinfo"
		fi

		if [ ! -f $compiler ]
		then
			compiler="g++"
		fi

		mkdir -p $build_o
		echo "compiling \"$build_i\" targeting $platform to \"$build_f\""
		if $compiler -g -rdynamic $lexecinfo "$build_i" -lX11 -m64 $optimization -o "$build_f" -Wl,--no-as-needed -ldl
		then
			if [ $run = 1 ]
			then
				echo "compiled targeting $platform to \"$build_f\" succesfully"
				echo "executing compiled executable.."
				print_time
				$build_f
			fi
		else
			err=1
		fi
	else
		echo "wanted to build for $platform, but an error occurred earlier in the script"
	fi
fi



# Compile wasm
if [ $wasm = 1 ]
then
	if [ $err = 0 ]
	then
		compiler="emcc"
		build_i="$code_dir/solitude.cpp"
		build_s="$code_dir/platform/wasm/wasm.html"
		build_o="$root_dir/builds/wasm"
		build_f="$build_o/index.html"
		optimization=""
		if [ $release = 1 ]
		then
			optimization="-O3"
		fi

		mkdir -p $build_o
		echo "compiling \"$build_i\" targeting WebAssembly to \"$build_f\""
		if $compiler -o "$build_f" "$build_i" $optimization -Wno-parentheses -Wno-version-check --shell-file "$build_s" -s NO_EXIT_RUNTIME=1 -s "EXPORTED_RUNTIME_METHODS=['ccall']"
		then
			echo "compiled targeting WebAssembly to \"$build_f\" succesfully"
		fi
	else
		echo "wanted to build for WebAssembly, but an error occurred earlier in the script"
	fi
fi

# exiting
print_time
exit 0


# Links

## 2D
https://www.freepik.com/
https://www.iconfinder.com/
https://unsplash.com/
https://richiepanda.ee/

## Text UI
https://github.com/azsn/gllabel
https://wdobbie.com/post/gpu-text-rendering-with-vector-textures/
https://wdobbie.com/pdf/

## 3D
https://github.com/boksajak/brdf/blob/master/brdf.h
https://www.guerrilla-games.com/read/decima-engine-advances-in-lighting-and-aa/
www.mamoniem.com/author/muhammad/
https://www.intel.com/content/www/us/en/developer/topic-technology/graphics-research/samples.html
https://github.com/HectorMF/BRDFGenerator

## Misc
https://www.mathsisfun.com/algebra/trig-interactive-unit-circle.html
https://www.raylib.com/
http://number-none.com/product/index.html
https://cbloomrants.blogspot.com/
http://programming.sirrida.de/
http://www.ludicon.com/castano/blog/
https://fgiesen.wordpress.com/2012/03/28/half-to-float-done-quic/
https://raphlinus.github.io/graphics/2020/04/21/blurred-rounded-rects.html
https://iquilezles.org/articles/distfunctions2d/
https://caseymuratori.com/blog_0015
https://blog.molecular-matters.com/2011/09/05/properly-handling-keyboard-input/
https://lance.handmade.network/blog/p/7891-how_to_make_printscreen_work_in_exclusive_fullscreen
https://olster1.github.io/windows-keyboard-input.html
https://www.anthropicstudios.com/2021/03/05/crash-reporter/
https://github.com/rudimeier/atem/blob/34b54f687bbe45bc85671b4a0d3575ccb12b0d70/src/itoa.c
https://cellperformance.beyond3d.com/articles/2008/03/three-big-lies.html
https://atomicobject.com/uploads/archive/files/ObjectOrientedProgramming.pdf
https://ciechanow.ski/
https://www.youtube.com/watch?v=PNlgMPzj-7Q&list=PLpzmRsG7u_gqaTo_vEseQ7U8KFvtiJY4K

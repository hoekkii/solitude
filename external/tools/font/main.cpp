// Converts freetype (ttf) to bitmap font
// NOTE: This is quick and dirty and not made for efficiency


#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <cstdio>
#include <stdint.h>
#include <limits>
#include <float.h>
#include <string>

#define PNG_DEBUG 3
#include <png.h>

#include <ft2build.h>
#include FT_FREETYPE_H


#define assert(expr) if (!(expr)) { printf("FAILED assertion [" __FILE__ ":%i] " #expr "\n", __LINE__); *(int*)0=0; }

// TYPES
typedef int8_t s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef float r32;
typedef double r64;
typedef intptr_t intptr;
typedef uintptr_t uintptr;
typedef unsigned char uchar;

using namespace std;

struct Glyph
{
    s32 code;
    s32 x;
    s32 y;
    s32 width;
    s32 height;
    s32 left;
    s32 top;
    s32 advance;
};



png_byte color_type = PNG_COLOR_TYPE_GRAY;

// TODO: Make these params
s32 padding = 4;
s32 border = 4;
s32 size = 512;
s32 font_size = 65;
const char* file = "inconsolata.ttf";
const char* output = "inconsolata.png";
const char* output_sfont = "inconsolata.sfont";
const char* chars = 
"!\"#$%\'()*+,-./"
"0123456789"
":;<=>?@"
"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
"[\\]^_`"
"abcdefghijklmnopqrstuvwxyz"
"{|}~";



png_structp png_ptr;
png_infop info_ptr;
int number_of_passes;
png_bytep* row_pointers;
void abort_(const char * s, ...)
{
    va_list args;
    va_start(args, s);
    vfprintf(stderr, s, args);
    fprintf(stderr, "\n");
    va_end(args);
    abort();
}

int main(int argc, char** argv)
{
    assert(sizeof(char) == 1);
    
    
    s32 charcount = strlen(chars);
    //printf("\n\n%i\n\n", strlen(chars));
    //return 1;
    
    //printf("%i", sizeof("asdf"));
    //return 0;
    
    /*
    // TODO: Instead of hardcoding settings, allow for parameters to set these
for (s32 i = 1; i < argc; i++)
    {
        string s(argv[i]);
        printf(s.c_str());
    }
*/
    
    ////////////////////////////
    // Initialize
    ////////////////////////////
    
    // TTF
    FT_Library library;
    auto error = FT_Init_FreeType(&library);
    assert(!error);
    
    printf("%i\n", __LINE__);
    FT_Face face;
    error = FT_New_Face(library, file, 0, &face);
    assert(!error);
    
    error = FT_Set_Char_Size(face, font_size * 64, 0, 100, 0);
    assert(!error);
    
    // PNG
    FILE* fp = fopen(output, "wb");
    if (!fp) abort_("[write_png_file] File %s could not be opened for writing", output);
    png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (!png_ptr) abort_("[write_png_file] png_create_write_struct failed");
    info_ptr = png_create_info_struct(png_ptr);
    if (!info_ptr) abort_("[write_png_file] png_create_info_struct failed");
    if (setjmp(png_jmpbuf(png_ptr))) abort_("[write_png_file] Error during init_io");
    png_init_io(png_ptr, fp);
    if (setjmp(png_jmpbuf(png_ptr))) abort_("[write_png_file] Error during writing header");
    png_set_IHDR(png_ptr, info_ptr, size, size, 8, color_type, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
    
    // Create clean background
    row_pointers = (png_bytep*)png_malloc(png_ptr, size * sizeof(png_byte*));
    for (auto y = 0; y < size; y++)
    {
        png_byte* row = (png_byte*)png_malloc(png_ptr, size * 1);
        row_pointers[y] = row;
        
        for (auto x = 0; x < size; x++)
        {
            row[x] = (png_byte)0;
        }
    }
    
    
    ////////////////////////////
    // DO ACTUAL WORK HERE
    ////////////////////////////
    Glyph* gs = (Glyph*)malloc(sizeof(Glyph) * charcount);
    s32 i = 0;
    
    for (auto cp = chars; *cp; cp++, i++)
    {
        printf("%c: ", *cp);
        
        error = FT_Load_Char(face, *cp, FT_LOAD_RENDER);
        assert(!error);
        auto cglyph = face->glyph;
        auto cbitmap = cglyph->bitmap;
        auto cbuffer = cbitmap.buffer;
        
        Glyph& g = gs[i];
        g.code = static_cast<s32>(*cp);
        g.x = static_cast<s32>(border);
        g.y = static_cast<s32>(border);
        g.width = static_cast<s32>(cbitmap.width);
        g.height = static_cast<s32>(cbitmap.rows);
        g.left = static_cast<s32>(cglyph->bitmap_left);
        g.top = static_cast<s32>(cglyph->bitmap_top);
        g.advance = static_cast<s32>(cglyph->advance.x);
        bool done = false;
        
        printf("%ix%i\n", g.width, g.height);
        
        for (;;)
        {
            g.x = static_cast<s32>(border);
            for (;;)
            {
                if (g.x + g.width  > size - border ||
                    g.y + g.height > size - border)
                    break;
                
                bool collision = false;
                for (auto j = 0; !collision && j < i; j++)
                {
                    auto o = gs[j];
                    collision =
                        o.x + o.width > g.x - padding &&
                        o.x < g.x + g.width + padding &&
                        o.y + o.height > g.y - padding &&
                        o.y < g.y + g.height + padding;
                }
                
                if (!collision)
                {
                    for (auto y = g.y, cy = 0; cy < g.height; y++, cy++)
                        for (auto x = g.x, cx = 0; cx < g.width; x++, cx++)
                        row_pointers[y][x] = cbuffer[cy * g.width + cx];
                    
                    goto __l_done;
                }
                
                g.x++;
            }
            g.y++;
        }
        
        assert(false /*size of image too small*/);
        __l_done:;
    }
    
    
    
    
    ////////////////////////////
    // Finalize
    ////////////////////////////
    png_write_info(png_ptr, info_ptr);
    if (setjmp(png_jmpbuf(png_ptr))) abort_("[write_png_file] Error during writing bytes");
    png_write_image(png_ptr, row_pointers);
    if (setjmp(png_jmpbuf(png_ptr))) abort_("[write_png_file] Error during end of write");
    png_write_end(png_ptr, NULL);
    for (auto y=0; y<size; y++)
        free(row_pointers[y]);
    fclose(fp);
    
    
    
    FILE* sfp = fopen(output_sfont, "wb");
    fwrite(gs, sizeof(Glyph), charcount, sfp);
    fclose(sfp);
    
    return 0;
}
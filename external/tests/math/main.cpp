namespace external
{
	extern float acos(float);
};

#include "math.h"
#include "stdio.h"
#include <cstdlib>
#include <chrono>
using namespace std::chrono;


constexpr int ___length = 1024 * 64;
float* generate_arr() {
  auto x = new float[___length];
  srand(0);
  for (auto i = 0; i < ___length; i++)
    x[i] = (float)(((double)rand() / (RAND_MAX / 2)) - 1.0);
  return x;
}


//__declspec(noinline) 
[[gnu::noinline]]
int member_func(float x) { return (int)x; }

int main()
{
	double value = 0;
	unsigned long long time0 = 0;
	unsigned long long time1 = 0;
	float* arr = generate_arr();
	for (int i = 0; i < 1024; i++)
	{

		auto start1 = high_resolution_clock::now();
		for (int j = 0; j < ___length; j++)
			value += acosf(arr[j]);
		auto stop1 = high_resolution_clock::now();
		auto duration1 = duration_cast<microseconds>(stop1 - start1);
		time1 += duration1.count();


		auto start0 = high_resolution_clock::now();
		for (int j = 0; j < ___length; j++)
			value += external::acos(arr[j]);
		auto stop0 = high_resolution_clock::now();
		auto duration0 = duration_cast<microseconds>(stop0 - start0);
		time0 += duration0.count();
	}
	
	printf("time0: %lu\n", time0);
	printf("time1: %lu\n", time1);
	return member_func((float)value);
}







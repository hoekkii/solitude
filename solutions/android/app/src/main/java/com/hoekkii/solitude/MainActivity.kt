package com.hoekkii.solitude

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.hoekkii.solitude.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Example of a call to a native method
        binding.sampleText.text = testing()
    }

    /**
     * A native method that is implemented by the 'solitude' native library,
     * which is packaged with this application.
     */
    external fun stringFromJNI(): String
    external fun testing(): String

    companion object {
        // Used to load the 'solitude' library on application startup.
        init {
            System.loadLibrary("solitude")
        }
    }
}
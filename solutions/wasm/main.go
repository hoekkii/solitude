package main
import (
	"os"
	"fmt"
	"time"
	"net/http"
)

const (
	HOME = "../../builds/wasm"
	PUBLIC = HOME + ""
)

type Server struct { }

func (server *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	path := r.URL.Path
	if path == "" || path == "/" {
		path = "/index.html"
	}
	
	file := PUBLIC + path;
	if _, err := os.Stat(file); err == nil {
		http.ServeFile(w, r, file)
	} else {
		w.WriteHeader(404)
	}
}

func main() {
	server := &Server { }
	http_server := &http.Server{
		Handler:        server,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1048576,
		Addr: ":80",
	}
	fmt.Fprintf(os.Stdout, "Server running at %v\n", http_server.Addr)
	err := http_server.ListenAndServe()
	if err != nil {
		fmt.Fprintf(os.Stdout, "An error has occurred: %v", err)
		os.Exit(1)
	}
}